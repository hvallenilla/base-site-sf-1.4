-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-08-2013 a las 10:50:53
-- Versión del servidor: 5.5.27
-- Versión de PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `br_joaopaulo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id_contact` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `last_name` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `organization` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `comments` longtext COLLATE latin1_spanish_ci NOT NULL,
  `contact_type` set('0','1','2') COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_contact`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_actividades`
--

CREATE TABLE IF NOT EXISTS `log_actividades` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(15) NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lx_analytics`
--

CREATE TABLE IF NOT EXISTS `lx_analytics` (
  `id_analytics` int(11) NOT NULL AUTO_INCREMENT,
  `send_report` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(150) COLLATE latin1_spanish_ci DEFAULT NULL,
  `password` varchar(150) COLLATE latin1_spanish_ci DEFAULT NULL,
  `connected` set('0','1') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_analytics`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `lx_analytics`
--

INSERT INTO `lx_analytics` (`id_analytics`, `send_report`, `email`, `password`, `connected`) VALUES
(1, '', '', '', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lx_module`
--

CREATE TABLE IF NOT EXISTS `lx_module` (
  `id_module` int(11) NOT NULL AUTO_INCREMENT,
  `name_module` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `sf_module` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `credential` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `status` set('0','1') COLLATE latin1_spanish_ci DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `delete` set('1','0') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_module`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=39 ;

--
-- Volcado de datos para la tabla `lx_module`
--

INSERT INTO `lx_module` (`id_module`, `name_module`, `sf_module`, `credential`, `status`, `id_parent`, `position`, `delete`) VALUES
(1, 'Administração de usuários', '', '', '1', 0, 1, '1'),
(2, 'Informação da conta', 'lxaccount', 'lx_account', '1', 1, 1, '1'),
(3, 'Trocar senha', 'lxchangePassword', 'lx_password', '1', 1, 2, '1'),
(4, 'Módulos', 'lxmodule', 'lx_module', '1', 1, 3, '1'),
(5, 'Usuarios', 'lxuser', 'lx_user', '1', 1, 5, '1'),
(6, 'Instituições', 'lxprofile', 'lx_profile', '1', 8, 6, '1'),
(7, 'Administração de Seções', '', '', '1', 0, 3, '0'),
(8, 'Usuários e Perfis', '', '', '1', 1, 6, '1'),
(9, 'Seções', 'lxsection', 'lx_section', '1', 7, 1, '0'),
(13, 'Notícias', 'news', 'sf_news', '1', 7, 2, '1'),
(16, 'Permissões de usuário', 'lxuserpermission', 'lx_user_permission', '1', 8, NULL, '1'),
(18, 'Galeria', 'album', 'sf_album', '1', 7, 5, '0'),
(20, 'Agenda', 'agenda', 'agenda', '0', 31, NULL, '0'),
(21, 'Videos', 'video', 'video', '1', 7, NULL, '0'),
(23, 'Categoria da Agenda', 'categoriaAgenda', 'categoriaagenda', '0', 7, NULL, '0'),
(27, 'Arquivos de seção', 'archivo', 'archivo', '1', 7, NULL, '0'),
(29, 'Serviços', 'servicos', 'servicos', '1', 31, NULL, '1'),
(30, 'Informativos', 'informativo', 'informativo', '1', 31, NULL, '1'),
(31, 'Propriedades do Sistema', '', '', '1', 0, NULL, '1'),
(32, 'Categorias Atos Oficiais', 'atosCategoria', 'atoscategoria', '1', 31, NULL, '1'),
(33, 'Atos Oficiais', 'atosOficiais', 'atosoficiais', '1', 31, NULL, '1'),
(34, 'Banner', 'banner', 'banner', '1', 31, NULL, '1'),
(35, 'Agendas', 'eventos', 'eventos', '1', 31, NULL, '1'),
(36, 'Segurança', 'seguranca', 'seguranca', '1', 31, NULL, '1'),
(37, 'Conselhos', 'conselhos', 'conselhos', '1', 7, NULL, '1'),
(38, 'Obras', 'obras', 'obras', '1', 31, NULL, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lx_privilege`
--

CREATE TABLE IF NOT EXISTS `lx_privilege` (
  `id_privilege` int(11) NOT NULL AUTO_INCREMENT,
  `privilege_name` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `sf_privilege` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `privilege_description` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_privilege`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `lx_privilege`
--

INSERT INTO `lx_privilege` (`id_privilege`, `privilege_name`, `sf_privilege`, `privilege_description`) VALUES
(1, 'View', 'view', 'Privilege for view content'),
(2, 'Insert', 'insert', 'Privilege for insert content'),
(3, 'Update', 'update', 'Privilege for update content'),
(4, 'Delete', 'delete', 'Privilege for delete content');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lx_profile`
--

CREATE TABLE IF NOT EXISTS `lx_profile` (
  `id_profile` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_profile` varchar(100) NOT NULL DEFAULT '',
  `nome_corto` varchar(30) NOT NULL,
  `nome_secretario` varchar(100) NOT NULL DEFAULT '',
  `sexo_secretario` varchar(2) DEFAULT NULL,
  `end_inst` varchar(100) NOT NULL DEFAULT '',
  `telefone` varchar(13) NOT NULL DEFAULT '',
  `email_inst` varchar(100) NOT NULL DEFAULT '',
  `data_cadastro` date NOT NULL,
  `tipo_inst` varchar(20) NOT NULL DEFAULT 'secretaria',
  `permalink` varchar(255) NOT NULL DEFAULT 'secretaria',
  `conteudo` text NOT NULL,
  `mapa` text,
  `biografia` text,
  `foto` varchar(255) DEFAULT NULL,
  `ordem` int(10) unsigned NOT NULL DEFAULT '0',
  `status` set('0','1') NOT NULL DEFAULT '1',
  `clase_profile` varchar(30) NOT NULL,
  PRIMARY KEY (`id_profile`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Volcado de datos para la tabla `lx_profile`
--

INSERT INTO `lx_profile` (`id_profile`, `name_profile`, `nome_corto`, `nome_secretario`, `sexo_secretario`, `end_inst`, `telefone`, `email_inst`, `data_cadastro`, `tipo_inst`, `permalink`, `conteudo`, `mapa`, `biografia`, `foto`, `ordem`, `status`, `clase_profile`) VALUES
(1, 'Root', '', 'Henry Vallenilla', NULL, '', '', 'henryvallenilla@gmail.com', '2013-03-21', 'Desarrollador', '', '', NULL, '', NULL, 1, '1', ''),
(2, 'Admistrador', '', 'Henry Vallenilla', NULL, '', '', 'henryvallenilla@gmail.com', '2013-01-14', '', '', '', '', '<p>Everaldo Francisco da Silva</p>\r\n<p>Everaldo Francisco da Silva, brasileiro, natural de Recife &ndash; Pernambuco, casado, &eacute; formado em Hist&oacute;ria e Pedagogia. Lecionou na Rede P&uacute;blica de Ensino durante 20 anos e ocupou o cargo de Diretor de Escola por 10 anos. Foi vereador de 1997 a 2000 e Ouvidor do munic&iacute;pio de 2009 a 2010. Voltou a ser diretor de escola e assumiu, em 1&ordm; de janeiro de 2013, a Secretaria de Administra&ccedil;&atilde;o. &nbsp;&nbsp;</p>', NULL, 0, '1', ''),
(29, 'Portal Carapicuiba', 'Portal Carapicuiba', 'Administrador', NULL, '', '', '', '2000-03-03', 'secretaria', 'carapicuiba', '', '', '', NULL, 0, '1', 'azul');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lx_profile_module`
--

CREATE TABLE IF NOT EXISTS `lx_profile_module` (
  `id_profile_module` int(11) NOT NULL AUTO_INCREMENT,
  `id_privilege` int(11) NOT NULL,
  `id_profile` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  PRIMARY KEY (`id_profile_module`),
  UNIQUE KEY `privilege_module` (`id_privilege`,`id_module`,`id_profile`),
  KEY `id_perfil` (`id_profile`),
  KEY `id_modulo` (`id_module`),
  KEY `id_privilege` (`id_privilege`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=357 ;

--
-- Volcado de datos para la tabla `lx_profile_module`
--

INSERT INTO `lx_profile_module` (`id_profile_module`, `id_privilege`, `id_profile`, `id_module`) VALUES
(1, 1, 1, 2),
(15, 1, 2, 2),
(2, 1, 1, 3),
(104, 4, 1, 16),
(3, 1, 1, 4),
(7, 1, 1, 5),
(99, 4, 2, 9),
(11, 1, 1, 6),
(25, 1, 1, 8),
(30, 1, 1, 9),
(103, 3, 1, 16),
(38, 1, 1, 10),
(42, 1, 1, 11),
(88, 1, 2, 14),
(53, 1, 1, 12),
(76, 1, 1, 13),
(4, 2, 1, 4),
(8, 2, 1, 5),
(98, 3, 2, 9),
(12, 2, 1, 6),
(26, 2, 1, 8),
(31, 2, 1, 9),
(91, 4, 2, 14),
(39, 2, 1, 10),
(43, 2, 1, 11),
(54, 2, 1, 12),
(77, 2, 1, 13),
(5, 3, 1, 4),
(9, 3, 1, 5),
(97, 2, 2, 9),
(13, 3, 1, 6),
(27, 3, 1, 8),
(32, 3, 1, 9),
(90, 3, 2, 14),
(40, 3, 1, 10),
(44, 3, 1, 11),
(55, 3, 1, 12),
(78, 3, 1, 13),
(6, 4, 1, 4),
(10, 4, 1, 5),
(96, 1, 2, 9),
(14, 4, 1, 6),
(28, 4, 1, 8),
(33, 4, 1, 9),
(89, 2, 2, 14),
(41, 4, 1, 10),
(45, 4, 1, 11),
(56, 4, 1, 12),
(79, 4, 1, 13),
(80, 1, 1, 14),
(81, 2, 1, 14),
(82, 3, 1, 14),
(83, 4, 1, 14),
(84, 1, 1, 15),
(85, 2, 1, 15),
(86, 3, 1, 15),
(87, 4, 1, 15),
(102, 2, 1, 16),
(101, 1, 1, 16),
(100, 1, 2, 3),
(105, 1, 3, 2),
(106, 1, 3, 3),
(107, 1, 4, 2),
(108, 1, 4, 3),
(109, 1, 3, 13),
(110, 3, 3, 13),
(111, 4, 3, 13),
(112, 2, 3, 13),
(113, 1, 3, 9),
(114, 2, 3, 9),
(115, 3, 3, 9),
(116, 4, 3, 9),
(117, 1, 3, 15),
(118, 2, 3, 15),
(119, 3, 3, 15),
(120, 4, 3, 15),
(121, 1, 1, 17),
(122, 2, 1, 17),
(123, 3, 1, 17),
(124, 4, 1, 17),
(125, 1, 1, 18),
(126, 2, 1, 18),
(127, 3, 1, 18),
(128, 4, 1, 18),
(129, 1, 8, 2),
(130, 1, 8, 3),
(131, 1, 8, 13),
(132, 2, 8, 13),
(133, 3, 8, 13),
(134, 4, 8, 13),
(135, 1, 8, 18),
(136, 2, 8, 18),
(137, 3, 8, 18),
(138, 4, 8, 18),
(139, 1, 8, 15),
(140, 2, 8, 15),
(141, 3, 8, 15),
(142, 4, 8, 15),
(143, 1, 8, 17),
(144, 2, 8, 17),
(145, 3, 8, 17),
(146, 4, 8, 17),
(147, 1, 1, 19),
(148, 2, 1, 19),
(149, 3, 1, 19),
(150, 4, 1, 19),
(151, 1, 8, 19),
(152, 3, 8, 19),
(153, 2, 8, 19),
(154, 4, 8, 19),
(155, 1, 1, 20),
(156, 2, 1, 20),
(157, 3, 1, 20),
(158, 4, 1, 20),
(159, 1, 8, 20),
(160, 2, 8, 20),
(161, 3, 8, 20),
(162, 4, 8, 20),
(163, 1, 8, 9),
(164, 2, 8, 9),
(165, 3, 8, 9),
(166, 4, 8, 9),
(167, 1, 3, 18),
(168, 2, 3, 18),
(169, 3, 3, 18),
(170, 4, 3, 18),
(171, 1, 1, 21),
(172, 2, 1, 21),
(173, 3, 1, 21),
(174, 4, 1, 21),
(175, 1, 3, 21),
(176, 3, 3, 21),
(177, 2, 3, 21),
(178, 4, 3, 21),
(179, 1, 10, 2),
(180, 1, 10, 3),
(181, 1, 9, 9),
(182, 2, 9, 9),
(183, 3, 9, 9),
(184, 4, 9, 9),
(185, 1, 9, 13),
(186, 2, 9, 13),
(187, 3, 9, 13),
(188, 4, 9, 13),
(189, 1, 9, 18),
(190, 2, 9, 18),
(191, 3, 9, 18),
(192, 4, 9, 18),
(193, 1, 9, 19),
(194, 2, 9, 19),
(195, 3, 9, 19),
(196, 4, 9, 19),
(197, 1, 9, 20),
(198, 2, 9, 20),
(199, 3, 9, 20),
(200, 4, 9, 20),
(201, 1, 9, 21),
(202, 2, 9, 21),
(203, 3, 9, 21),
(204, 4, 9, 21),
(205, 1, 10, 9),
(206, 2, 10, 9),
(207, 3, 10, 9),
(208, 4, 10, 9),
(209, 1, 10, 13),
(210, 2, 10, 13),
(211, 3, 10, 13),
(212, 4, 10, 13),
(213, 1, 10, 18),
(214, 2, 10, 18),
(215, 3, 10, 18),
(216, 4, 10, 18),
(217, 1, 10, 19),
(218, 2, 10, 19),
(219, 3, 10, 19),
(220, 4, 10, 19),
(221, 1, 10, 20),
(222, 2, 10, 20),
(223, 3, 10, 20),
(224, 4, 10, 20),
(225, 1, 10, 21),
(226, 2, 10, 21),
(227, 3, 10, 21),
(228, 4, 10, 21),
(229, 1, 9, 2),
(230, 1, 9, 3),
(231, 1, 1, 22),
(232, 2, 1, 22),
(233, 3, 1, 22),
(234, 4, 1, 22),
(235, 1, 1, 23),
(236, 2, 1, 23),
(237, 3, 1, 23),
(238, 4, 1, 23),
(239, 1, 1, 24),
(240, 2, 1, 24),
(241, 3, 1, 24),
(242, 4, 1, 24),
(243, 1, 1, 25),
(244, 2, 1, 25),
(245, 3, 1, 25),
(246, 4, 1, 25),
(247, 1, 1, 26),
(248, 2, 1, 26),
(249, 3, 1, 26),
(250, 4, 1, 26),
(251, 1, 3, 26),
(252, 2, 3, 26),
(253, 3, 3, 26),
(254, 4, 3, 26),
(255, 1, 1, 27),
(256, 2, 1, 27),
(257, 3, 1, 27),
(258, 4, 1, 27),
(259, 1, 1, 28),
(260, 2, 1, 28),
(261, 3, 1, 28),
(262, 4, 1, 28),
(263, 1, 3, 28),
(264, 2, 3, 28),
(265, 3, 3, 28),
(266, 4, 3, 28),
(267, 1, 11, 2),
(268, 1, 11, 3),
(269, 1, 12, 2),
(270, 1, 12, 3),
(271, 1, 13, 2),
(272, 1, 13, 3),
(273, 1, 29, 2),
(274, 1, 29, 3),
(275, 1, 2, 0),
(276, 4, 2, 0),
(277, 1, 2, 5),
(278, 4, 2, 5),
(279, 2, 2, 5),
(280, 3, 2, 5),
(281, 1, 2, 13),
(282, 2, 2, 13),
(283, 4, 2, 13),
(284, 3, 2, 13),
(285, 1, 2, 18),
(286, 2, 2, 18),
(287, 4, 2, 18),
(288, 3, 2, 18),
(289, 1, 2, 20),
(290, 2, 2, 20),
(291, 3, 2, 20),
(292, 4, 2, 20),
(293, 1, 2, 21),
(294, 2, 2, 21),
(295, 3, 2, 21),
(296, 4, 2, 21),
(297, 1, 1, 29),
(298, 2, 1, 29),
(299, 3, 1, 29),
(300, 4, 1, 29),
(301, 1, 1, 30),
(302, 2, 1, 30),
(303, 3, 1, 30),
(304, 4, 1, 30),
(305, 1, 1, 31),
(306, 2, 1, 31),
(307, 3, 1, 31),
(308, 4, 1, 31),
(309, 1, 1, 32),
(310, 2, 1, 32),
(311, 3, 1, 32),
(312, 4, 1, 32),
(313, 1, 1, 33),
(314, 2, 1, 33),
(315, 3, 1, 33),
(316, 4, 1, 33),
(317, 1, 1, 34),
(318, 2, 1, 34),
(319, 3, 1, 34),
(320, 4, 1, 34),
(321, 1, 4, 13),
(322, 2, 4, 13),
(323, 3, 4, 13),
(324, 4, 4, 13),
(325, 1, 4, 18),
(326, 2, 4, 18),
(327, 3, 4, 18),
(328, 4, 4, 18),
(329, 1, 4, 9),
(330, 2, 4, 9),
(331, 3, 4, 9),
(332, 4, 4, 9),
(333, 1, 1, 35),
(334, 2, 1, 35),
(335, 3, 1, 35),
(336, 4, 1, 35),
(337, 1, 28, 13),
(338, 3, 28, 13),
(339, 2, 28, 13),
(340, 4, 28, 13),
(341, 1, 2, 33),
(342, 2, 2, 33),
(343, 4, 2, 33),
(344, 3, 2, 33),
(345, 1, 1, 36),
(346, 2, 1, 36),
(347, 3, 1, 36),
(348, 4, 1, 36),
(349, 1, 1, 37),
(350, 2, 1, 37),
(351, 3, 1, 37),
(352, 4, 1, 37),
(353, 1, 1, 38),
(354, 2, 1, 38),
(355, 3, 1, 38),
(356, 4, 1, 38);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lx_user`
--

CREATE TABLE IF NOT EXISTS `lx_user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) DEFAULT NULL,
  `matricula` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `name` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `login` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `password` text COLLATE latin1_spanish_ci,
  `email` varchar(70) COLLATE latin1_spanish_ci NOT NULL,
  `photo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `telefone` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `celular` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `departamento` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `observacoes` text COLLATE latin1_spanish_ci NOT NULL,
  `last_access` datetime DEFAULT NULL,
  `status` set('0','1') COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_perfil` (`id_profile`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `lx_user`
--

INSERT INTO `lx_user` (`id_user`, `id_profile`, `matricula`, `name`, `login`, `password`, `email`, `photo`, `telefone`, `celular`, `departamento`, `observacoes`, `last_access`, `status`) VALUES
(1, 1, '', 'Root', 'root', 'b8b42cb337d1fce718048937c32a81a0', 'info@aberic.com', '1_254397_1015150692047.jpg', '', '', '', '', '2013-08-24 09:57:11', '1'),
(2, 2, '14868322', 'Administrator', 'admin', '93279e3308bdbbeed946fc965017f67a', 'info@aberic.com', '', '', '', '', '', '2013-06-01 12:31:43', '1'),
(3, 28, '14868322', 'Henry Vallenilla', 'hvallenilla', 'e10adc3949ba59abbe56e057f20f883e', 'henryvallenilla@gmail.com', '3_254397_1015150692047.jpg', '', '', '', '', '2013-05-10 14:55:23', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lx_user_module`
--

CREATE TABLE IF NOT EXISTS `lx_user_module` (
  `id_user_module` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  PRIMARY KEY (`id_user_module`),
  KEY `id_user` (`id_user`),
  KEY `id_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `id_newsletter` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id_newsletter`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `newsletter`
--

INSERT INTO `newsletter` (`id_newsletter`, `nombre`, `email`) VALUES
(1, 'Henry Vallenilla', 'henryvallenilla@gmail.com'),
(2, 'henryh', 'henryvallenilla@hotmai.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_album`
--

CREATE TABLE IF NOT EXISTS `sf_album` (
  `id_album` int(10) NOT NULL AUTO_INCREMENT,
  `id_relation` int(10) NOT NULL,
  `album_name` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `resumo` text COLLATE latin1_spanish_ci NOT NULL,
  `data` date NOT NULL,
  `status` set('0','1') COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_album`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_album_access`
--

CREATE TABLE IF NOT EXISTS `sf_album_access` (
  `id_access_album` int(11) NOT NULL AUTO_INCREMENT,
  `id_nucleo` int(11) NOT NULL,
  `id_album` int(11) NOT NULL,
  PRIMARY KEY (`id_access_album`),
  KEY `id_nucleo` (`id_nucleo`),
  KEY `id_album` (`id_album`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_album_content`
--

CREATE TABLE IF NOT EXISTS `sf_album_content` (
  `id_content` int(10) NOT NULL AUTO_INCREMENT,
  `id_album` int(10) NOT NULL,
  `image` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `status` set('0','1') NOT NULL,
  `caption` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_content`),
  KEY `id_album` (`id_album`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_archivos_seccion`
--

CREATE TABLE IF NOT EXISTS `sf_archivos_seccion` (
  `id_archivo_seccion` int(11) NOT NULL AUTO_INCREMENT,
  `id_seccion` int(11) NOT NULL,
  `titulo_archivo` varchar(100) NOT NULL,
  `archivo` varchar(150) NOT NULL,
  PRIMARY KEY (`id_archivo_seccion`),
  KEY `id_seccion` (`id_seccion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_language`
--

CREATE TABLE IF NOT EXISTS `sf_language` (
  `id_language` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `country` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `lang_country` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `principal` set('0','1') COLLATE latin1_spanish_ci DEFAULT '0',
  `status` set('0','1') COLLATE latin1_spanish_ci DEFAULT '0',
  PRIMARY KEY (`id_language`),
  UNIQUE KEY `culture` (`language`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `sf_language`
--

INSERT INTO `sf_language` (`id_language`, `language`, `country`, `lang_country`, `principal`, `status`) VALUES
(1, 'en', 'US', 'en_US', '0', '1'),
(2, 'pt', 'BR', 'pt_BR', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_news`
--

CREATE TABLE IF NOT EXISTS `sf_news` (
  `id_news` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profile` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `summary` text,
  `body` text,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `flag_ultima_noticia` int(10) unsigned NOT NULL DEFAULT '0',
  `image` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `category` int(10) unsigned NOT NULL DEFAULT '0',
  `position_profile` int(1) NOT NULL DEFAULT '0',
  `permalink` text NOT NULL,
  `ordem_destaque` int(2) NOT NULL DEFAULT '99',
  PRIMARY KEY (`id_news`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1449 ;

--
-- Volcado de datos para la tabla `sf_news`
--

INSERT INTO `sf_news` (`id_news`, `id_profile`, `title`, `summary`, `body`, `status`, `flag_ultima_noticia`, `image`, `date`, `category`, `position_profile`, `permalink`, `ordem_destaque`) VALUES
(98, 8, 'Atleta de Carapicuíba busca vaga no mundial de jiu-jitsu', '<p><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;">A atleta de jiu-jitsu da Secretaria de Esportes e Lazer, Roberta Goivinho,&nbsp;est&aacute; de olho no mundial que acontecer&aacute; no m&ecirc;s de junho na cidade de&nbsp;Long Beach, Calif&oacute;rnia, EUA.</span></p>', '<p style="text-align: justify; font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">A expectativa da jovem de 21 anos&nbsp;&eacute; consequ&ecirc;ncia dos resultados expressivos obtidos nas &uacute;ltimas competi&ccedil;&otilde;es que participou:&nbsp;4&ordm; lugar no Panamericano de Irvine, Calif&oacute;rnia&nbsp;e&nbsp;3&ordf; coloca&ccedil;&atilde;o no Campeonato Paulista realizado neste domingo (25) no Complexo Esportivo Baby Barioni em S&atilde;o Paulo.</span></p>\r\n<p style="text-align: justify; font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;"><img id="imagem_1_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/98_imagem_1.jpg" alt="" width="200" height="150" /></span></p>\r\n<p style="text-align: justify; font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">Roberta esclarece que est&aacute; vivendo uma &oacute;tima fase e o apoio do povo e da prefeitura&nbsp;de Carapicu&iacute;ba tem contribu&iacute;do para o seu bom desempenho nas competi&ccedil;&otilde;es.</span></p>', 1, 0, '', '2011-05-20', 0, 0, 'atleta-de-carapicuiba-busca-vaga-no-mundial-de-jiu-jitsu', 99),
(99, 15, 'Carapicuíba participará da 1ª Plenária Regional de Saúde Mental Intersetorial', '<p><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;">A 1&ordf; Plen&aacute;ria Regional de Sa&uacute;de Mental Intersetorial acontecer&aacute; nos dia 29 de Abril das 8 &agrave;s 17 horas, no Sindicato dos Metal&uacute;rgicos de Osasco.</span></p>', '<p style="text-align: justify;"><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">A 1&ordf; Plen&aacute;ria Regional de Sa&uacute;de Mental Intersetorial acontecer&aacute; nos dia 29 de Abril das 8 &agrave;s 17 horas, no Sindicato dos Metal&uacute;rgicos de Osasco, sito &agrave; Rua Erasmo Braga n&ordm;31&nbsp;- Presidente Altino - Osasco.<br />&nbsp;<br />Embora o Estado de S&atilde;o Paulo, contrariando a tend&ecirc;ncia dos demais estados brasileiros, n&atilde;o tenha se mobilizado para realizar a confer&ecirc;ncia estadual, os munic&iacute;pios paulistas, diante da import&acirc;ncia do tema, reuniram a popula&ccedil;&atilde;o em suas plen&aacute;rias. Carapicu&iacute;ba realizou a confer&ecirc;ncia no in&iacute;cio do m&ecirc;s de abril.<br />&nbsp;<br />De acordo com a orienta&ccedil;&atilde;o do Conselho Nacional de Sa&uacute;de, a Plen&aacute;ria Regional de Sa&uacute;de Mental Intersetorial tem como objetivo referendar os delegados escolhidos nas confer&ecirc;ncias realizadas nos 17 Munic&iacute;pios da regi&atilde;o dos mananciais e da Rota dos Bandeirantes.<br />&nbsp;<br />A etapa seguinte seria a Confer&ecirc;ncia Estadual. Na aus&ecirc;ncia de mobiliza&ccedil;&atilde;o por parte do Estado de S&atilde;o Paulo, o F&oacute;rum das entidades Pr&oacute;-Confer&ecirc;ncia de Sa&uacute;de Mental tomou a iniciativa e aprovou a realiza&ccedil;&atilde;o da plen&aacute;ria regional, que acontecer&aacute; em S&atilde;o Bernardo do Campo.<br />&nbsp;<br />Esse encontro, chamado &agrave; revelia do Governo de S&atilde;o Paulo, ir&aacute; reunir delegados municipais e regionais, para referendar por fim os delegados que participar&atilde;o do IV Congresso Nacional de Sa&uacute;de Mental Intersetorial, que acontecer&aacute; em Bras&iacute;lia, nos dias 27 e 30 de junho.<br />&nbsp;<br />O tema central da confer&ecirc;ncia ser&aacute; &ldquo;Sa&uacute;de Mental, direito e compromisso de todos: Consolidar avan&ccedil;os e enfrentar desafios&rdquo;. Entre os eixos que ser&atilde;o objetos de debate na Confer&ecirc;ncia, est&atilde;o Sa&uacute;de Mental e Pol&iacute;ticas de Estado: pactuar caminhos intersetoriais.</span></span></p>\r\n<p style="text-align: justify;">&nbsp;</p>\r\n<div style="text-align: justify;"><span style="font-size: small; line-height: 115%; font-family: tahoma;"><span style="font-size: small;"><strong>Texto: Hyde Pedreira</strong><br /><strong>Foto: Diego Bustamante</strong></span></span></div>', 1, 0, '', '2011-05-20', 0, 0, 'carapicuiba-participara-da-1o-plenaria-regional-de-saude-mental-intersetorial', 99),
(100, 8, '1º Torneio Intermunicipal de Futsal Feminino e Masculino Sub-21', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">A Secretaria de Esportes e Lazer de Carapicu&iacute;ba est&aacute; promovendo o 1&ordm; Campeonato Intermunicipal de Futsal para garotas e meninos com menos de 21 anos.</span></p>', '<p style="text-align: justify;"><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">A Secretaria de Esportes e Lazer de Carapicu&iacute;ba </span>est&aacute; promovendo o 1&ordm;&nbsp;Campeonato Intermunicipal de Futsal para garotas e meninos com menos de 21 anos, cujo&nbsp;in&iacute;cio se deu neste domingo (25), no Gin&aacute;sio de Esportes Tancredo Neves.<br /><br />Os resultados da Primeira Rodada do&nbsp;Masculino&nbsp;foram: Carapicu&iacute;ba 9 x 4 Pirapora do Bom Jesus e Santana de Parna&iacute;ba 9 x 3 Cajamar.<br /><br />A pr&oacute;xima rodada acontece dia 1&ordm; de maio, a partir das 15h no Gin&aacute;sio Tancred&atilde;o com as seguintes partidas: Santana de Parna&iacute;ba X Pirapora do Bom Jesus e Carapicu&iacute;ba X Cajamar.<br /><br />J&aacute; o Feminino apresentou o seguinte placar: Santana de Parna&iacute;ba 6 X 4 Cajamar e Carapicu&iacute;ba 1 X 4 Pirapora do Bom Jesus. <br /><br />Os pr&oacute;ximos jogos s&atilde;o: Santana de Parna&iacute;ba X Pirapora do Bom Jesus e Carapicu&iacute;ba X Cajamar, a partir das 17h no Tancred&atilde;o.</span></span></p>', 1, 0, '', '2011-05-20', 0, 0, '1o-torneio-intermunicipal-de-futsal-feminino-e-masculino-sub-21', 99),
(101, 8, 'Secretário de Esportes de Carapicuíba recebe prêmio de melhor do ano', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">O Secret&aacute;rio de Esportes e Lazer de Carapicu&iacute;ba, Eduardo Ara&uacute;jo, recebeu o pr&ecirc;mio de &ldquo;Melhores do ano de 2009&rdquo; em evento promovido pela FPFS (Federa&ccedil;&atilde;o Paulista de Futebol de Sal&atilde;o) neste domingo (25).</span></p>', '<p style="text-align: justify;"><span style="font-size: small;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">O Secret&aacute;rio de Esportes e Lazer de Carapicu&iacute;ba, Eduardo Ara&uacute;jo, recebeu o pr&ecirc;mio de &ldquo;Melhores do ano de 2009&rdquo; em evento promovido pela FPFS (Federa&ccedil;&atilde;o Paulista de Futebol de Sal&atilde;o) neste domingo (25).<br /><br /><br /><img id="imagem_2_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/101_imagem_2.jpg" alt="" width="200" height="150" /></span></span>&nbsp;</p>\r\n<div id="conteudo1">\r\n<p class="MsoNormal" style="text-align: justify; line-height: normal;"><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">A entrega da honraria&nbsp;aconteceu no Sal&atilde;o de Festas Golden House, que fica na Av. Condessa Elizabeth Rubiano, 2100, Penha (ao lado do Gin&aacute;sio da FPFS) e contou com a presen&ccedil;a do presidente da entidade Ciro Font&atilde;o, o vereador Toninho Paiva, a deputada Vanessa Damo, al&eacute;m de outras autoridades e empres&aacute;rios ligados ao esporte. <br /></span></span></p>\r\n<p class="MsoNormal" style="text-align: justify; line-height: normal;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Confira a rela&ccedil;&atilde;o dos&nbsp;premiados em: </span></span></p>\r\n<span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><a href="http://www.futsalpaulista.com.br/noticias/noticia.asp" target="_blank"><span style="line-height: 115%; color: blue;"><span style="font-size: small;">http://www.futsalpaulista.com.br/noticias/noticia.asp</span></span></a></span></div>', 1, 0, '', '2011-05-20', 0, 0, 'secretario-de-esportes-de-carapicuiba-recebe-premio-de-melhor-do-ano', 99),
(102, 15, 'Prefeitura de Carapicuíba participa de Congresso de Secretários Municipais de Saúde', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Prefeitura do Munic&iacute;pio de Carapicu&iacute;ba participa entre os dias 21 e 23 de abril do XXIV Congresso de Secret&aacute;rios Municipais de Sa&uacute;de do Estado de S&atilde;o Paulo, que est&aacute; sendo realizado em Campinas, no Hotel The Royal Palm Plaza.</span></p>', '<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Prefeitura do Munic&iacute;pio de Carapicu&iacute;ba participa entre os dias 21 e 23 de abril do XXIV Congresso de Secret&aacute;rios Municipais de Sa&uacute;de do Estado de S&atilde;o Paulo, que est&aacute; sendo realizado em Campinas, no Hotel The Royal Palm Plaza. O&nbsp; tema desse Congresso ser&aacute; &ldquo;Os Munic&iacute;pios Construindo redes de SUS&rdquo;. Haver&aacute; tamb&eacute;m&nbsp;a IX Mostra de Experi&ecirc;ncias Exitosas dos Munic&iacute;pios.</span></span></p>\r\n<div style="text-align: justify;">\r\n<p class="MsoNormal" style="line-height: normal; text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">A Secretaria da Sa&uacute;de de Carapicu&iacute;ba apresentar&aacute; cinco (05) trabalhos: odontologia em ambiente hospitalar, equoterapia e o atendimento de pacientes especiais, o regate do programa Bolsa Fam&iacute;lia, pr&aacute;ticas de educa&ccedil;&atilde;o permanente em sa&uacute;de, e o carnaval e DST/AIDS. O II Pr&ecirc;mio David Capistrano de Experi&ecirc;ncias &Ecirc;xitos na &Aacute;rea de Sa&uacute;de ser&aacute; concedido aos 10 melhores trabalhos.</span></span></p>\r\n<p class="MsoNormal" style="line-height: normal; text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">T&eacute;cnicos da Secretaria da Sa&uacute;de de Carapicu&iacute;ba participar&atilde;o dos dez cursos de capacita&ccedil;&atilde;o oferecidos pelo Congresso, cujos temas s&atilde;o Aten&ccedil;&atilde;o B&aacute;sica e Gest&atilde;o do Cuidado; contratualiza&ccedil;&atilde;o de Servi&ccedil;os de Sa&uacute;de; Controle Social e Gest&atilde;o Participativa; Determinantes Sociais da Sa&uacute;de e Promo&ccedil;&atilde;o;&nbsp; Gest&atilde;o das Redes de Urg&ecirc;ncia; Gest&atilde;o do Fundo e crit&eacute;rios de repasse do Recurso Federal nos 5 Blocos; Planejamento e Informa&ccedil;&atilde;o; 08- Pr&aacute;ticas Integrativas;&nbsp; Regula&ccedil;&atilde;o da Aten&ccedil;&atilde;o &agrave; Sa&uacute;de; Vigil&acirc;ncia em Sa&uacute;de.</span></span></p>\r\n</div>\r\n<div style="text-align: justify;"><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">A atividade al&eacute;m de proporcionar &agrave; Secretaria de Sa&uacute;de uma&nbsp; excelente oportunidade para a expor, as suas experi&ecirc;ncias e os seus projetos tamb&eacute;m possibilitar&aacute; ao Munic&iacute;pio conhecer solu&ccedil;&otilde;es inovadoras que refor&ccedil;ar&aacute; a amplia&ccedil;&atilde;o a efetiva&ccedil;&atilde;o do Sistema &Uacute;nico de Sa&uacute;de (SUS) na Cidade.</span></span></div>\r\n<div style="text-align: justify;"><strong><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><br /><br /><span style="font-size: small;">Texto de Hyde Pedreira</span></span></strong></div>', 1, 0, '', '2011-05-20', 0, 0, 'prefeitura-de-carapicuiba-participa-de-congresso-de-secretarios-municipais-de-saude', 99),
(104, 13, 'Fundo Social promove a 1ª Feira do Dia das Mães', '<p><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;">De 26 de abril a 8 de maio, durante todo o dia, o Fundo Social de Solidariedade realizar&aacute;, no Cal&ccedil;ad&atilde;o de Carapicu&iacute;ba, a 1&ordf; Feira do Dia das M&atilde;es. </span></p>', '<p style="text-align: justify;"><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">De 26 de abril a 8 de maio, durante todo o dia, o Fundo Social de Solidariedade realizar&aacute;, no Cal&ccedil;ad&atilde;o de Carapicu&iacute;ba, a 1&ordf; Feira do Dia das M&atilde;es. </span></span></p>\r\n<div style="text-align: justify;">\r\n<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Os trabalhos s&atilde;o confeccionados pelos alunos de entidades parceiras da Secretaria da Promo&ccedil;&atilde;o Social e do Fundo Social de Solidariedade.</span></span></p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">As barracas colocar&atilde;o &agrave; venda tape&ccedil;aria em barbantes, panos de prato com detalhes em pintura e croch&ecirc;, chinelos com pedrarias, jogos de almo&ccedil;o americano, patchwork, bolsas artesanais, e muito mais.</span></span></p>\r\n<span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><span><span style="font-size: small;">A presidente do Fundo Social de Solidariedade, &Aacute;urea Rodrigues Silva afirmou que os recursos arrecadados na Feira ser&atilde;o usados para as atividades do Fundo Social, como o Casamento Comunit&aacute;rio em junho e a Campanha do Agasalho. &ldquo;As a&ccedil;&otilde;es do Fundo s&atilde;o para beneficiar a popula&ccedil;&atilde;o menos favorecida, e contamos com o apoio da sociedade, atrav&eacute;s de doa&ccedil;&otilde;es e apoio aos nossos eventos&rdquo;, reiterou.</span></span></span></div>', 1, 0, '', '2011-05-20', 0, 0, 'fundo-social-promove-a-1o-feira-do-dia-das-maes', 99),
(105, 10, 'Prefeito e comitiva visitam obras na cidade', '<p><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;">Na &uacute;ltima sexta-feira (16), o prefeito Sergio Ribeiro e secret&aacute;rios percorreram a cidade, em vistoria a obras terminadas ou em execu&ccedil;&atilde;o.</span></p>', '<p style="text-align: justify;"><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">Na &uacute;ltima sexta-feira (16), o prefeito Sergio Ribeiro e secret&aacute;rios percorreram a cidade, em vistoria a obras terminadas ou em execu&ccedil;&atilde;o. Em um dia azafamado, a comitiva esteve no Parque da Lagoa, no Rodoanel, vistoriou obras do PAC, empreendimentos da Alphaville Urbanismo e visitou uma creche rec&eacute;m conclu&iacute;da.</span></span></p>\r\n<div id="conteudo1" style="text-align: justify;">\r\n<p class="MsoNormal" style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O parque da Lagoa, uma obra realizada em parceria com o Governo do Estado, ter&aacute; uma &aacute;rea de 134 metros quadrados, onde ser&aacute; constru&iacute;do um Centro de Conviv&ecirc;ncia, um Centro Comunit&aacute;rio, um Pavilh&atilde;o de Eventos e um Viveiro para educa&ccedil;&atilde;o ambiental.</span></span></p>\r\n<p class="MsoNormal" style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Obra importante para o munic&iacute;pio a execu&ccedil;&atilde;o da primeira fase das obras do PAC tamb&eacute;m foi objeto da vistoria do prefeito.&nbsp; O projeto realizado em parceria com o Governo Federal prev&ecirc; a canaliza&ccedil;&atilde;o do c&oacute;rrego e a edifica&ccedil;&atilde;o de cento e noventa e cinco unidades. Na &aacute;rea ao longo do c&oacute;rrego, ainda ser&aacute; constru&iacute;da uma avenida que interligar&aacute; o centro aos bairros, diminuindo o tr&acirc;nsito na Avenida Inoc&ecirc;ncio Ser&aacute;fico.</span></span></p>\r\n<p class="MsoNormal" style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Lan&ccedil;ado no ano passado, o empreendimento da Alphaville Urbanismo na regi&atilde;o da Granja Viana cujos lotes foram todos vendidos em apenas dois dias tamb&eacute;m recebeu visita da comitiva. Naquela regi&atilde;o, como contrapartida ao impacto do empreendimento, a empresa construir&aacute; uma nova via de acesso a Raposo Tavares, al&eacute;m de duplicar a Avenida S&atilde;o Camilo.</span></span></p>\r\n<p class="MsoNormal" style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Outro compromisso contra&iacute;do pela Alphaville Urbanismo, a creche da Estrada do Aderno, que, depois de dezenove anos ficou paralisada, foi finalizada pela empresa tamb&eacute;m foi vistoriada. Na seq&uuml;&ecirc;ncia, o prefeito esteve no Parque Jandaia, onde est&aacute; sendo constru&iacute;da uma nova sa&iacute;da para o Rodoanel, e na Esta&ccedil;&atilde;o de Carapicu&iacute;ba, para conhecer o projeto de readequa&ccedil;&atilde;o para que transformar a linha de trem e metr&ocirc; de superf&iacute;cie.</span></span></p>\r\n<p class="MsoNormal" style="text-align: justify;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">Foto: Gilberto Cerri</span></span></strong></p>\r\n</div>', 1, 0, '', '2011-05-20', 0, 0, 'prefeito-e-comitiva-visitam-obras-na-cidade', 99),
(108, 13, 'Prefeitura realiza Seminário para discutir assistência social', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">No dia 14 de maio, a partir das oito horas, a Prefeitura de Carapicu&iacute;ba realizar&aacute; o I Semin&aacute;rio Municipal de Assist&ecirc;ncia Social, no Teatro Jorge Amado.</span></p>', '<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">No dia 14 de maio, a partir das oito horas, a Prefeitura de Carapicu&iacute;ba realizar&aacute; o I Semin&aacute;rio Municipal de Assist&ecirc;ncia Social, no Teatro Jorge Amado.</span></span></p>\r\n<div style="text-align: justify;">\r\n<p class="MsoNormal" style="text-align: justify; margin-bottom: 10pt;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Com o tema "Conquistas e Desafios do SUAS - Sistema &Uacute;nico de Assist&ecirc;ncia Social", o semin&aacute;rio ser&aacute; uma oportunidade para discuss&atilde;o entre a sociedade civil e agentes p&uacute;blicos, no sentido de contribuir para a elabora&ccedil;&atilde;o e aperfei&ccedil;oamento de uma pol&iacute;tica de assist&ecirc;ncia social no munic&iacute;pio.</span></span></p>\r\n<p class="MsoNormal" style="text-align: justify; margin-bottom: 10pt;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O Teatro Municipal Jorge Amado fica na Avenida Mirian, 94 - Centro.</span></span></p>\r\n</div>', 1, 0, '', '2011-05-20', 0, 0, 'prefeitura-realiza-seminario-para-discutir-assistencia-social', 99),
(109, 3, 'Vestibular da Fatec recebe inscrições a partir desta sexta', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">As inscri&ccedil;&otilde;es do Vestibular para o 2&deg; semestre da Faculdade de Tecnologia (Fatec) do Estado de S&atilde;o Paulo come&ccedil;am no dia 07 de maio.</span></p>', '<p><span style="font-size: small;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><em>Prazo vai de 7 de maio at&eacute; as 15h00 de 8 de junho somente pela internet</em></span><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;">&nbsp;</span></span></p>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">&nbsp;</span></span></div>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">As inscri&ccedil;&otilde;es do Vestibular para o 2&deg; semestre da Faculdade de Tecnologia (Fatec) do Estado de S&atilde;o Paulo come&ccedil;am no dia 07 de maio. Na unidade de Carapicu&iacute;ba s&atilde;o oferecidas 360 vagas, sendo 80 para tecn&oacute;logo em Log&iacute;stica distribu&iacute;das em dois per&iacute;odos (manh&atilde; e noite), 240 para tecn&oacute;logo em An&aacute;lise de Sistemas e Tecnologia da Informa&ccedil;&atilde;o, em tr&ecirc;s per&iacute;odos (manh&atilde;, tarde e noite) e 40 para Secretariado &agrave; tarde.</span></span></div>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">&nbsp;</span></span></div>\r\n<div style="text-align: justify;"><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">No dia 7 tamb&eacute;m ser&aacute; divulgado o resultado do programa da isen&ccedil;&atilde;o ou redu&ccedil;&atilde;o de 50% na taxa de inscri&ccedil;&atilde;o do Vestibular. O candidato que receber um dos benef&iacute;cios deve fazer sua inscri&ccedil;&atilde;o para o processo seletivo das Fatec, exclusivamente pela internet, no site <span style="color: #000080;"><strong><a href="http://www.vestibularfatec.com.br/" target="_blank">www.vestibularfatec.com.br</a></strong></span>. O Manual do Candidato tamb&eacute;m estar&aacute; dispon&iacute;vel neste site.</span></span></div>\r\n<div style="text-align: justify;"><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><br /><span style="font-size: small;">&nbsp;</span></span></div>\r\n<div style="text-align: justify;"><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">O valor da taxa de inscri&ccedil;&atilde;o para o Vestibular &eacute; R$ 70. O candidato isento do pagamento da taxa deve efetuar sua inscri&ccedil;&atilde;o para a Fatec na qual obteve a isen&ccedil;&atilde;o, em um &uacute;nico curso de gradua&ccedil;&atilde;o, no per&iacute;odo oferecido pela unidade de ensino.</span></span></div>', 1, 0, '', '2011-05-20', 0, 0, 'vestibular-da-fatec-recebe-inscricoes-a-partir-desta-sexta', 99),
(112, 8, 'Carapicuíba participa do Dia Mundial do Desafio', '<p><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;">Em </span><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;">sorteio eletr&ocirc;nico realizado no dia 3/5 no SESC Pinheiros, que contou a presen&ccedil;a de representantes da Secretaria de Esportes e Lazer.</span></p>', '<p style="text-align: justify;"><span style="font-size: small;"><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;">Em </span><span style="font-size: small; font-family: tahoma,arial,helvetica,sans-serif;">sorteio eletr&ocirc;nico realizado no dia 3/5 no SESC Pinheiros, que contou a presen&ccedil;a de representantes da Secretaria de Esportes e Lazer, o munic&iacute;pio de Carapicu&iacute;ba foi sorteado para&nbsp;competir com a cidade de Nicolas Romero, do M&eacute;xico, a fim de motivar o maior percentual de habitantes para participar do Dia Mundial do Desafio, que acontece no dia 26 de maio, com o tema: &ldquo;Voc&ecirc; se mexe e o mundo mexe junto&rdquo;.</span></span></p>\r\n<div id="conteudo1" style="text-align: justify;">\r\n<p class="MsoNormal" style="line-height: normal; text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;"><span>O Dia do Desafio foi criado em 1983, no Canad&aacute;, com o objetivo de incentivar a pr&aacute;tica regular de atividades f&iacute;sicas em benef&iacute;cio da sa&uacute;de. O evento &eacute; difundido mundialmente pela TAFISA - The Association For International Sport for All - entidade de promo&ccedil;&atilde;o do esporte para todos, sediada na Alemanha. </span><span>&nbsp;</span></span></span></p>\r\n<span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">O SESC-SP coordena o evento no Continente Americano, que acontece anualmente na &uacute;ltima quarta-feira do m&ecirc;s de maio, por meio de atividades desenvolvidas pelas Secretarias Municipais de Esportes, Educa&ccedil;&atilde;o e Sa&uacute;de e com apoio da comunidade de cada munic&iacute;pio para promover pelo menos 15 minutos de exerc&iacute;cios f&iacute;sicos.</span></span></div>', 1, 0, '', '2011-05-20', 0, 0, 'carapicuiba-participa-do-dia-mundial-do-desafio', 99),
(114, 8, 'Judô avança em Carapicuíba', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="line-height: 115%;">A Secretaria de Esportes e Lazer de Carapicu&iacute;ba est&aacute; com inscri&ccedil;&otilde;es abertas</span> para alunos de 7 a 35 anos (feminino e masculino) interessados em desenvolver a filosofia e as t&eacute;cnicas do jud&ocirc;.</span></p>', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;"><span style="line-height: 115%;">A Secretaria de Esportes e Lazer de Carapicu&iacute;ba est&aacute; com inscri&ccedil;&otilde;es abertas</span> para alunos de 7 a 35 anos (feminino e masculino) interessados em desenvolver a filosofia e as t&eacute;cnicas do jud&ocirc;.</span></span></p>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">&nbsp;</span></span></div>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">A primeira turma da milenar arte marcial iniciou as atividades em setembro do ano passado e j&aacute; fez a troca de faixas, passando de branca para azul claro, de acordo com a escala de gradua&ccedil;&atilde;o da categoria.</span></span></div>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">&nbsp;</span></span></div>\r\n<div style="text-align: justify;"><span style="font-size: small; line-height: 115%; font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">Os interessados em praticar jud&ocirc; devem procurar a Secretaria de Esportes e Lazer, que fica na Av. Inoc&ecirc;ncio Ser&aacute;fico, 2005 &ndash; Telefone 4187-7000.</span></span></div>', 1, 0, '', '2011-05-20', 0, 0, 'judo-avanca-em-carapicuiba', 99),
(115, 10, 'Ciclo de discussão do Orçamento Participativo começa neste sábado', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Neste s&aacute;bado (8), come&ccedil;a o ciclo de discuss&atilde;o do Or&ccedil;amento Participativo, com plen&aacute;rias na regi&atilde;o do Centro e Corintinha e na regi&atilde;o da COHAB.</span></p>', '<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Neste s&aacute;bado (8), come&ccedil;a o ciclo de discuss&atilde;o do Or&ccedil;amento Participativo, com plen&aacute;rias na regi&atilde;o do Centro e Corintinha e na regi&atilde;o da COHAB.</span></span></p>\r\n<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">\r\n<div style="text-align: justify;"><span style="font-size: medium; color: #000000;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: medium;">Programa&ccedil;&atilde;o:</span></span></strong></span>\r\n<p style="text-align: left;"><span style="color: #006100;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">8 DE MAIO (s&aacute;bado)</span></span></strong></span><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">10h &ndash; Plen&aacute;ria da regi&atilde;o 1 (Centro/Corintinha)<br />15h &ndash; Plen&aacute;ria da regi&atilde;o 2 (Cohab)</span></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;"><span style="color: #006100;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">22 DE MAIO (s&aacute;bado)</span></strong></span><br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">10h &ndash; Plen&aacute;ria da regi&atilde;o 3 (Vila Lourdes Cidade Ariston / Vila Sulamericana)</span><br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">15h &ndash; Plen&aacute;ria da regi&atilde;o 4 (Vila Crett / Vila Menck Vila Capriott / Roseira Parque)</span></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;"><span style="color: #006100;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">29 DE MAIO (s&aacute;bado)</span></strong></span><br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">10h &ndash; Plen&aacute;ria da regi&atilde;o 5 (Vila Marcondes/S&atilde;o Daniel)</span><br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">15h &ndash; Plen&aacute;ria da regi&atilde;o 6 (Pque Jandaia / Parque Industrial / Vila Dirce / Vila Veloso / Jd Gopiuva)</span><br /></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;"><span style="color: #006100;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">5 DE JUNHO (s&aacute;bado)</span></strong></span><br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">10h &ndash; Plen&aacute;ria da regi&atilde;o 7 (Jd. Maria Beatriz / Jd. Ang&eacute;lica / Jd. Ana Estela / Jd. Planalto / Jd. Tonato)</span><br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">15h &ndash; Plen&aacute;ria da regi&atilde;o 8 (Aldeia / Novo Horizonte / Parque Santa Tereza)</span></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;"><span style="color: #006100;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">12 DE JUNHO (s&aacute;bado)</span></strong></span><br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">15h &ndash; Plen&aacute;ria Geral &ndash; Teatro Jorge Amado</span></span></p>\r\n</div>\r\n</span></span></p>', 1, 0, '', '2011-05-20', 0, 0, 'ciclo-de-discussao-do-orcamento-participativo-comeca-neste-sabado', 99),
(116, 8, 'Futsal feminino de Carapicuíba se reabilita', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Ap&oacute;s estrear com derrota para Pirapora pelo placar de (4 a 1) a equipe de futsal feminino da Secretaria de Esportes e Lazer de Carapicu&iacute;ba&nbsp; se reabilitou ao vencer a equipe de Cajamar por 5 a 2 na rodada do s&aacute;bado (1&ordm;) do Campeonato Intermunicipal de Futsal Feminino.</span></p>', '<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Ap&oacute;s estrear com derrota para Pirapora pelo placar de (4 a 1) a equipe de futsal feminino da Secretaria de Esportes e Lazer de Carapicu&iacute;ba&nbsp; se reabilitou ao vencer a equipe de Cajamar por 5 a 2 na rodada do s&aacute;bado (1&ordm;) do Campeonato Intermunicipal de Futsal Feminino. </span></span></p>\r\n<div id="conteudo1" style="text-align: justify;">\r\n<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">A vit&oacute;ria deu novo &acirc;nimo &agrave; equipe de Carapicu&iacute;ba que agora enfrenta a sele&ccedil;&atilde;o de Santana de Parna&iacute;ba no pr&oacute;ximo s&aacute;bado (8), a partir das 17h no Tancred&atilde;o.</span></span></p>\r\n<span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">&Eacute; de gra&ccedil;a e vale a pena torcer pelas meninas.</span></span></div>', 1, 0, '', '2011-05-20', 0, 0, 'futsal-feminino-de-carapicuiba-se-reabilita', 99),
(117, 8, 'Campeonato Intermunicipal de Futebol de Campo Feminino', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Pela 2&ordf; rodada do Campeonato Intermunicipal de Futebol de Campo Feminino promovido pela Sec. de Esportes e Lazer, a sele&ccedil;&atilde;o de Carapicu&iacute;ba perdeu para o Gr&ecirc;mio Campo Grande de S&atilde;o Paulo pelo placar de 3 a 2.</span></p>', '<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Pela segunda rodada do Campeonato Intermunicipal de Futebol de Campo Feminino promovido pela Secretaria de Esportes e Lazer, a sele&ccedil;&atilde;o de Carapicu&iacute;ba perdeu para o Gr&ecirc;mio Campo Grande de S&atilde;o Paulo pelo placar de 3 a 2. J&aacute; o time de Cotia goleou a equipe do Floresta/Embu-Gua&ccedil;u por 11 a 0.&nbsp; </span></span></p>\r\n<div id="conteudo1" style="text-align: justify;">\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Em virtude da Comemora&ccedil;&atilde;o ao Dia das M&atilde;es neste domingo (9), n&atilde;o haver&aacute; rodada para o futebol. Os pr&oacute;ximos jogos acontecem no dia 16 de maio com as partidas envolvendo Secel Carapicu&iacute;ba X Floresta/Embu-Gua&ccedil;u e Cotia X Embu das Artes, a partir das 13h30 no Est&aacute;dio do Niter&oacute;i (Cohab 2), com entrada franca.</span></span></p>\r\n</div>', 1, 0, '', '2011-05-20', 0, 0, 'campeonato-intermunicipal-de-futebol-de-campo-feminino', 99),
(118, 27, 'Festa do 1º de Maio reúne 100 mil trabalhadores no Parque do Planalto', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Comemora&ccedil;&atilde;o &eacute; realizada pelo 6&ordm; ano consecutivo, com organiza&ccedil;&atilde;o da central sindical UGT, em parceria com o vice-prefeito Salim Reis e apoio institucional da Prefeitura de Carapicu&iacute;ba.</span></p>', '<p style="text-align: justify;">&nbsp;<em><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Comemora&ccedil;&atilde;o &eacute; realizada pelo 6&ordm; ano consecutivo, com organiza&ccedil;&atilde;o da central sindical UGT, em parceria com o vice-prefeito Salim Reis e apoio institucional da Prefeitura de Carapicu&iacute;ba.</span></span></em></p>\r\n<div id="conteudo1">\r\n<p style="text-align: justify;">&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Cerca de 100 mil pessoas compareceram ao Parque do Planalto no dia 1&ordm; de Maio &ndash; Dia do Trabalhador &ndash; para prestigiar a grande festa, que pelo sexto ano consecutivo &eacute; organizada pela central sindical UGT, em parceria com o vice-prefeito de Carapicu&iacute;ba, Salim Reis e conta com o apoio institucional da Prefeitura do Munic&iacute;pio.<br /><br /><img id="imagem_2_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/118_imagem_2.jpg" alt="" width="200" height="150" /></span></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Atra&ccedil;&otilde;es musicais animaram o p&uacute;blico a partir das 10h, no local, e o evento foi encerrado com um show do cantor Belo.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Durante o evento foram sorteados pr&ecirc;mios entre o p&uacute;blico participante, e estandes montados ao lado do palco ofereceram servi&ccedil;os gratuitos &agrave; popula&ccedil;&atilde;o, como corte de cabelo, atendimento jur&iacute;dico e uma palestra oferecida pela AES Eletropaulo sobre seguran&ccedil;a el&eacute;trica.<br /><br /><img id="imagem_3_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/118_imagem_3.jpg" alt="" width="200" height="150" /></span></span></p>\r\n<p style="text-align: justify;">&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O prefeito Sergio Ribeiro saldou os trabalhadores e trabalhadoras,&nbsp; ressaltando a import&acirc;ncia da atua&ccedil;&atilde;o de todos para o desenvolvimento da cidade, e os esfor&ccedil;os da Administra&ccedil;&atilde;o Municipal no sentido da abertura de novos postos de trabalho.<br /><br /><img id="imagem_4_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/118_imagem_4.jpg" alt="" width="200" height="150" /><br /></span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O vice-prefeito Salim Reis, destacou as conquistas cl&aacute;ssicas alcan&ccedil;adas pelos trabalhadores ao longo dos tempos, chamando a aten&ccedil;&atilde;o para o papel das centrais sindicais na manuten&ccedil;&atilde;o dos direitos adquiridos.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Tamb&eacute;m compareceram ao evento autoridades locais e regionais, dentre as quais, o presidente da C&acirc;mara Municipal de Carapicu&iacute;ba, Isac Reis, e secret&aacute;rios da Prefeitura de Carapicu&iacute;ba.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-size: small;">&nbsp;</span>\r\n<p>&nbsp;</p>\r\n<span style="font-size: small;">\r\n<hr />\r\n</span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="color: #006100;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Carapicu&iacute;ba se destaca na cria&ccedil;&atilde;o de novos postos de trabalho</span></span></strong></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O munic&iacute;pio tem se destacado, desde 2009, como um dos que mais recolocam trabalhadores no mercado de trabalho. Nesse sentido, o Posto de Atendimento ao Trabalhador (PAT), tem grande atua&ccedil;&atilde;o na regi&atilde;o Oeste do Estado de S&atilde;o Paulo.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Nos primeiros meses de 2009, entre janeiro e o final de maio, mais de 600 profissionais conseguiram seu emprego no mercado formal.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Patenteando uma pol&iacute;tica que prioriza o trabalhador, a a&ccedil;&atilde;o do PAT conseguiu empregar, somente em maio do ano passado, 342 trabalhadores.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O PAT &eacute; um servi&ccedil;o disponibilizado pela Prefeitura, atrav&eacute;s da Secretaria de Desenvolvimento Econ&ocirc;mico, Social e Trabalho, e pelo Governo do Estado de S&atilde;o Paulo, cujo objetivo &eacute; recolocar trabalhadores desempregados que moram em Carapicu&iacute;ba no mercado de trabalho.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O PAT Carapicu&iacute;ba tamb&eacute;m oferece servi&ccedil;os de qualifica&ccedil;&atilde;o profissional, emiss&atilde;o de carteira de trabalho, e conta com o programa Jovem Cidad&atilde;o.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Em junho de 2009 mais 758 moradores de Carapicu&iacute;ba encontraram emprego. O saldo das recoloca&ccedil;&otilde;es em 2009 foi de 5.239.</span></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n<p style="text-align: justify;"><span style="font-size: small;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Em fevereiro de 2010, mais uma vez o PAT carapicuibano se destacou, com a recoloca&ccedil;&atilde;o de 547 pessoas no mercado de trabalho. Foi o primeiro lugar entre as cidades da regi&atilde;o oeste. Santana do Parna&iacute;ba, segundo colocado, recolocou 494 trabalhadores, enquanto Barueri, que ficou em terceiro, encaminhou ao mercado 460 pessoas.</span><br /></span>&nbsp;<br /><span style="font-size: small;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Os bons resultados foram poss&iacute;veis em fun&ccedil;&atilde;o das medidas adotadas pelo prefeito Sergio Ribeiro, no sentido de melhorar o relacionamento com os empregadores. A atual gest&atilde;o passou a oferecer o espa&ccedil;o da Secretaria de Desenvolvimento Econ&ocirc;mico, Social e Trabalho, aos setores de RH das empresas, para que se fa&ccedil;a a sele&ccedil;&atilde;o no pr&oacute;prio munic&iacute;pio, o que tornou a cidade bastante atrativa para os empres&aacute;rios.</span><br /></span>&nbsp;<br /><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Outro fator que aumentou a competitividade do trabalhador de Carapicu&iacute;ba, segundo o Prefeito Sergio Ribeiro, foi a ado&ccedil;&atilde;o do Bilhete &Uacute;nico em Carapicu&iacute;ba. O Bilhete &Uacute;nico permite duas viagens com a mesma tarifa, diminuindo o custo do trabalhador com transporte, o que aumentou interesse dos empres&aacute;rios, que geralmente arcam com os custos de vale transporte.</span></span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O sucesso da atua&ccedil;&atilde;o do PAT tamb&eacute;m pode ser medido pelo comparativo dos resultados anteriores &agrave; atual Administra&ccedil;&atilde;o. A m&eacute;dia mensal de empregabilidade nas administra&ccedil;&otilde;es anteriores era de apenas 6 trabalhadores. Em fevereiro de 2009 j&aacute; era registrado aumento de 1733,3 %. Em julho daquele ano a Prefeitura encaminhou 1179 trabalhadores ao mercado, n&uacute;mero maior do que os obtidos pelas outras cidades da Regi&atilde;o Oeste.</span></span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">O PAT fica na Secretaria de Desenvolvimento Econ&ocirc;mico, Social e Trabalho, e pode ser encontrado no Centro Administrativo da Prefeitura - Rua Get&uacute;lio Vargas, 280 - Santa Terezinha, pr&oacute;ximo ao Supermercado Sonda.</span></span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-size: small;">&nbsp;</span>\r\n<p>&nbsp;</p>\r\n<span style="font-size: small;">\r\n<hr />\r\n</span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="color: #006100;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Sobre o Primeiro de Maio</span></span></strong></span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Comemorado no dia 1&ordm; de maio, o Dia do Trabalho ou Dia do Trabalhador &eacute; uma data comemorativa usada para celebrar as conquistas dos trabalhadores ao longo da hist&oacute;ria. Nessa mesma data, em 1886, ocorreu uma grande manifesta&ccedil;&atilde;o de trabalhadores na cidade americana de Chicago.</span></span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Milhares de trabalhadores protestavam contra as condi&ccedil;&otilde;es desumanas de trabalho e a enorme carga hor&aacute;ria pela qual eram submetidos (13 horas di&aacute;rias). A greve paralisou os Estados Unidos.</span></span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">No Brasil, a data foi consolidada em 1924 no governo de Artur Bernardes. Al&eacute;m disso, a partir do governo de Get&uacute;lio Vargas, as principais medidas de benef&iacute;cio ao trabalhador passaram a ser anunciadas nesta data. Atualmente, in&uacute;meros pa&iacute;ses adotam o dia 1&deg; de maio como o Dia do Trabalho, sendo considerado feriado em muitos deles</span></span>\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;">&nbsp;\r\n<p>&nbsp;</p>\r\n<p style="text-align: justify;"><span style="font-size: small;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Texto: Suzana&nbsp;Camargo<br /></span></strong><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">Fotos: Gilberto&nbsp;Cerri&nbsp;</span></strong></span>\r\n<p>&nbsp;</p>\r\n</p>\r\n</p>\r\n</p>\r\n</p>\r\n</p>\r\n</p>\r\n</p>\r\n</p>\r\n</p>\r\n</p>\r\n</div>', 1, 0, '', '2011-05-20', 0, 0, 'festa-do-1o-de-maio-reune-100-mil-trabalhadores-no-parque-do-planalto', 99);
INSERT INTO `sf_news` (`id_news`, `id_profile`, `title`, `summary`, `body`, `status`, `flag_ultima_noticia`, `image`, `date`, `category`, `position_profile`, `permalink`, `ordem_destaque`) VALUES
(119, 5, 'Aldeia de Carapicuíba em festa na homenagem à Santa Cruz', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">A Festa da Santa Cruz, que acontece h&aacute; mais de 300 anos na Aldeia de Carapicu&iacute;ba,&nbsp;&eacute; um marco no calend&aacute;rio cultural do estado de S&atilde;o Paulo e do Brasil.</span></p>', '<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">A Festa da Santa Cruz, que acontece h&aacute; mais de 300 anos na Aldeia de Carapicu&iacute;ba,&nbsp;&eacute; um marco no calend&aacute;rio cultural do estado de S&atilde;o Paulo e do Brasil. A festa deste ano foi organizada pelos festeiros Otto Rubens Henne Jr, Vanette Camargo Gon&ccedil;ales, al&eacute;m da Comunidade de Santa Catarina e amigos e devotos da Santa Cruz. <br /><br /><img id="imagem_2_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/119_imagem_2.jpg" alt="" width="200" height="150" /><br /></span></span></p>\r\n<div id="conteudo1">\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">A abertura da festa foi no s&aacute;bado, dia 1&ordm;, com o projeto Amigos da Viola. Atra&ccedil;&otilde;es como a Orquestra Brasileira de Viola Caipira e a dupla&nbsp;Cacique &nbsp;e Paj&eacute; animaram os presentes.</span></span></p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">No domingo, 2, o destaque ficou por conta dos grupos &ldquo;&Ocirc; de Casa&rdquo;, com o Sarau Caipira e da dupla Roberto e Altair. As homenagens &agrave; Santa Cruz tiveram in&iacute;cio &agrave;s 19h30, com a b&ecirc;n&ccedil;&atilde;o do estandarte da Santa Cruz e o levantamento do mastro. Em seguida, teve in&iacute;cio a missa. O padre Vagner ressaltou a import&acirc;ncia da devo&ccedil;&atilde;o popular constru&iacute;da ao longo dos s&eacute;culos e mantidas gra&ccedil;as ao compromisso das fam&iacute;lias e devotos.<br /><br /><img id="imagem_3_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/119_imagem_3.jpg" alt="" width="200" height="150" /></span></span></p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Ap&oacute;s a missa, o Sarau Caipira retornou com a Dan&ccedil;a das Fitas, e em seguida teve in&iacute;cio a dan&ccedil;a da Santa Cruz, que foi at&eacute; o in&iacute;cio da madrugada. Os presentes cantaram, tocaram e dan&ccedil;aram diversas vezes diante das pequenas cruzes espalhadas na pra&ccedil;a.<br /><br /><img id="imagem_4_" style="display: block; margin-left: auto; margin-right: auto; border: black 1px solid;" src="http://187.17.68.70/carapicuiba.sp/imagens/noticias/119_imagem_4.jpg" alt="" width="200" height="150" /></span></span></p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Na segunda-feira, 3, cumprindo a tradi&ccedil;&atilde;o, a festa come&ccedil;ou &agrave;s 11 horas com a missa. Em seguida, os festeiros, convidados e a comunidade almo&ccedil;aram uma deliciosa feijoada.</span></span></p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Na noite de segunda-feira, a dan&ccedil;a da Santa Cruz recome&ccedil;ou com a presen&ccedil;a especial da cantora e folclorista&nbsp;Inezita Barroso. Os m&uacute;sicos da dan&ccedil;a da Santa Cruz tocam viol&atilde;o, pandeiro, cu&iacute;ca e reco-reco. As can&ccedil;&otilde;es sa&uacute;dam o templo e a cruz em que Cristo foi crucificado, encontrada pela santa Helena no ano 320.</span></span></p>\r\n<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">A grande festa terminou na noite de ter&ccedil;a-feira, 4, com o t&eacute;rmino da dan&ccedil;a, quando todas as cruzes s&atilde;o novamente saudadas. Os presentes se confraternizaram com a canja e se despedira na madrugada, com a gemada.</span></span></p>\r\n<strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Fotos: Gilberto Cerri</span></span></strong></div>', 1, 0, '', '2011-05-20', 0, 0, 'aldeia-de-carapicuiba-em-festa-na-homenagem-a-santa-cruz', 99),
(120, 8, 'Inscrições abertas para Dança de Salão', '<p><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;">A Secretaria de Esportes e Lazer est&aacute; com inscri&ccedil;&otilde;es abertas para Dan&ccedil;a de Sal&atilde;o no Gin&aacute;sio de Esportes Tancredo Neves, &agrave;s ter&ccedil;as e quintas das 8h &agrave;s 10h.</span></p>', '<p style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">A Secretaria de Esportes e Lazer est&aacute; com inscri&ccedil;&otilde;es abertas para Dan&ccedil;a de Sal&atilde;o no Gin&aacute;sio de Esportes Tancredo Neves, &agrave;s ter&ccedil;as e quintas das 8h &agrave;s 10h.</span></span></p>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">&nbsp;</span></span></div>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Desde o final do ano passado, a Dan&ccedil;a do Ventre vem formando a primeira turma e agora os interessados em aprender Bolero, Samba, Valsa, Gafieira, Fox, Rumba, Cha-cha-cha, Valdeir&atilde;o, entre outros ritmos, podem se inscrever gratuitamente na secretaria, que fica na Av. Inoc&ecirc;ncio Ser&aacute;fico, 2005, Telefone 4187-7000.</span></span></div>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">&nbsp;</span></span></div>\r\n<div style="text-align: justify;"><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><span style="font-size: small;">Para efetuar a matr&iacute;cula &eacute; necess&aacute;rio apresentar originais e xerox de RG, comprovante de endere&ccedil;o, declara&ccedil;&atilde;o escolar, 2 fotos 3x4 e atestado m&eacute;dico.</span></span></div>\r\n<div style="text-align: justify;"><strong><span style="font-family: tahoma,arial,helvetica,sans-serif; font-size: small;"><br /><span style="font-size: small;">Texto e Foto: Rog&eacute;rio Roque</span></span></strong></div>', 1, 0, '', '2011-05-20', 0, 0, 'inscricoes-abertas-para-danca-de-salao', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_news_access`
--

CREATE TABLE IF NOT EXISTS `sf_news_access` (
  `id_access` int(11) NOT NULL AUTO_INCREMENT,
  `id_nucleo` int(11) NOT NULL,
  `id_news` int(11) NOT NULL,
  `destaque` set('0','1') NOT NULL DEFAULT '0',
  `data_destaque` datetime NOT NULL,
  PRIMARY KEY (`id_access`),
  KEY `id_news` (`id_news`),
  KEY `id_nucleo` (`id_nucleo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_news_album`
--

CREATE TABLE IF NOT EXISTS `sf_news_album` (
  `id_news_album` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `id_album` int(11) NOT NULL,
  PRIMARY KEY (`id_news_album`),
  KEY `id_news` (`id_news`),
  KEY `id_album` (`id_album`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_news_archivos`
--

CREATE TABLE IF NOT EXISTS `sf_news_archivos` (
  `id_news_archivo` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `id_archivo` int(11) NOT NULL,
  PRIMARY KEY (`id_news_archivo`),
  KEY `id_news` (`id_news`),
  KEY `id_archivo` (`id_archivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_section`
--

CREATE TABLE IF NOT EXISTS `sf_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT '0',
  `position` int(11) DEFAULT NULL,
  `control` char(1) COLLATE latin1_spanish_ci DEFAULT '0',
  `sw_menu` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `status` char(1) COLLATE latin1_spanish_ci DEFAULT '0',
  `home` char(1) COLLATE latin1_spanish_ci DEFAULT '0',
  `special_page` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `show_text` char(1) COLLATE latin1_spanish_ci DEFAULT '0',
  `only_complement` char(1) COLLATE latin1_spanish_ci DEFAULT '0',
  `url_externa` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `delete` char(1) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sw_menu` (`sw_menu`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci AUTO_INCREMENT=125 ;

--
-- Volcado de datos para la tabla `sf_section`
--

INSERT INTO `sf_section` (`id`, `id_profile`, `id_parent`, `position`, `control`, `sw_menu`, `status`, `home`, `special_page`, `show_text`, `only_complement`, `url_externa`, `delete`) VALUES
(1, 0, 0, 1, '1', 'home', '2', '1', 'home', '0', '0', '', '0'),
(20, 29, 0, 11, '1', 'contactoppal', '1', '1', 'contact', '1', '1', '', '1'),
(23, 0, 0, 1, '1', 'noticia', '2', '0', 'detalleNoticia', '0', '0', '', '0'),
(24, 29, 0, 1, '1', 'noticias', '1', '0', 'news', '1', '0', '', '0'),
(67, 0, 0, 2, '1', 'galeria', '2', '0', 'gallery', '0', '0', '', '1'),
(77, 0, 0, 10, '1', 'busca', '2', '0', 'busca', '0', '0', '', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sf_section_i18n`
--

CREATE TABLE IF NOT EXISTS `sf_section_i18n` (
  `id` int(11) NOT NULL,
  `language` varchar(7) COLLATE latin1_spanish_ci NOT NULL,
  `name_section` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `descrip_section` text COLLATE latin1_spanish_ci NOT NULL,
  `meta_title` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `meta_keyword` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `meta_description` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`language`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `sf_section_i18n`
--

INSERT INTO `sf_section_i18n` (`id`, `language`, `name_section`, `descrip_section`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(1, 'pt', 'Inicio', '', '', 'Brain Fitness and Brain Training', 'Brain Fitness and Brain Training'),
(20, 'pt', 'Contato', '<div class="lc">\r\n<h2 class="what">&nbsp;</h2>\r\n</div>', '', '', ''),
(67, 'pt', 'Galeria', '', '', '', ''),
(23, 'pt', 'Detalle Noticias', '', '', '', ''),
(24, 'pt', 'Noticias', '', '', '', ''),
(77, 'pt', 'Resultado da Busca', '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
