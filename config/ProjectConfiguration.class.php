<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    //$this->setWebDir($this->getRootDir().'/web/site');
    $this->setWebDir($this->getRootDir().'/public_html');
    
    //sfConfig::set('sf_upload_dir', $this->getRootDir().'public_html'.DIRECTORY_SEPARATOR.'uploads');
    // set region values
    setlocale(LC_ALL, "pt_BR@BRL", "pt_BR", "ptb");

    
    $this->enablePlugins(
        'sfPropelPlugin',
        'sfJqueryPlugin',
        'sfLynxLanguagePlugin',
        'sfLynxSectionPlugin',
        'sfLynxNewsPlugin',
        'sfFormExtraPlugin',
        'sfLynxUploadFilePlugin',
        'sfThumbnailPlugin',
        'sfLynxPhotoAlbumPlugin',
        'sfUserV1Plugin',
        'sfSegurancaPlugin',
        'sfGalleryPlugin'
    );
  }
  
  
}
