<?php

/**
 * GaleriaBase form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
class GaleriaBaseForm extends BaseGaleriaBaseForm
{
  public function configure()
  {
    $fieldSize = 69;    
    $idProfile = sfContext::getInstance()->getUser()->getAttribute('idProfile');
    $nucleos = LxProfilePeer::getProfileWithoutAdminAndRoot();
    // widgets
    $this->widgetSchema['nombre']->setAttributes(array('class' => 'validate[required]', 'size' => $fieldSize));
    $this->widgetSchema['resumen'] = new sfWidgetFormTextarea(array(),array('cols' => 50, 'rows' => 6,'style' => 'width:437px' ));
    $this->widgetSchema['data'] = new sfWidgetFormInputHidden();
    $types = array('1' => 'Ativo', '0' => 'Inativo');
    $this->widgetSchema['status'] = new sfWidgetFormSelect(array('choices' => $types));
    $this->setDefault('status', '1');
    
    if($this->getObject()->isNew())
    {
        $this->setDefault('data', date("Y-m-d"));
    }
    //Validators
    $this->validatorSchema['nombre']  = new sfValidatorString(array('required' => true, 'trim' => true));
    $this->validatorSchema['resumen']  = new sfValidatorString(array('required' => false, 'trim' => true));
    $this->validatorSchema['status']  = new sfValidatorString(array('required' => false));

    //Etiquetas
    $this->widgetSchema->setLabels(array(
        'nombre'  => 'Nome do álbum <span class="required">*</span>',
    ));
  }
}
