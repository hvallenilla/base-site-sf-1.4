<?php

/**
 * GaleriaBase form base class.
 *
 * @method GaleriaBase getObject() Returns the current form's model object
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseGaleriaBaseForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id_galeria' => new sfWidgetFormInputHidden(),
      'nombre'     => new sfWidgetFormInputText(),
      'resumen'    => new sfWidgetFormTextarea(),
      'data'       => new sfWidgetFormDate(),
      'status'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id_galeria' => new sfValidatorPropelChoice(array('model' => 'GaleriaBase', 'column' => 'id_galeria', 'required' => false)),
      'nombre'     => new sfValidatorString(array('max_length' => 100)),
      'resumen'    => new sfValidatorString(),
      'data'       => new sfValidatorDate(),
      'status'     => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('galeria_base[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'GaleriaBase';
  }


}
