<?php

/**
 * Galeria form base class.
 *
 * @method Galeria getObject() Returns the current form's model object
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseGaleriaForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'id_galeria' => new sfWidgetFormInputText(),
      'titulo'     => new sfWidgetFormInputText(),
      'foto'       => new sfWidgetFormInputText(),
      'principal'  => new sfWidgetFormInputText(),
      'posicion'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorPropelChoice(array('model' => 'Galeria', 'column' => 'id', 'required' => false)),
      'id_galeria' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'titulo'     => new sfValidatorString(array('max_length' => 50)),
      'foto'       => new sfValidatorString(array('max_length' => 100)),
      'principal'  => new sfValidatorString(),
      'posicion'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
    ));

    $this->widgetSchema->setNameFormat('galeria[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Galeria';
  }


}
