<?php


/**
 * Skeleton subclass for representing a row from the 'galeria_base' table.
 *
 * 
 *
 * This class was autogenerated by Propel 1.4.1 on:
 *
 * 31/08/2013 17:08:22
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    plugins.sfGalleryPlugin.lib.model
 */
class GaleriaBase extends BaseGaleriaBase {

} // GaleriaBase
