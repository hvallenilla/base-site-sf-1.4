<?php if($fotos): ?>    
    <?php foreach($fotos as $foto): ?>
        <?php $i = $foto->getId(); ?>
        <div id="item_<?php echo $i; ?>" class="contentPicture" >
            <div style="height: 150px; margin-top: 5px; margin-bottom: 10px;"><img alt="" src="/images/../uploads<?php echo $upDir.'lista_'.$foto->getFoto() ?>"></div>
            <div class="opcoe_imagem">
            <?php echo link_to(image_tag($foto->getPrincipal().'_old','style="position: relative;left: 160px;"'),$module.'/destaqueImage?id_gallery='.$sf_request->getParameter('id').'&id='.$foto->getId()) ?>
            <?php
                echo jq_link_to_remote(image_tag('eliminar','width="15"'), array(
                 'update'   =>  'item_'.$i,
                 'url'      =>  $module.'/borrarImagen?foto='.$foto->getId(),
                 'script'   =>  true,
                 'success' =>  "",
                 'confirm'  => '¿Esta seguro que desea eliminar esta imagen?',
                 'before'   => "$('#item_".$i."').html('<div align=center><br />".image_tag('preload.gif','title="" alt=""')."</div>');",
                 'complete' => "$('#item_".$i."').remove();"),
                 'class="" style="position: relative;left: 165px;"');
           ?>
          </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>