<?php use_javascript('jq/jquery.uploadify.js') ?>
<?php use_stylesheet('jq/uploadify.css') ?>

<div id="title_module">
  <div class="frameForm" >      
      <h1><?php echo $album->getNombre() ?> - Galeria de Fotos</h1>
          
    <?php include_partial('imagenesInmueble', array('variables' => $variables, 'id_gallery' => $sf_user->getAttribute('id_gallery'))) ?>
  </div>
    <script type="text/javascript">
            <?php $timestamp = time();?>
            $(function() {
                $("#file_upload").uploadify({
                    debug           : false,
                    fileSizeLimit   : '10550MB',
                    checkExisting : '<?php echo url_for("gallery/checkImages")?>',
                    fileTypeExts    : '*.png; *.jpg',
                    removeCompleted : true,
                    height          : 30,
                    swf             : '/js/uploadify.swf',
                    uploader        : '<?php echo url_for("gallery/uploadImages?id=".$sf_user->getAttribute('id_gallery')."&uploadify=onUpload&" . session_name() . "=" . session_id(), true)?>',
                    buttonText      : 'Adicionar Fotos',
                    width           : 120,
                    multi           : true,
                    onSelectError   : function() {
                        alert('The file ' + file.name + ' returned an error and was not added to the queue.');
                    },
                    onUploadError   : function(file, errorCode, errorMsg, errorString) {
                        alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                    },
                    onQueueComplete : function(queueData) {
                        //alert(queueData.uploadsSuccessful + ' files were successfully uploaded.');     
                        $.ajax({
                            type: "POST",
                            url: "<?php echo url_for('gallery/getFotos'); ?>",
                            data: "id=<?php echo $sf_user->getAttribute('id_gallery')?>",
                            beforeSend: function(objeto){
                                $('#contenedorFotos').html("<img src='/images/loading.gif'/>");
                            },
                            success: function(html){
                                $('#contenedorFotos').html('');
                                $('#contenedorFotos').append(html);
                            }
                          })
                    },
                    onAllComplete: function() {
                        
                    }
                });
            });
    </script> 
  <div class="frameForm" >
    <?php echo link_to(__('Voltar'), '@default?module=gallery&action=index', array('class' => 'button')) ?>
  </div>
</div>


