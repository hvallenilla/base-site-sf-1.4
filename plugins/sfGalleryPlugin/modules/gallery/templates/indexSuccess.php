<?php use_helper('Text') ?>
<div id="title_module">
    <div class="frameForm" >
    <h1><?php echo __('Álbum de fotos') ?></h1>
    </div>
<div class="msn_error" id="no_select_item" style="display: none;"><?php echo __("There aren't items selected"); ?>.&nbsp;&nbsp;<a href="#" onclick="noSelectedItem();"><?php echo __('Hidden'); ?></a> </div>
<?php if ($sf_user->hasFlash('listo')): ?>
    <div class="msn_ready"><?php echo $sf_user->getFlash('listo') ?></div>
<?php endif; ?>
<div class="frameForm">
    <?php echo form_tag('gallery/deleteAll',array('name' => 'frmChk', 'id' => 'frmChk','style'=>'margin:0px')) ?>
    <table border="0">
        <tr>
            <td>

                <a name="commit" href="#" onclick="return existItems(this);"><?php echo __('Eliminar todos') ?></a>
            </td>
            <td>&nbsp;|&nbsp;</td>
            <td>
                <a href="<?php echo url_for($this->getModuleName().'/new') ?>"><?php echo __('Novo álbum de fotos')?></a>
            </td>
        </tr>
    </table>
</div>
<table cellpadding="0" cellspacing="0" border="0"  id="resultsList">
  <thead>
    <tr>
        <th>
            &nbsp;<input type="checkbox" id="chkTodos" value="checkbox" onClick="checkTodos(this);" >&nbsp;
        </th>
        <th>
            <?php echo link_to(__('Nome do álbum'),'@default?module=gallery&action=index&sort=gallery_name&by='.$by.'&page='.$albums->getPage().'&buscador='.$buscador) ?>
            <?php if($sort == "gallery_name"){ echo image_tag($by_page); }?>
        </th>
        <th>
            <?php echo link_to(__('Status'),'@default?module=gallery&action=index&sort=status&by='.$by.'&page='.$albums->getPage().'&buscador='.$buscador) ?>
            <?php if($sort == "status"){ echo image_tag($by_page,'align="top"'); }?>
        </th>
    </tr>
  </thead>
  <tbody>
  <?php if ($albums->getNbResults()): ?>
    <?php $i=0; ?>
    <?php foreach ($albums as $album): ?>
    <?php fmod($i,2)?$class = "grayBackground":$class=''; ?>
    <tr class="<?php echo $class;?>" valign="top" onmouseover="javascript:overRow(<?php echo $i; ?>);" onmouseout="javascript:outRow(<?php echo $i; ?>);">
        <td class="borderBottomDarkGray" width="28" align="center">
            &nbsp;<input type="checkbox" id="chk_<?php echo $album->getIdGaleria() ?>" name="chk[<?php echo $album->getIdGaleria() ?>]" value="<?php echo $album->getIdGaleria() ?>">&nbsp;
        </td>
        <td class="borderBottomDarkGray">
            <div class="displayTitle">
                <div id="title">                               
                    <a href="<?php echo url_for('gallery/edit?id_gallery='.$album->getIdGaleria()) ?>" class="titulo">
                        <?php echo $album->getNombre()  ?>
                    </a>
                </div>
                <div class="row-actions">
                    <div class="row-actions_<?php echo $i; ?>" style="display: none;">
                        <a href="<?php echo url_for('gallery/edit?id_gallery='.$album->getIdGaleria(), $album) ?>" class="edit"><?php echo __('Editar') ?></a>&nbsp;|&nbsp;
                        <?php echo link_to(__('Excluir'),'gallery/delete?id_gallery='.$album->getIdGaleria(), array('method' => 'delete', 'class' => 'delete' , 'confirm' => __('Are you sure you want to delete the selected data?'))) ?>&nbsp;|&nbsp;
                        <a href="<?php echo url_for('gallery/fotosAlbum?id_gallery='.$album->getIdGaleria(), $album) ?>" class="edit"><?php echo __('Imágenes') ?></a>
                    </div>
                </div>
            </div>
        </td>
        <?php $status = $album->getStatus(); ?>
        <?php $id = $album->getIdGaleria();?>        
        <td class="borderBottomDarkGray" id="status_<?php echo $id; ?>">
            <?php echo jq_link_to_remote(image_tag($status.'.png','alt="" title="" border=0'), array(
                'update'  =>  'status_'.$id,
                'url'     =>  'gallery/changeStatus?id='.$id.'&status='.$status.'&field=status',
                'script'  => true,
                'before'  => "$('#status_".$id."').html('". image_tag('preload.gif','title="" alt=""')."');"
            )); ?>                
        </td>
    </tr>        
    <?php $i++; ?>
    <?php endforeach; ?>
  </tbody>
</table>
    <?php else: ?>
    <table width="100%" align="center"  border="0" cellspacing="10">
        <tr>
            <td align="center"><strong><?php echo __('Your search did not match any result') ?></strong></td>
        </tr>
    </table>
    <?php endif; ?>
  
</form>
<?php if ($albums->haveToPaginate()): ?>
<table width="100%" align="center" id="paginationTop" border="0">
	<tr>
    	<td align="left" ><i><?php echo $albums->getNbResults().' '.__('results') ?>  - <?php echo __('page').' '.$albums->getPage().' '.__('for').' ' ?> <?php echo $albums->getLastPage() ?></i> </td>
        <td align="right">	
        	<table>
                	<tr>
                		<?php if ($albums->getFirstPage()!=$albums->getPage()) :?>
                		<td><?php echo link_to(image_tag('icon_first_page.jpg','alt='.__('First').' title='.__('First').' border=0'), '@default?module=gallery&action=index&sort='.$sort.'&by='.$by_page.'&page='.$albums->getFirstPage().$bus_pagi) ?></td>
                		<td><?php echo link_to(image_tag('icon_prew_page.jpg','alt='.__('Previous').' title='.__('Previous').' border=0'),'@default?module=gallery&action=index&sort='.$sort.'&by='.$by_page.'&page='.$albums->getPreviousPage().$bus_pagi) ?></td>
                		<?php endif; ?>
                		<td >
                		<?php $links = $albums->getLinks(); 
                        
	                        foreach ($links as $page): ?>
	                        <?php echo ($page == $albums->getPage()) ? '<strong>'.$page.'</strong>' : link_to($page, '@default?module=gallery&action=index&sort='.$sort.'&by='.$by_page.'&page='.$page.$bus_pagi) ?>
		                        <?php if ($page != $albums->getCurrentMaxLink()): ?>
		                        -
		                        <?php endif; ?>
	                        <?php endforeach; ?>
                		</td>
                		<?php if ($albums->getLastPage()!=$albums->getPage()) :?>
                		<td><?php echo link_to(image_tag('icon_next_page.jpg','alt='.__('Next').' title='.__('Next').' border=0'), '@default?module=gallery&action=index&page='.$albums->getNextPage().$bus_pagi) ?></td>
                		<td><?php echo link_to(image_tag('icon_last_page.jpg','alt='.__('Last').' title='.__('Last').' border=0'), 'gallery/index?page='.$albums->getLastPage().$bus_pagi) ?></td>
                		<?php endif; ?>
                	</tr>
            </table>
		</td>
	</tr>
</table>
<?php else: ?>
<div class="results">
            <i><?php echo $albums->getNbResults().' '.__('results') ?></i>
</div>
<?php endif; ?>
</div>

