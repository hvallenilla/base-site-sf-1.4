<?php
  $module = $variables['module'];
  $appYml = $variables['appYml'];
  $fotos  = $variables['fotos'];
  $upDir  = $variables['upDir'];
  $ancho = "98%";
?>
<div style="width:<?php echo $ancho?>" class="divResultsList">
  <table width="100%">
    <tr>
      <td style="padding: 10px; padding-left:0px">
        <form>
            <div id="queue"></div>            
            <input id="file_upload" name="file_upload" type="file" multiple="true"  />             
        </form>
      </td>
    </tr>
    <tr>
      <td>
        <span class="msn_help"> 
          <b><?php echo __('Seleccione una Imagen');?></b><br />
          <?php
            echo __('As imagens não podem ser maiores que '.(sfConfig::get('app_image_size_text')).'<br /> ');
            echo __('O tamanho mínimo de 640x480.<br />');
            echo __('<div id="mes_adv"></div>');
          ?>
        </span>
      </td>
    </tr>
    <tr>
      <td height="25" align="center" valign="middle">
        <div id="fileUpload" class="msn_ready" style="display:none;">Imagem enviada com sucesso</div>
        <div id="mimeError"  class="msn_error" style="display:none; margin-top: 0px" >Arquivo incorreto só .jpg, .png, o .gif</div>
      </td>
    </tr>
    <tr>
      <td height="25">
        <span class="msn_help">As imagens são apresentadas na seguinte ordem:</span>
      </td>
    </tr>
    <tr>
      <td id="contenedorFotos">
        <?php if($fotos): ?>
          
          <?php foreach($fotos as $foto): ?>
            <?php $i = $foto->getId(); ?>
            <div id="item_<?php echo $i; ?>" class="contentPicture" >
                <div style="height: 150px; margin-top: 5px; margin-bottom: 10px;">
                    <img alt="" src="/images/../uploads<?php echo $upDir.'lista_'.$foto->getFoto() ?>">
                </div>
                <div class="opcoe_imagem">
                <?php echo link_to(image_tag($foto->getPrincipal().'_old','style="position: relative;left: 160px;"'),$module.'/destaqueImage?id_gallery='.$id_gallery.'&id='.$foto->getId()) ?>
                <?php
                    echo jq_link_to_remote(image_tag('eliminar','width="15"'), array(
                     'update'   =>  'item_'.$i,
                     'url'      =>  $module.'/borrarImagen?foto='.$foto->getId(),
                     'script'   =>  true,
                     'success' =>  "",
                     'confirm'  => '¿Esta seguro que desea eliminar esta imagen?',
                     'before'   => "$('#item_".$i."').html('<div align=center><br />".image_tag('preload.gif','title="" alt=""')."</div>');",
                     'complete' => "$('#item_".$i."').remove();"),
                     'class="" style="position: relative;left: 165px;"');
               ?>
               
              </div>
            </div>
            
          <?php endforeach; ?>
        <?php endif; ?>
        <?php echo jq_sortable_element('contenedorFotos', array(
    'url' => 'gallery/changePosition',
    'only' => 'contentPicture',
    ),
    'response',
    '<div class="load">Arranging positions … </div><br />',
    '<div class="load">Positions saved</div><br />'
) ?>
      </td>
    </tr>
  </table>
</div>


<div id="response" ></div>