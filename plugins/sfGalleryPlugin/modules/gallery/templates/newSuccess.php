<div id="title_module">
    <div class="frameForm" >
        <h1><?php echo __($moduleParent['parent_name'])?> - <a href="<?php echo url_for('gallery/index') ?>"><?php echo __('Álbum de Fotos')  ?></a> - <?php echo __('Novo álbum de fotos') ?> </h1>
    </div>
    <?php include_partial('form', array('form' => $form)) ?>
</div>

