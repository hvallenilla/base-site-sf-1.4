<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<script type="text/javascript"> 
    $(document).ready(function() {
        $("#gallery").validationEngine();
    });
</script>

<form id="gallery" action="<?php echo url_for('gallery/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id_gallery='.$form->getObject()->getIdGaleria() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>> 

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<div class="frameForm" align="left">
  <table width="100%">
      <tr>
        <td>
            &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são obrigatórios ')?>
        </td>
      </tr>
      <tr>
        <td id="errorGlobal">
            <?php echo $form->renderGlobalErrors() ?>
        </td>
      </tr>
      <tr>
          <td>
              <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                                <?php echo link_to(__('Voltar na lista'), '@default?module=gallery&action=index', array('class' => 'button')) ?>
                        </div>
                    </td>
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                                <?php echo link_to(__('Delete'), 'gallery/delete?id_gallery='.$form->getObject()->getIdGaleria(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                        <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
          </td>
      </tr>
    <tfoot>
      <tr>
        <td>
            <?php echo $form->renderHiddenFields(false) ?>
          <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Voltar na lista'), '@default?module=gallery&action=index', array('class' => 'button')) ?>
                        </div>
                    </td>            
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Excluir'), 'gallery/delete?id_gallery='.$form->getObject()->getIdGaleria(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                        <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
        </td>
      </tr>
    </tfoot>
    <tbody>
        <tr>
            <td>                
                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                  <tr>
                      <td><?php echo $form['nombre']->renderLabel() ?><br />
                        <?php echo $form['nombre'] ?>
                        <?php echo $form['nombre']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['resumen']->renderLabel() ?><br />
                        <?php echo $form['resumen'] ?>
                        <?php echo $form['resumen']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['status']->renderLabel() ?><br />
                        <?php echo $form['status'] ?>
                        <?php echo $form['status']->renderError() ?>
                    </td>
                  </tr>
                </table>                
            </td>
        </tr>
    </tbody>
  </table>
</div>

</form>