<?php

/**
 * gallery actions.
 *
 * @package    joaapaulo
 * @subpackage gallery
 * @author     Henry Vallenilla <henryvallenilla@gmail.com>
 */
class galleryActions extends sfActions
{
  public function executeIndex()
  {
    //Criterios de busqueda
    $c = new Criteria();
    $c->addDescendingOrderByColumn(GaleriaBasePeer::DATA);
    $pager = new sfPropelPager('GaleriaBase',15);
    $pager->setCriteria($c);
    $pager->setPage($this->getRequestParameter('page',1));
    $pager->init();
    $this->albums = $pager;         
  }
  public function executeNew(sfWebRequest $request)
  {
    sfConfig::set('sf_escaping_strategy', false);
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Novo').' album - Lynx Cms');
    $this->form = new GaleriaBaseForm();
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Editar').' Album - Lynx Cms');
    if (!$request->isMethod('post'))
    {
        $this->redirect("gallery/new");
    }
    $this->form = new GaleriaBaseForm();
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    sfConfig::set('sf_escaping_strategy', false);
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Editar').' Album - Lynx Cms');
    $this->forward404Unless($Album = GaleriaBasePeer::retrieveByPk($request->getParameter('id_gallery')), sprintf('Object Album does not exist (%s).', $request->getParameter('id_gallery')));

    $this->form = new GaleriaBaseForm($Album);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }
  
  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($SfAlbum = GaleriaBasePeer::retrieveByPk($request->getParameter('id_gallery')), sprintf('Object Album does not exist (%s).', $request->getParameter('id_gallery')));
    $this->form = new GaleriaBaseForm($SfAlbum);

    $this->processForm($request, $this->form);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->setTemplate('edit');
  }
  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($album = GaleriaBasePeer::retrieveByPk($request->getParameter('id_gallery')), sprintf('Object Gallery does not exist (%s).', $request->getParameter('id_gallery')));
    //Delete images process
    $c = new Criteria();
    $c->add(GaleriaPeer::ID_GALERIA, $request->getParameter('id_gallery'));
    $items = GaleriaPeer::doSelect($c);
    if ($items)
    {
        foreach ($items as $item)
        {
            $this->deleitaFoto($item->getId(), $item->getFoto());            
        }
    }
    //Album and album content delete (Cascade)
    $album->delete();
    $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));
    return $this->redirect('gallery/index');
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $album = $form->save();
      $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_save_confir')));
      if(!$request->getParameter('id_gallery'))
      {
          return $this->redirect('gallery/fotosAlbum?id_gallery='.$album->getIdGaleria());
      }else{
          return $this->redirect('gallery/index');
      }            
    }
  }

  public function executeFotosAlbum(sfWebRequest $request)
  {
    sfConfig::set('sf_escaping_strategy', false);
    $id_album = $request->getParameter('id_gallery');
    $this->album = GaleriaBasePeer::retrieveByPK($id_album);
    $this->getUser()->setAttribute('id_gallery', $id_album);
    
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Adicionar imagens').' - '.sfConfig::get('app_name_app'));
    $fotos = array(); 
    $uploadDir = sfConfig::get('sf_upload_dir').'/galeria';
    if(file_exists($uploadDir))
    {
      $c = new Criteria();
      $c->add(GaleriaPeer::ID_GALERIA, $id_album, Criteria::EQUAL);
      $c->addAscendingOrderByColumn(GaleriaPeer::POSICION);
      $fotos = GaleriaPeer::doSelect($c);      
    }
    //Se construye el arreglo de variables para el componente de imagenes
    $upDir='/galeria/';
    $this->variables = array(
        'module' => $this->getModuleName(), 
        'appYml' => sfConfig::get('mod_inmuebles_usados_configuration_upload_images'),
        'fotos' => $fotos, 'upDir' => $upDir
        );
  }
  
  public function executeGetFotos(sfWebRequest $request)
  {
      $this->module = $this->getModuleName();
      $this->upDir = '/galeria/';
      $this->fotos = array(); //Arreglo sin fotos para los inmuebles sin imagenes
      $directory = sfConfig::get('sf_upload_dir').'/galeria';
      if(file_exists($directory))
      {
          $c = new Criteria();
          $c->add(GaleriaPeer::ID_GALERIA, $request->getParameter('id'), Criteria::EQUAL);
          $c->addAscendingOrderByColumn(GaleriaPeer::POSICION);
          $this->fotos = GaleriaPeer::doSelect($c);
      }
  }

  public function executeDestaqueImage(sfWebRequest $request)
  {
      $id_gallery = $request->getParameter('id_gallery');
      GaleriaPeer::unSelectPrincipal($id_gallery);
      GaleriaPeer::selectPrincipal($request->getParameter('id'));
      return $this->redirect('gallery/fotosAlbum?id_gallery='.$id_gallery);
  }

  public function executeCheckImages(sfWebRequest $request)
  {
      $targetFolder = '/uploads'; // Relative to the root and should match the upload folder in the uploader script
        $tempFile = $_FILES['Filedata']['tmp_name'];
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $targetFolder . '/' . $_POST['filename'])) {
                echo 1;
        } else {
                echo $_SERVER['DOCUMENT_ROOT'] . $targetFolder . '/' . $_POST['filename'];
        }
      return sfView::NONE;
  }
  
  public function executeUploadImages(sfWebRequest $request)
  {
      if (!empty($_FILES))
      {
        $random = aplication_system::GenerateRandomString();
        sfProjectConfiguration::getActive()->loadHelpers('upload');
        $uploadDir = sfConfig::get('sf_upload_dir').'/galeria/';   
        $name_foto = $random.'.jpg';
        $this->fileUploaded = loadFile($name_foto,$_FILES['Filedata']['tmp_name'], 0, $uploadDir, 'op', true);
        $file = $uploadDir.$this->fileUploaded;          
        $img = new sfImage($file, 'image/jpg');
        // LightBox
        $img->thumbnail(800,600);
        $img->setQuality(100);
        $img->saveAs($uploadDir.'original_'.$this->fileUploaded);
        // Lista Y Home
        $img->thumbnail(200,150);
        $img->setQuality(100);
        $img->saveAs($uploadDir.'lista_'.$this->fileUploaded);
        unlink($uploadDir.$this->fileUploaded);
        
        $las_position = GaleriaPeer::getLastPosition();
        $las_position = $las_position + 1;
        $inc = new Galeria();
        $inc->setId($id);
        $inc->setIdGaleria($request->getParameter('id'));
        $inc->setFoto($this->fileUploaded);
        $inc->setPosicion($las_position);
        $inc->setPrincipal(0);
        $inc->save();        
      }
      return sfView::NONE;
  }
  
  public function executeChangePosition(sfWebRequest $request)
  {
    foreach ($request->getParameter('item') as $position => $id)
    {
        /** Update position **/
        $item = GaleriaPeer::retrieveByPK($id);
        $item->setPosicion($position);
        $item->save();
    }
    return true;
  }

  public function executeBorrarImagen(sfWebRequest $request)
  {
    $id_foto = GaleriaPeer::retrieveByPK($request->getParameter('foto'));
    $imagen = $id_foto->getFoto();
    $this->deleitaFoto($request->getParameter('foto'), $imagen);
    return true;
  }
  
  public function deleitaFoto($id_foto, $imagen)
  {
    $directorio = sfConfig::get('sf_upload_dir').'/galeria/';
    $filename = $directorio.$imagen;
    unlink($filename); //Borra la imagen con nombre P_ID-INMUEBLE_POSICION    
    
    $filename = $directorio."lista_".$imagen; 
    unlink($filename);     
    $filename = $directorio."original_".$imagen;
    unlink($filename); 
    GaleriaPeer::deleita($id_foto);    
    chmod($directorio, 0777);  
  }

  
}
