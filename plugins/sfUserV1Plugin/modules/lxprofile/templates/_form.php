<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jq/jquery.validationEngine.js') ?>
<?php use_javascript('jq/jquery.validationEngine-en_US.js') ?>
<script src="/js/jq/jquery-ui-1.8.16.custom/development-bundle/ui/i18n/jquery.ui.datepicker-br.js"></script>
<script type="text/javascript"> 
$(document).ready(function() {
      $("#lxprofile").validationEngine();
      $("#lx_profile_data_cadastro").datepicker({
            dateFormat: 'yy-mm-dd',  
            yearRange: '-93:-13',
            changeMonth: true,
            changeYear: true
          });
})
</script>

<form id="lxprofile" action="<?php echo url_for('lxprofile/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id_profile='.$form->getObject()->getIdProfile() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

 <?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<div class="frameForm" align="center">
  <table width="100%">
      <tr>
        <td colspan="2">
            &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são obrigatórios')?>
        </td>
      </tr>
      <tr>
        <td>
            <?php echo $form->renderGlobalErrors() ?>
        </td>
      </tr>
      <tr>
          <td colspan="2">
              <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                                                            <?php echo link_to(__('Voltar à lista'), $sf_request->getReferer(), array('class' => 'button')) ?>
                                                    </div>
                    </td>
                    <?php 
                    //El administrador se puede editar pero no se puede borrar
                    if (!$form->getObject()->isNew() and $form->getObject()->getIdProfile()!=2):
                    ?>

                    <td>
                        <div class="button">
                                <?php echo link_to(__('Eliminar'), 'lxprofile/delete?id_profile='.$form->getObject()->getIdProfile(), array('method' => 'Eliminar', 'confirm' => __('Are you sure you want to Eliminar the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                    <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
          </td>
      </tr>
    <tfoot>
      <tr>
        <td colspan="2">
                                  <?php echo $form->renderHiddenFields(false) ?>
                        <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                                               <?php echo link_to(__('Voltar à lista'), $sf_request->getReferer(), array('class' => 'button')) ?>
                                            </div>
                    </td>            
                     <?php
                    //El administrador se puede editar pero no se puede borrar
                    if (!$form->getObject()->isNew() and $form->getObject()->getIdProfile()!=2):
                    ?>
                    <td>
                        <div class="button">
                                            <?php echo link_to(__('Eliminar'), 'lxprofile/delete?id_profile='.$form->getObject()->getIdProfile(), array('method' => 'Eliminar', 'confirm' => __('Are you sure you want to Eliminar the selected data?'), 'class' => 'button')) ?>
                                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                    <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
        </td>
      </tr>
    </tfoot>
    <tbody>
        <tr>
            <td>                
                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                  <tr>
                      <td>
                          <?php $module = 'secretarias'; ?>
                          <?php  $appYml = sfConfig::get('app_upload_images_lxprofile'); ?>
                          <table cellpadding="0" cellspacing="0" border="0" width="80%" style="margin-top: 15px; margin-bottom: 15px;">
                              <tr>
                                  <td width="3%" align="left" >
                                      <div id="imageFIELD" style="min-height: 110px; min-width: 170px;">
                                          <?php if($form->getObject()->getFoto()):  ?>
                                          <?php echo image_tag('/uploads/'.$module.'/'.$appYml['size_1']['pref_1'].'_'.$form->getObject()->getFoto(), 'class="borderImage" width="150" height="110"')?>
                                          <?php else:?>
                                          <?php echo image_tag('no_image.jpg', 'border=0 width="150" height="110" class="borderImage"');?>
                                          <?php endif;?>
                                      </div>
                                  </td>
                                  <td width="67%" valign="bottom" style="padding-left:7px">
                                      <?php echo $form['foto']->renderLabel() ?><br />
                                      <?php echo $form['foto'] ?>
                                      <?php echo $form['foto']->renderError() ?>
                                      <span class="msn_help"><?php echo $form['foto']->renderHelp() ?></span>
                                  </td>
                              </tr>
                              <tr>
                                   <td>
                                       <?php if($form->getObject()->getFoto()):?>
                                      <div id="deleteImage" style="margin-left: 40px;" >
                                          <?php echo jq_link_to_remote('Deletar Imagem', array(
                                                'update'  =>  'imageFIELD',
                                                'url'     =>  'lxprofile/deleteImage?id='.$form->getObject()->getIdProfile(),
                                                'script'  => true,
                                                'confirm' => __('Are you sure you want to delete the selected data?'),
                                                'before'  => "$('#imageFIELD').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');",
                                                'complete'=> "$('#deleteImage').html('');"
                                            ));
                                            ?>
                                      </div>
                                      <?php endif;?>
                                   </td>
                                   <td>&nbsp;</td>
                              </tr>
                          </table>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['name_profile']->renderLabel() ?> <span class="required">*</span><br />
                        <?php echo $form['name_profile'] ?>
                        <?php echo $form['name_profile']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['nome_corto']->renderLabel() ?> <span class="required">*</span><br />
                        <?php echo $form['nome_corto'] ?>
                        <?php echo $form['nome_corto']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['nome_secretario']->renderLabel() ?> <span class="required">*</span><br />
                        <?php echo $form['nome_secretario'] ?>
                        <?php echo $form['nome_secretario']->renderError() ?>
                    </td>
                  </tr>
                  <?php //if ($form->getObject()->getIdProfile() > 2): ?>
                  <tr>
                      <td><?php echo $form['sexo_secretario']->renderLabel() ?> <span class="required">*</span><br />
                        <?php echo $form['sexo_secretario'] ?>
                        <?php echo $form['sexo_secretario']->renderError() ?>
                    </td>
                  </tr>
                  <?php //endif; ?>
                  <tr>
                      <td><?php echo $form['clase_profile']->renderLabel() ?><br />
                        <?php echo $form['clase_profile'] ?>
                        <?php echo $form['clase_profile']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['end_inst']->renderLabel() ?><br />
                        <?php echo $form['end_inst'] ?>
                        <?php echo $form['end_inst']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['email_inst']->renderLabel() ?><br />
                        <?php echo $form['email_inst'] ?>
                        <?php echo $form['email_inst']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['telefone']->renderLabel() ?><br />
                        <?php echo $form['telefone'] ?>
                        <?php echo $form['telefone']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['data_cadastro']->renderLabel() ?><br />
                        <?php echo $form['data_cadastro'] ?>
                        <?php echo $form['data_cadastro']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['tipo_inst']->renderLabel() ?><br />
                        <?php echo $form['tipo_inst'] ?>
                        <?php echo $form['tipo_inst']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['conteudo']->renderLabel() ?><br />
                        <?php echo $form['conteudo'] ?>
                        <?php echo $form['conteudo']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['biografia']->renderLabel() ?><br />
                        <?php echo $form['biografia'] ?>
                        <?php echo $form['biografia']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td style="vertical-align: top;">
                          <?php echo $form['mapa']->renderLabel() ?><br />
                          <table width="100%" >
                              <tr>
                                  <td style="vertical-align: top;">
                                      Inserir o código iframe associado com Google Map<br />
                                      <?php echo $form['mapa'] ?>
                                  </td>
                                  <td>
                                      <div style="border: 1px solid #CCC;">
                                      <?php echo html_entity_decode($form->getObject()->getMapa()) ?>
                                      </div>
                                  </td>
                              </tr>
                          </table>
                        <?php echo $form['mapa']->renderError() ?>
                    </td>
                  </tr>
                  
                  <tr>
                      <td><?php echo $form['status']->renderLabel() ?><br />
                        <?php echo $form['status'] ?>
                        <?php echo $form['status']->renderError() ?>
                    </td>
                  </tr>
                  
                                        </table>                
            </td>
        </tr>
    </tbody>
  </table>
    </div>
</form>
