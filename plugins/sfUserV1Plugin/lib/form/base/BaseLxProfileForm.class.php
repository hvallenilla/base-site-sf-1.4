<?php

/**
 * LxProfile form base class.
 *
 * @method LxProfile getObject() Returns the current form's model object
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseLxProfileForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id_profile'      => new sfWidgetFormInputHidden(),
      'name_profile'    => new sfWidgetFormInputText(),
      'nome_corto'      => new sfWidgetFormInputText(),
      'nome_secretario' => new sfWidgetFormInputText(),
      'sexo_secretario' => new sfWidgetFormInputText(),
      'end_inst'        => new sfWidgetFormInputText(),
      'telefone'        => new sfWidgetFormInputText(),
      'email_inst'      => new sfWidgetFormInputText(),
      'data_cadastro'   => new sfWidgetFormDate(),
      'tipo_inst'       => new sfWidgetFormInputText(),
      'permalink'       => new sfWidgetFormInputText(),
      'conteudo'        => new sfWidgetFormTextarea(),
      'mapa'            => new sfWidgetFormTextarea(),
      'biografia'       => new sfWidgetFormTextarea(),
      'foto'            => new sfWidgetFormInputText(),
      'ordem'           => new sfWidgetFormInputText(),
      'status'          => new sfWidgetFormInputText(),
      'clase_profile'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id_profile'      => new sfValidatorPropelChoice(array('model' => 'LxProfile', 'column' => 'id_profile', 'required' => false)),
      'name_profile'    => new sfValidatorString(array('max_length' => 100)),
      'nome_corto'      => new sfValidatorString(array('max_length' => 30)),
      'nome_secretario' => new sfValidatorString(array('max_length' => 100)),
      'sexo_secretario' => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'end_inst'        => new sfValidatorString(array('max_length' => 100)),
      'telefone'        => new sfValidatorString(array('max_length' => 13)),
      'email_inst'      => new sfValidatorString(array('max_length' => 100)),
      'data_cadastro'   => new sfValidatorDate(),
      'tipo_inst'       => new sfValidatorString(array('max_length' => 20)),
      'permalink'       => new sfValidatorString(array('max_length' => 255)),
      'conteudo'        => new sfValidatorString(),
      'mapa'            => new sfValidatorString(array('required' => false)),
      'biografia'       => new sfValidatorString(array('required' => false)),
      'foto'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ordem'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'status'          => new sfValidatorString(),
      'clase_profile'   => new sfValidatorString(array('max_length' => 30)),
    ));

    $this->widgetSchema->setNameFormat('lx_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'LxProfile';
  }


}
