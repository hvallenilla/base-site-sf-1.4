<?php

/**
 * LxProfile form.
 *
 * @package    lynx
 * @subpackage form
 * @author     Your name here
 */
class LxProfileForm extends BaseLxProfileForm
{
  public function configure()
  {
      $sexo = array("M" => "Homem" , "F" => "Mulher");
      
      // Widget
      $this->widgetSchema['name_profile']->setAttributes(array('class' => 'validate[required]','size' => '40','maxlength' => '100'));
      $this->widgetSchema['nome_secretario']->setAttributes(array('class' => 'validate[required]','size' => '40','maxlength' => '30'));
      $this->widgetSchema['nome_corto']->setAttributes(array('class' => 'validate[required]','size' => '40','maxlength' => '30'));
      $this->widgetSchema['end_inst']->setAttributes(array('class' => '','size' => '70','maxlength' => '100'));
      $this->widgetSchema['telefone']->setAttributes(array('class' => '','size' => '40','maxlength' => '13'));
      $this->widgetSchema['email_inst']->setAttributes(array('class' => '','size' => '40','maxlength' => '100'));
      $this->widgetSchema['data_cadastro'] = new sfWidgetFormInputText();
      $this->widgetSchema['tipo_inst']->setAttributes(array('class' => '','size' => '30','maxlength' => '20'));
      $this->widgetSchema['conteudo'] = new sfWidgetFormRichTextarea(array('tool'=>'Custom','height' => '250'),array('class' => 'validate[required]'));
      $this->widgetSchema['mapa'] = new sfWidgetFormTextarea(array(),array('cols' => '40', 'rows' => '10'));
      $this->widgetSchema['biografia'] = new sfWidgetFormRichTextarea(array('tool'=>'Custom','height' => '250'),array('class' => 'validate[required]'));
      $this->widgetSchema['status'] = new sfWidgetFormChoice(array('choices' => array('1' => 'Ativo', '0' => 'Inativo')));
      $this->widgetSchema['clase_profile'] = new sfWidgetFormChoice(array('choices' => array('azul' => 'Azul', 'amarillo' => 'Amarelo', 'verde' => 'Verde', 'rojo' => 'Vermelho')));
      $this->widgetSchema['sexo_secretario'] = new sfWidgetFormChoice(array('choices' => $sexo));
      $this->widgetSchema['foto'] = new sfWidgetFormInputFileEditable(array(
        'file_src' => sfConfig::get('sf_upload_dir').'/secretarias/'.$this->getObject()->getFoto(),
        'is_image'  => true,
        'edit_mode' => !$this->isNew(),
        'template'      => '<div>%file%<br />%input%<br /></div>',
        ));
      
      //Validores
      $this->validatorSchema['name_profile']->setOption('required', true);
      $this->validatorSchema['sexo_secretario']->setOption('required', true);
      $this->validatorSchema['nome_secretario']->setOption('required', true);
      $this->validatorSchema['nome_corto']->setOption('required', true);
      $this->validatorSchema['email_inst']->setOption('required', false);
      $this->validatorSchema['end_inst']->setOption('required', false);
      $this->validatorSchema['telefone']->setOption('required', false);
      $this->validatorSchema['conteudo']->setOption('required', false);
      $this->validatorSchema['mapa']->setOption('required', false);
      $this->validatorSchema['biografia']->setOption('required', false);
      $this->validatorSchema['foto'] = new sfValidatorFile(array(
        'required'   => false,
        'max_size'   => sfConfig::get('app_image_size'),
        'mime_types' => array('image/jpeg','image/pjpeg','image/png','image/gif'),
       ));
       $this->validatorSchema['foto'] = new sfImageFileValidator(array(
                'required'      => false,
                'mime_types'    => array('image/jpeg', 'image/png', 'image/gif', 'image/pjpeg'),
                'max_size'      => sfConfig::get('app_image_size'),
                'min_height'    => '100',
                'min_width'     => '100',
                'path'          => false,
            ), array(
                'required'      => "La imagen principal es requerida",
                'min_width'     => "El ancho de la imagen es muy corto (mínimo es %min_width%px, tiene %width%px ).",
                'min_height'    => "La altura mínima de la imagen debe ser 100px."

        ));
        $this->validatorSchema['foto']->setMessage('mime_types','Error mime types %mime_type%.');

      //Labels
        $this->widgetSchema->setLabels(array(
            'name_profile'      => 'Nome Instituição',
            'status'            => 'Status',
            'tipo_inst'         => 'Tipo Instituição',
            'email_inst'        => 'Email da Instituição',
            'end_inst'          => 'Endereço',
            'nome_secretario'   => 'Nome Secretario',
            'clase_profile'   => 'Classe Associada',

        ));
        $appYml = sfConfig::get('app_upload_images_lxprofile');
        //Help Messages
        $this->widgetSchema->setHelps(array(
            'foto'     => 'A imagem deve ser JPEG, JPG, PNG ou GIF<br />
                            A imagem deve ter um tamanho Maximo de '.(sfConfig::get('app_image_size_text')).'<br />
                            A imagem deve ter um tamanho mínimo de '.$appYml['size_1']['image_width_1'].' x '.$appYml['size_1']['image_height_1'].' pixels',

        ));
      
        unset($this['permalink'], $this['ordem']);
  }
  
  protected function doSave($con = null)
  {
      $module = 'secretarias';
      $appYml = sfConfig::get('app_upload_images_lxprofile');
      // Si hay un nuevo archivo por subir y ya mi registro tiene un archivo asociado entonces,
      if ($this->getObject()->getFoto() && $this->getValue('foto'))
      {
          // recorro y elimino
          for($i=1;$i<=$appYml['copies'];$i++)
          {
              // Elimino las fotos de la carpeta
              if(is_file(sfConfig::get('sf_upload_dir').'/'.$module.'/'.$appYml['size_'.$i]['pref_'.$i].'_'.$this->getObject()->getFoto()))
              {
                unlink(sfConfig::get('sf_upload_dir').'/'.$module.'/'.$appYml['size_'.$i]['pref_'.$i].'_'.$this->getObject()->getFoto());
              }
          }
      }
      return parent::doSave($con);
  }
}
