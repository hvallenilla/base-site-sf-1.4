<script type="text/javascript">
    
    function posiciona(num)
        {
            $('#loading-' + num).html("<img src='<?php echo sfConfig::get('app_base_dir_web') ?>images/preloader.gif'/>").fadeIn('fast');
            $.ajax
            ({
                type: "POST",
                url:  "<?php echo url_for('news/estableceNuevaPosicion') ?>",
                data: "id_news=<?php echo $sf_request->getParameter('id_news') ?>&position="+num,
                success: function(msg)
                {
                    $('#loading-' + num).fadeOut('fast');
                }
            });
        }
</script>
<div class="preview-top-site">
    <div  class="bloques">
    <div class="posicion" id="position" onclick="javascript:posiciona(1);">
        POSITION 1
        <div id="loading-1"></div>
        
    </div>
    <div class="posicion" id="position" onclick="posiciona(2)">
        POSITION 2
        <div id="loading-2"></div>
        
    </div>
    <div class="posicion" id="position" onclick="posiciona(3)">
        POSITION 3
        <div id="loading-3"></div>
        
    </div>
    <div class="posicion" id="position" onclick="posiciona(4)">
        POSITION 4
        <div id="loading-4"></div>
        
    </div>
    
</div>

</div>
