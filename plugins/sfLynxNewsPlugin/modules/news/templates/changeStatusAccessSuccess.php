<?php echo jq_link_to_remote(image_tag($status.'.png','alt="" title="" border=0'), array(
    'update'  =>  'status_'.$sf_request->getParameter('id_nucleo'),
    'url'     =>  'news/changeStatusAccess?id_nucleo='.$sf_request->getParameter('id_nucleo').'&status='.$status.'&id_news='.$sf_request->getParameter('id_news'),
    'script'  => true,
    'before'  => "$('#status_".$sf_request->getParameter('id_nucleo')."').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');$('#destaque_".$sf_request->getParameter('id_nucleo')."').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');$('#list_permissions').html('<div align=center class=ppalText>". image_tag('loading.gif','title="" alt=""')."</div>');",
    'complete'=> "$('#list_permissions').html('<div class=ppalText>".__(sfConfig::get('mod_news_msn_ppal_permissions'))."</div>');"
));
?>
<script type="text/javascript">
    //$("#destaque_<?php echo $sf_request->getParameter('id_nucleo') ?>").html("<img src='<?php echo sfConfig::get('app_base_dir_web') ?>images/preload.gif'/>");
    <?php if($status): ?>
            $.ajax
                ({
                    type: "POST",
                    url:  "<?php echo url_for('news/activeOptionDestaque') ?>",
                    data: "id_nucleo=<?php echo $sf_request->getParameter('id_nucleo') ?>&id_news=<?php echo $sf_request->getParameter('id_news') ?>",
                    success: function(msg)
                    {
                        $("#destaque_<?php echo $sf_request->getParameter('id_nucleo') ?>").html(msg);                    
                    }
                });

    <?php else: ?>
              $.ajax
                ({
                    type: "POST",
                    url:  "<?php echo url_for('news/desactiveOptionDestaque') ?>",
                    data: "id_nucleo=<?php echo $sf_request->getParameter('id_nucleo') ?>&id_news=<?php echo $sf_request->getParameter('id_news') ?>",
                    success: function(msg)
                    {
                        $("#destaque_<?php echo $sf_request->getParameter('id_nucleo') ?>").html(msg);                    
                    }
                });
    <?php endif; ?>
</script>
