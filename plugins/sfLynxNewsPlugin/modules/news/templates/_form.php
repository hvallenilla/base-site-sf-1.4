<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jq/jquery.validationEngine.js') ?>
<?php use_javascript('jq/jquery.validationEngine-en_US.js') ?>
<link rel="stylesheet" href="js/jq/jquery-ui-1.8.16.custom/development-bundle/themes/base/jquery.ui.all.css" />
<script src="/js/jq/jquery-ui-1.8.16.custom/development-bundle/ui/i18n/jquery.ui.datepicker-br.js"></script>
<?php  $appYml = sfConfig::get('app_upload_images_news'); ?>
<script type="text/javascript"> 
    $(document).ready(function() {
          $("#news").validationEngine();
          $("#sf_news_date").datepicker({
            dateFormat: 'yy-mm-dd',  
            yearRange: '-93:+1',
            changeMonth: true,
            changeYear: true
          });          
    })
</script>
<?php if($sf_user->getAttribute('idProfile') <= 2 || !$form->getObject()->getStatus() && $sf_user->getAttribute('idProfile') > 2 ):?>
<form id="news" action="<?php echo url_for('news/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id_news='.$form->getObject()->getIdNews() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php endif;?>
 <?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<div class="frameForm" align="center">
  <table width="100%">
      
      <?php if($sf_user->getAttribute('idProfile') <= 2 || !$form->getObject()->getStatus() && $sf_user->getAttribute('idProfile') > 2 ):?>
      <tr>
        <td>
            &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são obrigatórios')?>
        </td>
      </tr>
      <tr>
        <td id="errorGlobal">
            <?php echo $form->renderGlobalErrors() ?>
        </td>
      </tr>
      <tr>
          <td>
              <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                                <?php echo link_to(__('Voltar na lista'), '@default?module=news&action=index&'.$sf_user->getAttribute('uri_news'), array('class' => 'button')) ?>
                        </div>
                    </td>
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                                <?php echo link_to(__('Eliminar'), 'news/delete?id_news='.$form->getObject()->getIdNews(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                      <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                    <!--
                    <td>
                      <input type="submit" name="back" value="<?php echo __('Save and continue editing') ?>" />
                    </td>-->
                </tr>
            </table>
          </td>
      </tr>
    <tfoot>
      <tr>
        <td>
            <?php echo $form->renderHiddenFields(false) ?>
            <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                           <?php echo link_to(__('Voltar na lista'), '@default?module=news&action=index&'.$sf_user->getAttribute('uri_news'), array('class' => 'button')) ?>
                        </div>
                    </td>            
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Eliminar'), 'news/delete?id_news='.$form->getObject()->getIdNews(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                    <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                    <!--
                    <td>
                      <input type="submit" name="back" value="<?php echo __('Save and continue editing') ?>" />
                    </td>
                    -->
                </tr>
            </table>
        </td>
      </tr>
    </tfoot>
    <?php else: ?>
    <tr>
        <td>
            <div class="button" style="width: 100px;">
                    <?php echo link_to(__('Voltar na lista'), '@default?module=news&action=index&'.$sf_user->getAttribute('uri_news'), array('class' => 'button')) ?>
            </div>
        </td>
    </tr>
    <tr>
        <td class="msn_ready">
            Notícias só podem ser alterados do administrador do sistema
        </td>
      </tr>
    <?php endif; ?>
    <tbody>
        <tr>
            <td>                
                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                  <?php if($sf_user->getAttribute('idProfile') == 1 || $sf_user->getAttribute('idProfile') == 2):?>
                  <tr>
                      <td><?php echo $form['id_profile']->renderLabel() ?><br />
                        <?php echo $form['id_profile']->render(array('id' => 'id_profile', 'onchange' =>
                                jq_remote_function(array(
                                  'update' => 'id_parent',
                                  'before' => '$("#id_parent > option").remove();$("#id_parent").append("<option>Carregando seções...</option>"); ',
                                  'url'    => 'lxsection/getSectionByIdProfile',
                                  'with'   => " 'id=' + this.value ",
                                ))))
                        ?>  
                        <?php echo $form['id_profile']->renderError() ?>
                    </td>
                  </tr>
                  <?php endif; ?>
                  <?php if(sfConfig::get('app_newsConfig_categories')): ?>
                    <tr style="display: none;">
                        <td><?php echo $form['category']->renderLabel() ?><br />
                                <?php echo $form['category'] ?>
                                <?php echo $form['category']->renderError() ?>
                        </td>
                    </tr>
                  <?php endif; ?>
                    <tr>
                      <td>
                          <?php $module = 'news'; ?>
                          <table cellpadding="0" cellspacing="0" border="0" width="80%" style="margin-top: 15px; margin-bottom: 15px;">
                              <tr>
                                  <td width="3%" align="left" >
                                      <div id="imageFIELD" style="min-height: 110px; min-width: 170px;">
                                          <?php if($form->getObject()->getImage()):  ?>
                                          <?php echo image_tag('/uploads/'.$module.'/'.$appYml['size_1']['pref_1'].'_'.$form->getObject()->getImage(), 'class="borderImage" width="150" height="110"')?>
                                          <?php else:?>
                                          <?php echo image_tag('no_image.jpg', 'border=0 width="150" height="110" class="borderImage"');?>
                                          <?php endif;?>
                                      </div>
                                  </td>
                                  <td width="67%" valign="bottom" style="padding-left:7px">
                                      <?php echo $form['image']->renderLabel() ?><br />
                                      <?php echo $form['image'] ?>
                                      <?php echo $form['image']->renderError() ?>
                                      <span class="msn_help"><?php echo $form['image']->renderHelp() ?></span>
                                  </td>
                              </tr>
                              <tr>
                                   <td>
                                       <?php if($form->getObject()->getImage()):?>
                                      <div id="deleteImage" style="margin-left: 40px;" >
                                          <?php echo jq_link_to_remote('Deletar Imagem', array(
                                                'update'  =>  'imageFIELD',
                                                'url'     =>  $module.'/deleteImage?id='.$form->getObject()->getIdNews(),
                                                'script'  => true,
                                                'confirm' => __('Are you sure you want to delete the selected data?'),
                                                'before'  => "$('#imageFIELD').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');",
                                                'complete'=> "$('#deleteImage').html('');"
                                            ));
                                            ?>
                                      </div>
                                      <?php endif;?>
                                   </td>
                                   <td>&nbsp;</td>
                              </tr>
                          </table>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['title']->renderLabel() ?><br />
                        <?php echo $form['title'] ?>
                        <?php echo $form['title']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['date']->renderLabel() ?><br />
                        <?php echo $form['date']->render() ?>
                        <?php echo $form['date']->renderError(); ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['summary']->renderLabel() ?><br />
                        <?php echo $form['summary'] ?>
                        <?php echo $form['summary']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['body']->renderLabel() ?><br />
                        <?php echo $form['body'] ?>
                        <?php echo $form['body']->renderError() ?>
                    </td>
                  </tr>
                  
                  <?php if($sf_user->getAttribute('idProfile') == 1 || $sf_user->getAttribute('idProfile') == 2):?>
                  <tr style="display: none;">
                      <td><?php echo $form['status']->renderLabel() ?><br />
                        <?php echo $form['status'] ?>
                        <?php echo $form['status']->renderError() ?>
                    </td>
                  </tr>
                  <?php endif; ?>
            </table>
            </td>
        </tr>
    </tbody>
  </table>
    </div>
<?php if($sf_user->getAttribute('idProfile') <= 2 || !$form->getObject()->getStatus() && $sf_user->getAttribute('idProfile') > 2 ):?>
</form>
<?php endif; ?>
