<?php

/**
 * SfNewsAccess form base class.
 *
 * @method SfNewsAccess getObject() Returns the current form's model object
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseSfNewsAccessForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id_access'     => new sfWidgetFormInputHidden(),
      'id_nucleo'     => new sfWidgetFormInputText(),
      'id_news'       => new sfWidgetFormInputText(),
      'destaque'      => new sfWidgetFormInputText(),
      'data_destaque' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id_access'     => new sfValidatorPropelChoice(array('model' => 'SfNewsAccess', 'column' => 'id_access', 'required' => false)),
      'id_nucleo'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'id_news'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'destaque'      => new sfValidatorString(),
      'data_destaque' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('sf_news_access[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'SfNewsAccess';
  }


}
