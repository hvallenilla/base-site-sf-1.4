<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<script type="text/javascript"> 
$(document).ready(function() {
       $('input[type=checkbox]').live('click', function(){
            var parent = $(this).parent().attr('id');
            $('#'+parent+' input[type=checkbox]').removeAttr('checked');
            $(this).attr('checked', 'checked');
       });
       $(".u-e:checkbox").change(function(){
            var op = ($(this).val());
            //if($(this).attr("checked"))
            if(op == 1)
            {
                $("#field-url-externa").show();
                $("#field-secoe-url").hide();
            }else{
                $("#field-url-externa").hide();
                $("#field-secoe-url").show();
            }
        });
        $("#sf_section_status").change(function(){
            if($("#sf_section_status").val() == 1)
                {
                    $("#field-control").show();
                    <?php if(!$form->getObject()->isNew() && !$form->getObject()->getSwMenu()): ?>
                        $("#field-url-externa").show();
                        $("#opcoes-url-externa").show();
                    <?php endif; ?>
                    <?php if($form->getObject()->isNew()): ?>
                        $("#field-url-externa").show();
                        $("#opcoes-url-externa").show();
                    <?php endif; ?>
                    $("#field-secoe-url").hide();
                    
                }else{
                    $("#field-control").hide();
                    $("#field-url-externa").hide();
                    $("#opcoes-url-externa").hide();
                    $("#field-secoe-url").show();
                }
        });
        $("#sf_section_control").change(function(){
            if($("#sf_section_control").val() == 1)
                {
                    $("#field-url-externa").hide();
                    <?php if(!$form->getObject()->isNew() && !$form->getObject()->getSwMenu()): ?>                                
                        $("#opcoes-url-externa").show();          
                        $("#field-secoe-url").show();
                    <?php endif; ?>
                    <?php if($form->getObject()->isNew()): ?>
                        $("#opcoes-url-externa").show();          
                        $("#field-secoe-url").show();
                    <?php endif; ?>
                    //$("input#u-e").attr("checked", false);
                    $("#sf_section_url_externa").val('');
                }else{
                    $("#field-url-externa").hide();
                    $("#opcoes-url-externa").hide();
                    $("#field-secoe-url").show();
                    
                }
        });
        <?php if($sf_request->getParameter('id')):?>
            var id_section = <?php echo $sf_request->getParameter('id') ?>;
        <?php else: ?>
            var id_section = 0;
        <?php endif; ?>
        
        $("#sf_section_sw_menu").blur(function(){
            validateSecaoUrl($("#sf_section_sw_menu").val(), id_section);
        });
        <?php if(!$form->getObject()->isNew()):?>
            <?php if($form->getObject()->getSwMenu() && !$form->getObject()->getUrlExterna()): ?>
                <?php $checkedUE1 = "" ?>
                <?php $checkedUE0 = "checked" ?>
                $("#field-url-externa").hide();
                $("#opcoes-url-externa").hide();
                $("#field-secoe-url").show();
            <?php else: ?>
                <?php $checkedUE1 = "checked" ?>
                <?php $checkedUE0 = "" ?>
                $("#field-url-externa").show();
                $("#field-secoe-url").hide();
            <?php endif; ?>
            <?php if(!$form->getObject()->getControl()):?>
                $("#field-url-externa").hide();
                $("#opcoes-url-externa").hide();
                $("#field-secoe-url").show();
                <?php $checkedUE1 = "" ?>
                <?php $checkedUE0 = "checked" ?>
            <?php endif; ?>    
        <?php else: ?>
            
        <?php endif; ?>
            
        $("#toogle-info-secao").click(function () {
            $("#info-secao").toggle("slow");
            $("#toogle-info-secao").html('>>');
        },function () {
            $("#info-secao").toggle("slow");
            $("#toogle-info-secao").html('<<');
        });
        $("#toogle-info-conteudo").toggle(function () {
            $("#info-conteudo").toggle("slow");
            $("#toogle-info-conteudo").html('>>');
        },function () {
            $("#info-conteudo").toggle("slow");
            $("#toogle-info-conteudo").html('<<');
        });
})
</script>
<?php if ($sf_user->hasFlash('listo')): ?>
    <div class="msn_ready"><?php echo $sf_user->getFlash('listo') ?></div>
<?php endif; ?>
<form id="lxsection" action="<?php echo url_for('lxsection/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<div class="frameForm" align="center" style="border: 1px #e6e8dd solid;">
    <h2>Informações de Seção <a href="javascript:void(0);" id="toogle-info-secao" style="float: right;"> << </a></h2>
    
    
    <table width="100%" id="info-secao">
        <tr>
            <td id="errorGlobal">
                <?php echo $form->renderGlobalErrors() ?>
            </td>
        </tr>
        <tr>
          <td>
              &nbsp;<?php echo __('Os campos marcados com ') ?> <span class="required">*</span> <?php echo __('são obrigatórios')?>
          </td>
        </tr>
        <tfoot>
            <tr>
                <td>
                    <?php echo $form->renderHiddenFields(false) ?>
                    <table cellspacing="4">
                        <tr>
                            <td>
                                <div class="button">
                                   <?php if($sf_request->getParameter('back')):?>
                                        <?php echo link_to(__('Voltar'), '@default?module=lxsection&action=editContenido&id='.$sf_request->getParameter('id').'&language='.$sf_request->getParameter('language').'', array('class' => 'button')) ?>
                                    <?php else:?>
                                        <?php echo link_to(__('Voltar na lista'), '@default?module=lxsection&action=index&'.$sf_user->getAttribute('uri_lxsection'), array('class' => 'button')) ?>
                                    <?php endif;?>
                                </div>
                            </td>            
                            <?php if (!$form->getObject()->isNew() and $form->getObject()->getDelete()): ?>
                            <td>
                                <div class="button">
                                                    <?php echo link_to(__('Eliminar'), 'lxsection/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => __('Voc� tem certeza de que deseja excluir os dados selecionados?'), 'class' => 'button')) ?>
                                                </div>
                            </td>
                            <?php endif; ?>
                            <td>
                            <input type="submit" value="<?php echo __('Salvar') ?>" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <tr>
                <td>                
                    <table cellpadding="5" cellspacing="10" border="0" width="100%">
                      <?php if($sf_user->getAttribute('idProfile') == 1 || $sf_user->getAttribute('idProfile') == 2):?>
                      <tr>
                          <td><?php echo $form['id_profile']->renderLabel() ?><br />
                            <?php echo $form['id_profile']->render(array('id' => 'id_profile', 'onchange' =>
                                    jq_remote_function(array(
                                      'update' => 'id_parent',
                                      'before' => '$("#id_parent > option").remove();$("#id_parent").append("<option>Carregando seções...</option>"); ',
                                      'url'    => 'lxsection/getSectionByIdProfile',
                                      'with'   => " 'id=' + this.value ",
                                    ))))
                            ?>  
                            <?php echo $form['id_profile']->renderError() ?>
                        </td>
                      </tr>
                      <?php endif; ?>
                      <tr>
                          <td>
                            <?php echo $form['id_parent']->renderLabel() ?><br />
                            <?php echo $form['id_parent']->render(array('value' => 4)); ?>

                            <?php echo $form['id_parent']->renderError() ?>
                        </td>
                      </tr>                  
                      <tr>
                          <td>
                            <?php echo $form['status']->renderLabel() ?><br />                        
                            <?php echo $form['status'] ?>                        
                            <?php echo $form['status']->renderError() ?>
                        </td>
                      </tr>
                      <tr style="display: none;">
                          <td><?php echo $form['show_text']->renderLabel() ?><br />
                            <?php echo $form['show_text'] ?>
                            <?php echo $form['show_text']->renderError() ?>
                            <span class="msn_help"><?php echo $form['show_text']->renderHelp() ?></span>
                        </td>
                      </tr>                  
                      <tr id="field-control">
                          <td><?php echo $form['control']->renderLabel() ?><br />
                            <?php echo $form['control'] ?>
                            <?php echo $form['control']->renderError() ?>
                        </td>
                      </tr>            

                      <tr id="opcoes-url-externa">
                          <td>
                              <div id="select-url_externa">
                                <label>URL Externa</label><br /><br />
                                <input type="checkbox" value="1" id="u-e" class="u-e" name="chk" <?php echo $checkedUE1 ?> /> Sim
                                <input type="checkbox" value="0" id="u-e" class="u-e" name="chk" <?php echo $checkedUE0 ?> /> Não<br/>                            
                              </div>
                          </td>
                      </tr>
                      <tr id="field-url-externa" style="display: none;">
                          <td><label>Gravar a página que você deseja chamar</label><br />
                             <?php echo $form['url_externa'] ?>
                            <?php echo $form['url_externa']->renderError() ?>
                        </td>
                      </tr>                  
                      <tr id="field-secoe-url" style="display: none;">
                          <td><?php echo $form['sw_menu']->renderLabel() ?><br />
                              <a href="javascript:void(0);">http://www.carapicuiba.sp.gov.br/</a>&nbsp;&nbsp;<?php echo $form['sw_menu'] ?>
                            <span class="validate-secao">&nbsp;</span>
                            <br />
                            <?php echo $form['sw_menu']->renderError() ?>
                            <span class="msn_help"><?php echo $form['sw_menu']->renderHelp() ?></span>
                        </td>
                      </tr>
                      <?php //if ($sf_user->hasCredential('admin_lynx')): ?>
                      <tr style="display: none;">
                          <td><?php echo $form['delete']->renderLabel() ?><br />
                            <?php echo $form['delete'] ?>
                            <?php echo $form['delete']->renderError() ?>
                        </td>
                      </tr>
                      <?php //endif;?>
                      <tr style="display: none;">
                          <td><?php echo $form['only_complement']->renderLabel() ?><br />
                            <?php echo $form['only_complement'] ?>
                            <?php echo $form['only_complement']->renderError() ?>
                        </td>
                      </tr>
                      <tr style="display: none;">
                          <td><?php echo $form['home']->renderLabel() ?><br />
                            <?php echo $form['home'] ?>
                            <?php echo $form['home']->renderError() ?>
                        </td>
                      </tr>
                      
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td><h3 style="font-size: 13px; text-transform: uppercase;">Só para o site do desenvolvedor</h3></td>
                      </tr>
                      <tr>
                          <td><?php echo $form['special_page']->renderLabel() ?><br />
                            <?php echo $form['special_page'] ?>
                            <?php echo $form['special_page']->renderError() ?>
                        </td>
                      </tr>  
                      
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>

</form>
<?php if(!$form->getObject()->isNew()): ?>
<div class="frameForm" align="center" style="border: 1px #e6e8dd solid;">     
<h2>Informações de Conteúdo <a href="javascript:void(0);" id="toogle-info-conteudo" style="float: right;"> << </a></h2>

<form id="lxsectioni18n" action="<?php echo url_for('lxsection/saveInfoI18n?id='.$formI18n->getObject()->getId().'&language='.$formI18n->getObject()->getLanguage()) ?>" method="post" <?php $formI18n->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<table cellpadding="0" cellspacing="6" border="0" id="info-conteudo" >
    <tr>
        <td>
            <?php echo $formI18n['name_section']->renderLabel() ?><br />
            <?php echo $formI18n['name_section'] ?>
            <?php echo $formI18n['name_section']->renderError() ?>
        </td>
    </tr>
    <tr>
        <td>
            <br /><b><?php echo __('HTML Meta')?></b><br />
            <span class="msn_help"><?php echo __('HTML Meta fornece informa&ccedil;&atilde;o sobre a p&aacute;gina  atual. Esta informa&ccedil;&atilde;o pode mudar de uma p&aacute;gina para outra porque n&atilde;o conte  nome o presets de etiquetas.')?></span>
            <br />
        </td>
    </tr>
    <tr>
        <td><?php echo $formI18n['meta_title']->renderLabel() ?><br />
            <?php echo $formI18n['meta_title'] ?>
            <?php echo $formI18n['meta_title']->renderError() ?>
        </td>
    </tr>
    <tr>
        <td><?php echo $formI18n['meta_description']->renderLabel() ?><br />
        <?php echo $formI18n['meta_description'] ?>
        <?php echo $formI18n['meta_description']->renderError() ?>
          <span class="msn_help"><?php echo $formI18n['meta_description']->renderHelp() ?></span>
        </td>
    </tr>
    <tr>
        <td><?php echo $formI18n['meta_keyword']->renderLabel() ?><br />
          <?php echo $formI18n['meta_keyword'] ?>
          <?php echo $formI18n['meta_keyword']->renderError() ?>
            <span class="msn_help"><?php echo $formI18n['meta_keyword']->renderHelp() ?></span>
        </td>
    </tr>
    <tr>
        <td >
            <table cellpadding="0" cellspacing="2" border="0" >              
              <tr>
                  <td>
                    <br /><br />
                    <?php $fck = new sfWidgetFormRichTextarea();?>
                    <?php $fck->setOptions(array('tool'=>'Custom','height' => '400','width' => '800','skin' => 'office2003')) ?>
                    <?php echo $fck->render('descrip_section_'.$formI18n->getObject()->getLanguage(), $sf_section->getDescripSection()); ?>
                  </td>
              </tr>              
           </table>
        </td>
    </tr>
    <tr>
        <td valign="top" align="left">
            <table cellpadding="0" cellspacing="6" border="0" width="100%" align="left">                
                
              <tr>
                    <td><?php echo $formI18n->renderHiddenFields(false) ?>
                        <table cellspacing="4">
                            <tr>
                                <td>
                                    <div class="button">
                                       <?php echo link_to(__('Voltar na lista'), '@default?module=lxsection&action=index&'.$sf_user->getAttribute('uri_lxsection'), array('class' => 'button')) ?>
                                    </div>
                                </td>
                                <td>
                                <input type="submit" value="<?php echo __('Salvar') ?>" />
                                </td>
                            </tr>
                        </table>
                    </td>
                  </tr>
            </table>
        </td>
    </tr>    
</table>


</form>
</div>
<?php endif; ?>