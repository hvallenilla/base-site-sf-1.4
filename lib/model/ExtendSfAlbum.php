<?php
class ExtendSfAlbum extends SfAlbumPeer
{
  /**
   * List Gallery of New
   * @param <integer> $idNew
   * @return <array>
   */
  public static function viewGalleryNews($idNucleo, $limit = "")
  {
    $c = new Criteria();
    $c->clearSelectColumns();
    $c->addSelectColumn(self::ID_ALBUM);
    $c->addSelectColumn(self::ID_RELATION);
    $c->addSelectColumn(self::ALBUM_NAME);
    $c->addSelectColumn(SfAlbumContentPeer::CAPTION);
    $c->addSelectColumn(SfAlbumContentPeer::IMAGE);
    $c->add(self::STATUS, 1);
    $c->add(self::ID_RELATION, $idNucleo, Criteria::EQUAL);
    $c->addJoin(self::ID_ALBUM, SfAlbumContentPeer::ID_ALBUM, Criteria::INNER_JOIN);
    $c->addAscendingOrderByColumn(SfAlbumContentPeer::POSITION);
    /*if($limit)
    { */

        $c->setLimit(10);
    //}
    $rs = self::doSelectStmt($c);
    //Se recuperan los registros y se genera arreglo
    while($res = $rs->fetch())
    {
        $image['id_album'] = $res['ID_ALBUM'];
        $image['id_news'] = $res['ID_RELATION'];
        $image['album_name'] = $res['ALBUM_NAME'];
        $image['image'] = $res['IMAGE'];
        $image['caption'] = $res['CAPTION'];
        $gallery[] = $image;
    }
    if (!empty($gallery)){
            return $gallery;
    }else{
            return false;
    }
  }
  
  public static function viewListAlbunsByIdByNucleo($idNucleo, $idAlbum)
  {
    $c = new Criteria();
    $c->clearSelectColumns();

    $c->addSelectColumn(self::ID_ALBUM);
    $c->addSelectColumn(self::ID_RELATION);
    $c->addSelectColumn(self::ALBUM_NAME);
    $c->addSelectColumn(self::RESUMO);
    $c->addSelectColumn(SfAlbumContentPeer::CAPTION);
    $c->addSelectColumn(SfAlbumContentPeer::IMAGE);
    
    //$c->add(self::STATUS, 1, Criteria::EQUAL);
    $c->add(self::ID_ALBUM, $idAlbum, Criteria::EQUAL);
    //$c->add(self::ID_RELATION, $idNucleo, Criteria::EQUAL);
    $c->add(SfAlbumContentPeer::IMAGE, "", Criteria::NOT_EQUAL);
    
    $c->addJoin(self::ID_ALBUM, SfAlbumContentPeer::ID_ALBUM, Criteria::INNER_JOIN);
    //$c->addJoin(self::ID_ALBUM, SfAlbumAccessPeer::ID_ALBUM, Criteria::INNER_JOIN);
    //$c->add(SfAlbumAccessPeer::ID_NUCLEO, $idNucleo, Criteria::EQUAL);
    $rs = self::doSelectStmt($c);
    //Se recuperan los registros y se genera arreglo
    while($res = $rs->fetch())
    {
        $image['id_album'] = $res['ID_ALBUM'];
        $image['id_nucleo'] = $res['ID_RELATION'];
        $image['album_name'] = $res['ALBUM_NAME'];
        $image['resumo'] = $res['RESUMO'];
        $image['image'] = $res['IMAGE'];
        $image['caption'] = $res['CAPTION'];
        $gallery[] = $image;
    }
    if (!empty($gallery)){
            return $gallery;
    }else{
            return false;
    }
  }

  public static function viewListAlbunsNews($idNucleo, $limit = false)
  {
    $c = new Criteria();
    $c->clearSelectColumns();

    $c->addSelectColumn(self::ID_ALBUM);
    $c->addSelectColumn(self::ID_RELATION);
    $c->addSelectColumn(self::ALBUM_NAME);
    $c->addSelectColumn(self::RESUMO);
    $c->addSelectColumn(SfAlbumContentPeer::CAPTION);
    $c->addSelectColumn(SfAlbumContentPeer::IMAGE);

    $c->add(self::STATUS, 1, Criteria::EQUAL);
    //$c->addJoin(self::ID_ALBUM, SfAlbumAccessPeer::ID_ALBUM, Criteria::INNER_JOIN);
    //$c->add(SfAlbumAccessPeer::ID_NUCLEO, $idNucleo, Criteria::EQUAL);
    //$c->add(self::ID_RELATION, $idNucleo, Criteria::EQUAL);
    $c->addDescendingOrderByColumn(self::DATA);
    $c->add(SfAlbumContentPeer::IMAGE, "", Criteria::NOT_EQUAL);
    if($limit)
    {
        $c->setLimit($limit);
    }
    $c->addJoin(self::ID_ALBUM, SfAlbumContentPeer::ID_ALBUM, Criteria::INNER_JOIN);
    $c->setDistinct(self::ID_ALBUM);
    $c->addGroupByColumn(self::ID_ALBUM);
    $rs = self::doSelectStmt($c);
    //Se recuperan los registros y se genera arreglo
    while($res = $rs->fetch())
    {
        $image['id_album']      = $res['ID_ALBUM'];
        $image['id_nucleo']     = $res['ID_RELATION'];
        $image['album_name']    = $res['ALBUM_NAME'];
        $image['resumo']        = $res['RESUMO'];
        $image['image']         = $res['IMAGE'];
        $image['caption']       = $res['CAPTION'];
        $gallery[]              = $image;
    }
    if (!empty($gallery)){
            return $gallery;
    }else{
            return false;
    }
  }
  
  public static function getCountPagination($idNucleo)
  {
    $c = new Criteria();
    $c->clearSelectColumns();
    if($idNucleo != 29)
    {
    $c->addJoin(self::ID_ALBUM, SfAlbumAccessPeer::ID_ALBUM, Criteria::INNER_JOIN);
    $c->add(SfAlbumAccessPeer::ID_NUCLEO, $idNucleo, Criteria::EQUAL);
    }
    $c->add(self::STATUS, 1, Criteria::EQUAL);
    return self::doCount($c);
  }
  
  public static function getGaleriaPaginate($idNucleo, $start, $limit )
  {
    $c = new Criteria();
    $c->clearSelectColumns();

    $c->addSelectColumn(self::ID_ALBUM);
    $c->addSelectColumn(self::ID_RELATION);
    $c->addSelectColumn(self::ALBUM_NAME);
    $c->addSelectColumn(self::RESUMO);
    $c->addSelectColumn(SfAlbumContentPeer::CAPTION);
    $c->addSelectColumn(SfAlbumContentPeer::IMAGE);

    $c->add(self::STATUS, 1, Criteria::EQUAL);
    if($idNucleo != 29)
    {
        $c->addJoin(self::ID_ALBUM, SfAlbumAccessPeer::ID_ALBUM, Criteria::INNER_JOIN);
        $c->add(SfAlbumAccessPeer::ID_NUCLEO, $idNucleo, Criteria::EQUAL);
    }
    
    //$c->add(self::ID_RELATION, $idNucleo, Criteria::EQUAL);
    $c->addDescendingOrderByColumn(self::DATA);
    $c->add(SfAlbumContentPeer::IMAGE, "", Criteria::NOT_EQUAL);
    if ($start) 
    {
            $c->setLimit($limit);
            $c->setOffset($start);
    } else {
        $c->setLimit($limit);
    }
    $c->addJoin(self::ID_ALBUM, SfAlbumContentPeer::ID_ALBUM, Criteria::INNER_JOIN);
    $c->setDistinct(self::ID_ALBUM);
    $c->addGroupByColumn(self::ID_ALBUM);
    $rs = self::doSelectStmt($c);
    //Se recuperan los registros y se genera arreglo
    while($res = $rs->fetch())
    {
        $image['id_album']      = $res['ID_ALBUM'];
        $image['id_nucleo']     = $res['ID_RELATION'];
        $image['album_name']    = $res['ALBUM_NAME'];
        $image['resumo']        = $res['RESUMO'];
        $image['image']         = $res['IMAGE'];
        $image['caption']       = $res['CAPTION'];
        $gallery[]              = $image;
    }
    if (!empty($gallery)){
            return $gallery;
    }else{
            return false;
    }
  
  }
}
?>
