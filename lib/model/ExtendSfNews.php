<?php
class ExtendSfNews extends SfNewsPeer {

  public static function doSelectNewsForHome()
  {
    $c = new Criteria();
    $c->add(self::STATUS, '1');
    $c->add(self::HOME, '1');
    $c->addDescendingOrderByColumn(self::STICKY);
    $c->addAscendingOrderByColumn('rand()');
    $c->setLimit(2);
    return self::doSelect($c);
  }

  /***
   * Select news
   */
  public static function doSelectNews($quantity = 2, $orderType = 'Asc', $orderField = 'rand()', $category = 0, $filterField = 0, $home = 0)
  {
    $c = new Criteria();
    $c->add(self::STATUS, '1');
    if($home == 1)
      $c->add(self::HOME, '1');
    $c->add(self::CATEGORY, $category, Criteria::EQUAL);
    if($orderType == 'Desc')
    {
      $c->addDescendingOrderByColumn($orderField);
    }else{
      $c->addAscendingOrderByColumn($orderField);
    }
    $c->setLimit($quantity);
    return self::doSelect($c);
  }

  /***
   * Select news only for home
   */
  public static function doSelectNewsArticlesInterviewsForHome()
  {
    $c = new Criteria();
    $c->add(self::STATUS, '1');
    $c->add(self::HOME, '1');
    $c->add(self::CATEGORY, '0');
    $c->addOr(self::CATEGORY, '1');
    $c->addDescendingOrderByColumn('rand()');
    $c->setLimit('10');
    return self::doSelect($c);
  }

  /***
   * List of news
   */
  public static function selectNews($showPage, $category = 0)
  {
    $c = new Criteria();
    $c->add(self::STATUS, '1');
    //$c->add(SfNewsPeer::CATEGORY, $category, Criteria::EQUAL);
    $c->addDescendingOrderByColumn(self::DATE);
    $pager = new sfPropelPager('SfNews', 100);
    $pager->setCriteria($c);
    $pager->setPage($showPage);
    $pager->init();
    return $pager;
    //return self::doSelect($c);
  }
  /***
   * List of news
   */
  public static function selectNewsLike($showPage, $category = 0, $like)
  {
    $c = new Criteria();
    $c->add(self::STATUS, '1');
    $criterio = $c->getNewCriterion(self::TITLE, '%'.$like.'%', Criteria::LIKE);
    $criterio->addOr($c->getNewCriterion(self::BODY, '%'.$like.'%', Criteria::LIKE));
    $c->add($criterio);
    //$c->add(SfNewsPeer::CATEGORY, $category, Criteria::EQUAL);
    $c->addDescendingOrderByColumn(self::DATE);
    
    $pager = new sfPropelPager('SfNews', 100);
    $pager->setCriteria($c);
    $pager->setPage($showPage);
    $pager->init();
    return $pager;
    //return self::doSelect($c);
  }

    /***
   * List of news
   */
  public static function selectListNews($showPage, $nucleo)
  {
    $c = new Criteria();
    $c->add(self::STATUS, '1');
    //$c->addJoin(self::ID_NEWS, SfNewsAccessPeer::ID_NEWS, Criteria::INNER_JOIN);
    //$c->add(SfNewsAccessPeer::ID_NUCLEO, $nucleo, Criteria::EQUAL);

    $c->addDescendingOrderByColumn(self::DATE);
    $pager = new sfPropelPager('SfNews', 30);
    $pager->setCriteria($c);
    $pager->setPage($showPage);
    $pager->init();
    return $pager;
    //return self::doSelect($c);
  }
  
  public static function selectUltimasNoticias($showPage, $nucleo)
  {
    $c = new Criteria();
    $c->add(self::STATUS, '1');
    //$c->addJoin(self::ID_NEWS, SfNewsAccessPeer::ID_NEWS, Criteria::INNER_JOIN);
    //$c->add(SfNewsAccessPeer::ID_NUCLEO, $nucleo, Criteria::EQUAL);

    $c->addDescendingOrderByColumn(self::DATE);
    $pager = new sfPropelPager('SfNews', 5);
    $pager->setCriteria($c);
    $pager->setPage($showPage);
    $pager->init();
    return $pager;
    //return self::doSelect($c);
  }

  /***
   * Show details for selected news
   */
  public static function selectNewsDetail($permalink)
  {
    $c = new Criteria();
    $c->add(SfNewsPeer::PERMALINK, $permalink);
    //$c->add(SfNewsPeer::STATUS, '1');
    return SfNewsPeer::doSelectOne($c);
  }
}
