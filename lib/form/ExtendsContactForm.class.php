<?php

/**
 * Extend Contact form.
 *
 * @package    perigen
 * @subpackage form
 * @author     Your name here
 */
class ExtendsContactForm extends BaseContactForm {


    public function configure() {
        
        $id_nucleo = sfContext::getInstance()->getRequest()->getParameter('nucleo');
        $id_nucleo = LxProfilePeer::getDataByPermalink($id_nucleo);
        
        $this->widgetSchema['first_name']->setAttributes(array('class' => 'validate[required]','size' => '35','maxlength' => '100'));
        $this->widgetSchema['last_name']->setAttributes(array('class' => 'validate[required]','size' => '35','maxlength' => '100'));
        $this->widgetSchema['email']->setAttributes(array('class' => 'validate[required,custom[email]]','size' => '35','maxlength' => '100'));
        $this->widgetSchema['contact_type'] = new sfWidgetFormInputHidden();
        if(sfContext::getInstance()->getRequest()->getParameter('secciones') == 'schedule'){
        $this->setDefault('contact_type', '2');
        }else{
        $this->setDefault('contact_type', '1');
        }
        //$this->widgetSchema['captcha'] = new sfWidgetFormReCaptcha(array('public_key' => sfConfig::get('app_recaptcha_public_key')));


        /**
        * Labels
        */
        $this->widgetSchema->setLabels(array(
        'first_name'     => 'Nome <span style="color:red">*</span>',
        'last_name'      => 'Sobrenome <span style="color:red">*</span>',
        'email'          => 'Email <span style="color:red">*</span>',
        'comments'       => 'Comentar ',
        'contact_type'   => 'Contact Type',
        ));
        //$this->validatorSchema['captcha'] = new sfValidatorReCaptcha(array('private_key' => sfConfig::get('app_recaptcha_private_key')));
        $this->validatorSchema['first_name'] = new sfValidatorString(array('required' => true, 'trim' => true));
        $this->validatorSchema['last_name'] = new sfValidatorString(array('required' => true, 'trim' => true));
        $this->validatorSchema['email'] = new sfValidatorString(array('required' => true,'trim' => true));
        $this->validatorSchema['comments'] = new sfValidatorString(array('required' => false,'trim' => true));

        unset($this['id_contact']);
    }
}
