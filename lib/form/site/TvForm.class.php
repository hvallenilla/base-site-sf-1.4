<?php

/**
 * Tv form.
 *
 * @package    carapicuiba
 * @subpackage form
 * @author     Henry Vallenilla <henryvallenilla@gmail.com>
 */
class TvForm extends BaseTvForm
{
  public function configure()
  {
      $idProfile = sfContext::getInstance()->getUser()->getAttribute('idProfile');
      $profiles = LxProfilePeer::findProfilesCombo();
      $this->widgetSchema['status'] = new sfWidgetFormInputHidden();
      // Widgets      
      if($idProfile > 2)
      {
        $this->widgetSchema['id_profile'] = new sfWidgetFormInputHidden();
        $this->setDefault('id_profile',$idProfile);
      }else{
        $this->widgetSchema['id_profile'] = new sfWidgetFormChoice(array('choices'  => $profiles,'expanded' => false),array());
      }
      
      $this->widgetSchema['titulo_tv']->setAttributes(array('class' => 'validate[required]','size' => 60,'maxlength' => '100'));
      $this->widgetSchema['resumo_tv'] = new sfWidgetFormTextarea(array(), array('cols' => 70, 'rows' => '8'));
      $this->widgetSchema['data'] = new sfWidgetFormInputHidden();
      $this->widgetSchema['skin'] = new sfWidgetFormInputFileEditable(array(
        'file_src' => sfConfig::get('sf_upload_dir').'/news/'.$this->getObject()->getSkin(),
        'is_image'  => true,
        'edit_mode' => !$this->isNew(),
        'template'      => '<div>%file%<br />%input%<br /></div>',
        ));
      
      // set default
      if($this->getObject()->isNew())
      {
          $this->setDefault('data',date("Y-m-d"));
          $this->setDefault('status','1');
      }
      
      // validators
      $this->validatorSchema['titulo_tv'] = new sfValidatorString(array('required' => true));
      $this->validatorSchema['resumo_tv'] = new sfValidatorString(array('required' => false));
      $this->validatorSchema['skin'] = new sfValidatorFile(array(
        'required'   => false,
        'max_size'   => sfConfig::get('app_image_size'),
        'mime_types' => array('image/jpeg','image/pjpeg','image/png','image/gif'),
       ));
      $this->validatorSchema['skin'] = new sfImageFileValidator(array(
            'required'      => false,
            'mime_types'    => array('image/jpeg', 'image/png', 'image/gif', 'image/pjpeg'),
            'max_size'      => sfConfig::get('app_image_size'),
            'min_height'    => '171',
            'min_width'     => '228',
            'path'          => false,
        ), array(
            'min_width'     => "A largura de imagem é muito curto (e mínimo %min_width%px, tem %width%px ).",
            'min_height'    => "A altura mínima da imagem deve ser %min_height%px."            
    ));
      
    $this->validatorSchema['skin']->setMessage('mime_types','Error mime types %mime_type%.');
    
    //Etiquetas
    $this->widgetSchema->setLabels( array(
        'id_profile'    => 'Instituição: ',
        'titulo_tv'     => 'Título de vídeo: ',
        'resumo_tv'     => 'Resumo: ',
        'skin'          => 'Skin Vídeo'
    ));
    
    $appYml = sfConfig::get('app_upload_images_video');
    
    //Help Messages
    $this->widgetSchema->setHelps(array(
        'image'     => 'A imagem deve ser JPEG, JPG, PNG ou GIF<br />
                        A imagem deve ter um tamanho Maximo de '.(sfConfig::get('app_image_size_text')).'<br />
                        A imagem deve ter um tamanho mínimo de '.$appYml['size_1']['image_width_1'].' x '.$appYml['size_1']['image_height_1'].' pixels',

    ));
      
    unset($this['video'],$this['swf']);
  }
  
  protected function doSave($con = null)
  {
      $module = 'videos';
      $appYml = sfConfig::get('app_upload_images_video');
      // Si hay un nuevo archivo por subir y ya mi registro tiene un archivo asociado entonces,
      if ($this->getObject()->getSkin() && $this->getValue('skin'))
      {
          // recorro y elimino
          for($i=1;$i<=$appYml['copies'];$i++)
          {
              // Elimino las fotos de la carpeta
              if(is_file(sfConfig::get('sf_upload_dir').'/'.$module.'/'.$appYml['size_'.$i]['pref_'.$i].'_'.$this->getObject()->getSkin()))
              {
                unlink(sfConfig::get('sf_upload_dir').'/'.$module.'/'.$appYml['size_'.$i]['pref_'.$i].'_'.$this->getObject()->getSkin());
              }
          }
      }
      return parent::doSave($con);
  }
}
