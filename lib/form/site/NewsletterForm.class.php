<?php

/**
 * Newsletter form.
 *
 * @package    abge
 * @subpackage form
 * @author     Your name here
 */
class NewsletterForm extends BaseNewsletterForm
{
  public function configure()
  {
      
      $this->widgetSchema['nombre']->setAttributes(array('class' => 'validate[required]', 'size' => 30));
      $this->widgetSchema['email']->setAttributes(array('class' => 'validate[required]', 'size' => 30));
      
      $this->disableLocalCSRFProtection();

      $this->validatorSchema['nombre']->setOption('required', true);
      $this->validatorSchema['email']->setOption('required', true);
      
      // Agrega un post validador personalizado
      $this->validatorSchema->setPostValidator(
           new sfValidatorCallback(array('callback' => array($this, 'checkUserAndPassword')))
      );
  }
  
  public function checkUserAndPassword($validator, $values) 
  {
        if (!empty($values['nombre']) && !empty($values['email'])) {
            $this->dataUser = NewsletterPeer::validateUserNewsletter($values['email']);
            if($this->dataUser) {
                $error = new sfValidatorError($validator, 'O e-mail digitado já está registrado');
                throw new sfValidatorErrorSchema($validator, array('Error' => $error));
            }
        }
        return $values;
  }
}
