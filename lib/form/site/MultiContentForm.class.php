<?php

/**
 * MultiContent form.
 *
 * @package    joaopaulo
 * @subpackage form
 * @author     Henry Vallenilla <henryvallenilla@gmail.com>
 */
class MultiContentForm extends BaseMultiContentForm
{
  public function configure()
  {
        $id_category = sfContext::getInstance()->getUser()->getAttribute('id_category');
        
        $this->widgetSchema['id_category'] = new sfWidgetFormInputHidden(array(), array('value' => $id_category)); 
        $this->widgetSchema['data_created'] = new sfWidgetFormInputText(); 
        $this->widgetSchema['titulo']->setAttributes(array('class' => 'validate[required]', 'size' => '80'));
        $this->widgetSchema['resumo'] = new sfWidgetFormRichTextarea(array('tool'=>'Custom','width' => '750', 'height' => '400'),array('class' => 'validate[required]'));
        $this->widgetSchema['arquivo'] = new sfWidgetFormInputFileEditable(array(
            'file_src' => '',
            'is_image'  => true,
            'edit_mode' => !$this->isNew(),
            'template'      => '<div>%file%<br />%input%<br /></div>',
        ));
        $this->widgetSchema['imagen'] = new sfWidgetFormInputFileEditable(array(
            'file_src' => '',
            'is_image'  => true,
            'edit_mode' => !$this->isNew(),
            'template'      => '<div>%file%<br />%input%<br /></div>',
        ));
        
        $this->validatorSchema['titulo'] = new sfValidatorString(array('required' => true, 'trim' => true));
        $this->validatorSchema['resumo']  = new sfValidatorString(array('required' => false, 'trim' => true));
        $this->validatorSchema['arquivo'] = new sfValidatorFile(array(
            'required'   => false,
            'max_size'   => '20145728',
            'mime_types' => array('image/jpeg','image/pjpeg','image/png','image/gif', 'application/vnd.ms-excel', 'text/plain', 'application/vnd.ms-powerpoint', 'application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ),
        ));   
        $this->validatorSchema['imagen'] = new sfValidatorFile(array(
            'required'   => false,
            'max_size'   => sfConfig::get('app_image_size'),
            'mime_types' => array('image/jpeg','image/pjpeg','image/png','image/gif'),
        ));
        
        $this->validatorSchema['imagen']->setMessage('mime_types','Error mime types %mime_type%.');

        //Etiquetas
        $this->widgetSchema->setLabels(array(
              'titulo'      => 'Titulo <span class="required">*</span>',
              'resumo'      => 'Resumo <span class="required">*</span>',              
              'arquivo'     => 'Arquivo Associado',
              'data_created'     => 'Data',
              'imagen'     => 'Imagem',
        ));
        //Help Messages
        $this->widgetSchema->setHelps(array(
            'imagen'     => 'A imagem deve ser JPEG, JPG, PNG ou GIF<br />
                        A imagem deve ter mínimo <b>580px</b> de largura',

        ));
        unset($this['permalink']);
  }
  
  protected function doSave($con = null)
  {
      $directorio = sfConfig::get('sf_upload_dir').'/imagescontent/';
      if ($this->getObject()->getImagen() && $this->getValue('imagen'))
      {
        $filename = $directorio."big_".$this->getObject()->getImagen(); 
        unlink($filename);     
        $filename = $directorio."min_".$this->getObject()->getImagen(); 
        unlink($filename);     
      }
      $directorio = sfConfig::get('sf_upload_dir').'/arquivos/';
      if ($this->getObject()->getArquivo() && $this->getValue('arquivo'))
      {
        $filename = $directorio.$this->getObject()->getArquivo(); 
        unlink($filename);     
      }
  }
  
}
