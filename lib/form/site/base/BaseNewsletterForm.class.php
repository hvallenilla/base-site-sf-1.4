<?php

/**
 * Newsletter form base class.
 *
 * @method Newsletter getObject() Returns the current form's model object
 *
 * @package    joaopaulo
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseNewsletterForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id_newsletter' => new sfWidgetFormInputHidden(),
      'nombre'        => new sfWidgetFormInputText(),
      'email'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id_newsletter' => new sfValidatorPropelChoice(array('model' => 'Newsletter', 'column' => 'id_newsletter', 'required' => false)),
      'nombre'        => new sfValidatorString(array('max_length' => 150)),
      'email'         => new sfValidatorString(array('max_length' => 100)),
    ));

    $this->widgetSchema->setNameFormat('newsletter[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Newsletter';
  }


}
