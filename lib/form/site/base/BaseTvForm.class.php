<?php

/**
 * Tv form base class.
 *
 * @method Tv getObject() Returns the current form's model object
 *
 * @package    joaopaulo
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseTvForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id_tv'      => new sfWidgetFormInputHidden(),
      'id_profile' => new sfWidgetFormInputText(),
      'titulo_tv'  => new sfWidgetFormInputText(),
      'resumo_tv'  => new sfWidgetFormTextarea(),
      'data'       => new sfWidgetFormDate(),
      'video'      => new sfWidgetFormInputText(),
      'swf'        => new sfWidgetFormInputText(),
      'skin'       => new sfWidgetFormInputText(),
      'status'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id_tv'      => new sfValidatorPropelChoice(array('model' => 'Tv', 'column' => 'id_tv', 'required' => false)),
      'id_profile' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'titulo_tv'  => new sfValidatorString(array('max_length' => 100)),
      'resumo_tv'  => new sfValidatorString(array('required' => false)),
      'data'       => new sfValidatorDate(),
      'video'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'swf'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'skin'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'status'     => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tv[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Tv';
  }


}
