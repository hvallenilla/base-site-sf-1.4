<?php

/**
 * Contact form base class.
 *
 * @method Contact getObject() Returns the current form's model object
 *
 * @package    joaopaulo
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseContactForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id_contact'   => new sfWidgetFormInputHidden(),
      'first_name'   => new sfWidgetFormInputText(),
      'last_name'    => new sfWidgetFormInputText(),
      'organization' => new sfWidgetFormInputText(),
      'email'        => new sfWidgetFormInputText(),
      'comments'     => new sfWidgetFormTextarea(),
      'contact_type' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id_contact'   => new sfValidatorPropelChoice(array('model' => 'Contact', 'column' => 'id_contact', 'required' => false)),
      'first_name'   => new sfValidatorString(array('max_length' => 100)),
      'last_name'    => new sfValidatorString(array('max_length' => 100)),
      'organization' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'email'        => new sfValidatorString(array('max_length' => 100)),
      'comments'     => new sfValidatorString(),
      'contact_type' => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('contact[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Contact';
  }


}
