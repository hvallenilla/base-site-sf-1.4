<?php

/**
 * MultiContent form base class.
 *
 * @method MultiContent getObject() Returns the current form's model object
 *
 * @package    joaopaulo
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseMultiContentForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'id_category'  => new sfWidgetFormInputText(),
      'titulo'       => new sfWidgetFormInputText(),
      'resumo'       => new sfWidgetFormTextarea(),
      'arquivo'      => new sfWidgetFormInputText(),
      'imagen'       => new sfWidgetFormInputText(),
      'data_created' => new sfWidgetFormDate(),
      'permalink'    => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorPropelChoice(array('model' => 'MultiContent', 'column' => 'id', 'required' => false)),
      'id_category'  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'titulo'       => new sfValidatorString(array('max_length' => 150)),
      'resumo'       => new sfValidatorString(),
      'arquivo'      => new sfValidatorString(array('max_length' => 100)),
      'imagen'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'data_created' => new sfValidatorDate(array('required' => false)),
      'permalink'    => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('multi_content[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'MultiContent';
  }


}
