<?php

/**
 * video actions.
 *
 * @package    fito
 * @subpackage video
 * @author     Your name here
 */
class videoActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
        $this->getResponse()->setTitle($this->getContext()->getI18N()->__('List').' video - Lynx Cms');
	if (!$this->getRequestParameter('buscador')){
                    $this->buscador = '';
            }else{
                    $this->buscador = $this->getRequestParameter('buscador');
            }
            if(!$this->getRequestParameter('by'))
            {
                    $this->by = 'desc';               // Variable para el orden de los registros
                    $this->by_page = "asc";           // Variable para el paginador y las flechas de orden
                    $sortTemp =  TvPeer::getFieldNames(BasePeer::TYPE_FIELDNAME);
                    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
                    $this->sort = TvPeer::ID_TV;
            }
        //Criterios de busqueda
        $c = new Criteria();
        if($this->getRequestParameter('sort'))
        {
            $this->sort = $this->getRequestParameter('sort');
            switch ($this->getRequestParameter('by')) 
            {
                    case 'desc':
                            $c->addDescendingOrderByColumn(TvPeer::$this->getRequestParameter('sort'));
                            $this->by = "asc";
                            $this->by_page = "desc";
                            break;
                    default:
                            $c->addAscendingOrderByColumn(TvPeer::$this->getRequestParameter('sort'));
                            $this->by = "desc";
                            $this->by_page = "asc";
                            break;
            }
        }else{
            $c->addDescendingOrderByColumn($this->sort);
        }
        if($this->getRequestParameter('buscador'))
        {
        //Desactiva temporalmente el metodo de escape para que funcionen los link de la paginacion
        sfConfig::set('sf_escaping_strategy', false);
            //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
            $criterio = $c->getNewCriterion(TvPeer::ID_TV, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE);
            $criterio->addOr($c->getNewCriterion(TvPeer::TITULO_TV, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
            $criterio->addOr($c->getNewCriterion(TvPeer::RESUMO_TV, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
            $criterio->addOr($c->getNewCriterion(TvPeer::CONTEUDO_TV, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
            $c->add($criterio);
            $buscador = "&buscador=".$this->buscador;
            $this->bus_pagi = "&buscador=".$this->buscador;
        }else{
            $buscador = "";
            $this->bus_pagi = "";
        }

        $pager = new sfPropelPager('Tv',100);
        $pager->setCriteria($c);
        $pager->setPage($this->getRequestParameter('page',1));
        $pager->init();
        $this->videos = $pager;          
        
        // Crea sesion de la uri al momento
        $this->getUser()->setAttribute('uri_video','sort='.$this->sort.'&by='.$this->by_page.$buscador.'&page='.$this->videos->getPage());
  
  }

  public function executeNew(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Add new').' video - Lynx Cms');
    $this->form = new TvForm();
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeCreate(sfWebRequest $request)
  {
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Edit').' video - Lynx Cms');
    if (!$request->isMethod('post'))
    {
        $this->redirect("video/new");
    }
    $this->form = new TvForm();
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Edit').' video - Lynx Cms');
    $this->forward404Unless($Videos = TvPeer::retrieveByPk($request->getParameter('id_video')), sprintf('Object Videos does not exist (%s).', $request->getParameter('id_video')));
    $this->form = new TvForm($Videos);
    $this->getUser()->setAttribute('idVideo', $request->getParameter('id_video'));
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->dataVideo = $Videos;
  }

  public function executeUpdate(sfWebRequest $request)
  {
 
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($Videos = TvPeer::retrieveByPk($request->getParameter('id_video')), sprintf('Object Videos does not exist (%s).', $request->getParameter('id_video')));
    $this->form = new TvForm($Videos);
    
    $this->processForm($request, $this->form);
    
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->dataVideo = $Videos;
    $this->setTemplate('edit');
  }
  
  public function executeShowVideo(sfWebRequest $request)
  {
      $this->forward404Unless($this->video = TvPeer::retrieveByPK($this->getUser()->getAttribute('idVideo')), sprintf('Video does not exist (%s).', $this->getUser()->getAttribute('idVideo')));
  }

  public function executeSaveVideo(sfWebRequest $request)
  {
      if ($request->getParameter('edit') == 'true')
      {
          $id = $this->getUser()->getAttribute('idVideo');
          $this->forward404Unless($item = TvPeer::retrieveByPK($id), sprintf('Video does not exist (%s).', $id));
          $form = new TvForm($item);
          $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
          
          if ($form->isValid())
          {
            $Video = $form->save();
            return true;
          }          
      }else{
          $form = new TvForm();
          $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
          if ($form->isValid())
          {
            $Video = $form->save();
            $this->getUser()->setAttribute('idVideo', $Video->getIdTv());
            return true;
          }
      }
      return false;
  }
  
  public function executeDeleteVideo(sfWebRequest $request)
  {
    $this->forward404Unless($Model = TvPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Model does not exist (%s).', $request->getParameter('id')));
    // Delete video process
    if ($Model->getVideo())
    {
        $uploadDir = sfConfig::get('sf_upload_dir').'/videos/';
        //Delete images from uploads directory
        if(is_file($uploadDir.$Model->getVideo()))
        {
          unlink($uploadDir.$Model->getVideo());
        }      
    }
    $Model->setVideo('');
    $Model->save();
    return sfView::NONE;
  }

  public function executeUploadVideo(sfWebRequest $request)
  {
      if (!empty($_FILES))
      {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = sfConfig::get('sf_upload_dir');
	$targetPath = $_SERVER['DOCUMENT_ROOT'].'/uploads/videos';
	$lxValida = new lynxValida();
        $fileName = $lxValida->preparaArchivo($_FILES['Filedata']['name'],20);
        $fileName = $this->getUser()->getAttribute('idVideo')."_".$fileName; 
        
        $targetFile = rtrim($targetPath,'/') . '/' . $fileName;
        
	$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
        $fileParts  = pathinfo($_FILES['Filedata']['name']);
        move_uploaded_file($tempFile,$targetFile);
        $video = TvPeer::retrieveByPK($this->getUser()->getAttribute('idVideo'));
        $video->setVideo($fileName);
        $video->save();
        echo '1';        
      }
      return sfView::NONE;
  }
  
  public function executeCheckExists(sfWebRequest $request)
  {
      $targetFolder = sfConfig::get('sf_upload_dir').'/imgfck'; // Relative to the root and should match the upload folder in the uploader script
      //$targetFolder = '/uploads/tv'; // Relative to the root and should match the upload folder in the uploader script
      if (file_exists($targetFolder . '/' . $_POST['filename'])) {
        echo 1;
      }else{
        //echo $targetFolder . '/' . $_POST['filename']; 
        echo $_SERVER['DOCUMENT_ROOT'].'/uploads/imgfck'; 
      }
      
      return sfView::NONE;
  }
  
  

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($Videos = TvPeer::retrieveByPk($request->getParameter('id_video')), sprintf('Object Videos does not exist (%s).', $request->getParameter('id_video')));
    $Videos->delete();

    $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));
    return $this->redirect('video/index');
  }



public function executeDeleteAll(sfWebRequest $request)
{
    if ($this->getRequestParameter('chk'))
    {
            foreach ($this->getRequestParameter('chk') as $gr => $val)
            {
                    
                    $this->forward404Unless($Videos = TvPeer::retrieveByPk($val), sprintf('Object Videos does not exist (%s).', $request->getParameter('id_video')));
                    $Videos->delete();
            }
            $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));

    }
    return $this->redirect('video/index');
}

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
      
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $Videos = $form->save();
      //Image process
      if($form->getValue('skin'))
      {
        $file = $form->getValue('skin');
        $Model = TvPeer::retrieveByPK($Videos->getIdTv());
        // Aqui cargo la imagen con la funcion loadFiles de mi Helper
        sfProjectConfiguration::getActive()->loadHelpers('uploadFile');
        $fileUploaded = loadFiles($file->getOriginalName(), $file->getTempname(), 0, sfConfig::get('sf_upload_dir').'/videos/', $Model->getIdTv(), false);
        $Model->setSkin($fileUploaded);
        $Model->save();
      }

      $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_save_confir')));
      if($request->getParameter('id_video')){
        //return $this->redirect('@default?module=video&action=index&'.$this->getUser()->getAttribute('uri_video'));
        return $this->redirect('video/index');
      }else{
        //return $this->redirect('video/index');
        return $this->redirect('video/edit?id_video='.$Videos->getIdTv());
      }
    }
  }
  
  public function executeDeleteImage(sfWebRequest $request)
  {
    $this->forward404Unless($Model = TvPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Model does not exist (%s).', $request->getParameter('id')));
    //Delete images process
    if ($Model->getSkin())
    {
      $appYml = sfConfig::get('app_upload_images_video');
      $uploadDir = sfConfig::get('sf_upload_dir').'/videos/';
      for($i=1;$i<=$appYml['copies'];$i++)
      {
        //Delete images from uploads directory
        if(is_file($uploadDir.$appYml['size_'.$i]['pref_'.$i].'_'.$Model->getSkin()))
        {
          unlink($uploadDir.$appYml['size_'.$i]['pref_'.$i].'_'.$Model->getSkin());
        }
      }
    }
    $Model->setSkin('');
    $Model->save();
  }
  
  public function executeVisualizacionNucleo(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $this->forward404Unless($this->video = TvPeer::retrieveByPk($request->getParameter('id_video')), sprintf('Object Model does not exist (%s).', $request->getParameter('id_video')));
      
      $this->nucleos = LxProfilePeer::getProfileWithoutAdminAndRoot();
      
  }
  public function executeChangeStatus(sfWebRequest $request)
  {
    $this->forward404Unless($this->Video = TvPeer::retrieveByPK($request->getParameter('id_video')), sprintf('Object Video does not exist (%s).', $request->getParameter('id_video')));
      
    if($request->getParameter('status'))
    {
      $this->Video->setStatus(0);
    }else{
      $this->Video->setStatus(1);
    }
    $this->Video->save();
      
  }
  
  /**
   * Cambia el status del nucleo para la noticia
   *
   * @param sfWebRequest $request
   */
  public function executeChangeStatusAccess(sfWebRequest $request)
  {
      $this->nucleo = VideoAccessPeer::getSelectActiveNucleoInVideo($request->getParameter('id_nucleo'),$request->getParameter('id_video'));
      $this->forward404If(!$request->getParameter('id_nucleo') && !$request->getParameter('id_video'));
      if($request->getParameter('status'))
      {
        $this->editAccess = VideoAccessPeer::retrieveByPk($this->nucleo->getIdAccessVideo());
        $this->editAccess->delete();
        $this->status = 0;
        
      }else{
        $this->editAccess = new VideoAccess();
        $this->editAccess->setIdNucleo($request->getParameter('id_nucleo'));
        $this->editAccess->setIdVideo($request->getParameter('id_video'));
        $this->editAccess->save();
        $this->status = 1;
      }
  }
}
