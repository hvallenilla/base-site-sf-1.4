<div id="">
    <div class="frameForm" >
    <h1><?php echo __('Videos') ?></h1>
    </div>
<div class="msn_error" id="no_select_item" style="display: none;"><?php echo __("Nenhum item selecionado"); ?>.&nbsp;&nbsp;<a href="#" onclick="noSelectedItem();"><?php echo __('Ocultar'); ?></a> </div>
<?php if ($sf_user->hasFlash('listo')): ?>
    <div class="msn_ready"><?php echo $sf_user->getFlash('listo') ?></div>
<?php endif; ?>
<div class="frameForm">
    <?php echo form_tag('video/deleteAll',array('name' => 'frmChk', 'id' => 'frmChk','style'=>'margin:0px')) ?>
    <table border="0">
        <tr>
            <td>

                <a name="commit" href="#" onclick="return existItems(this);"><?php echo __('Eliminar todos') ?></a>
            </td>
            <td>&nbsp;|&nbsp;</td>
            <td>
                <a href="<?php echo url_for($this->getModuleName().'/new') ?>"><?php echo __('Agregar video')?></a>
            </td>
        </tr>
    </table>
</div>
<div style="padding-left: 30px;">

  <?php if ($videos->getNbResults()): ?>
    
    <?php fmod($i,2)?$class = "grayBackground":$class=''; ?>
            <?php foreach ($videos as $video): ?>
                <?php $idContent = $video->getIdTv(); ?>
                <div id="item_<?php echo $idContent;?>" class="contentPicture" style="float: left; width: 185px; margin-right: 10px; margin-bottom: 20px; cursor: auto;">
                    <?php if(file_exists(sfConfig::get('sf_upload_dir').'/videos/'.$idContent.'_imagem_1.jpg')): ?>
                        <?php echo image_tag('/uploads/videos/'.$idContent.'_imagem_1.jpg','class="big" width="175" height="98" ') ?>
                    <?php elseif(file_exists(sfConfig::get('sf_upload_dir').'/videos/big_'.$video->getSkin())): ?>
                        <?php echo image_tag('/uploads/videos/small_'.$video->getSkin(),'class="big" width="175" height="98" ') ?>
                    <?php else: ?>
                        <?php echo image_tag('preview_default_video_175_98.jpg') ?>
                    <?php endif; ?>     
                    <div class="row-actions" style="height: 30px; margin-left: 0px; width: 90px; position: relative; top: 15px; left: -12px; float: left;">
                        <a href="<?php echo url_for('video/edit?id_video='.$idContent) ?>" class="edit"><?php echo __('Editar') ?></a>&nbsp;|&nbsp;
                        <?php echo link_to(__('Eliminar'),'video/delete?id_video='.$idContent, array('method' => 'delete', 'class' => 'delete' , 'confirm' => __('Are you sure you want to delete the selected data?'))) ?>
                    </div>
                    <div class="" id="status_<?php echo $idContent?>" style="float: right; position: relative; top: 15px;">
                                <?php $img = $video->getStatus() ? '1' : '0'; ?>
                                <?php echo jq_link_to_remote(image_tag($img.'.png','alt="" title="" border=0'), array(
                                    'update'  =>  'status_'.$idContent,
                                    'url'     =>  'video/changeStatus?id_video='.$idContent.'&status='.$video->getStatus(),
                                    'script'  => true,
                                    'before'  => "$('#status_".$idContent."').html('". image_tag('preload.gif','title="" alt=""')."');"
                                ));
                                ?>
                    </div>
                </div>
            <?php endforeach; ?>
</div>
    <?php else: ?>
    <table width="100%" align="center"  border="0" cellspacing="10">
        <tr>
            <td align="center"><strong><?php echo __('Your search did not match any result') ?></strong></td>
        </tr>
    </table>
    <?php endif; ?>
  
</form>

</div>

