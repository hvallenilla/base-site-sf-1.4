<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jwplayer/jwplayer.js') ?>
<?php use_javascript('jq/jquery.validationEngine.js') ?>
<?php use_javascript('jq/jquery.validationEngine-en_US.js') ?>
<?php use_javascript('jq/jquery.uploadify.js') ?>
<?php use_stylesheet('jq/uploadify.css') ?>
<?php  $appYml = sfConfig::get('app_upload_images_video'); ?>
<script type="text/javascript"> 
    $(document).ready(function() {
          $("#video").validationEngine();
          <?php if($form->getObject()->isNew() || (!$form->getObject()->isNew() && !$dataVideo->getVideo()) ): ?>
                $("#form-upload-video").show();
          <?php else: ?>
                $("#form-upload-video").hide();
          <?php endif; ?>
    })
</script>
<?php //if ($form->getObject()->isNew()): ?>
    <?php /*echo jq_form_remote_tag(array(
        'url'       => 'video/saveVideo',
        'update'    => 'indicator',
        'position'  => 'after',
        'loading'   => "$('#indicator').show()",
        'complete'  => "$('#indicator').hide()",
        'success'   => "$('#video_upload').show(); $('#edit').val('true')",
      ), array(
        'id'     => 'video',)
      )*/ ?>
<?php //else: ?>
    <form id="video" action="<?php echo url_for('video/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id_video='.$form->getObject()->getIdTv() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>> 
<?php //endif; ?>

 <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
 <?php endif; ?>
    <div class="frameForm" align="left">
      <table width="100%">
          <tr>
            <td>
                &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são obrigatórios')?>
            </td>
          </tr>
          <tr>
            <td id="errorGlobal">
                <?php echo $form->renderGlobalErrors() ?>
            </td>
          </tr>
          <tr>
              <td>
                  <table cellspacing="4">
                    <tr>
                        <td>
                            <div class="button">
                                    <?php echo link_to(__('Voltar na lista'), '@default?module=video&action=index', array('class' => 'button')) ?>
                            </div>
                        </td>
                        <?php if (!$form->getObject()->isNew()): ?>
                        <td>
                            <div class="button">
                                    <?php echo link_to(__('Eliminar'), 'video/delete?id_video='.$form->getObject()->getIdTv(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                            </div>
                        </td>
                        <?php endif; ?>
                        <td>
                        <input type="submit" value="<?php echo __('Salvar') ?>" />
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
        <tfoot>
          <tr>
            <td>
                <?php echo $form->renderHiddenFields(false) ?>
                <table cellspacing="4">
                    <tr>
                        <td>
                            <div class="button">
                                <?php echo link_to(__('Voltar na lista'), '@default?module=video&action=index', array('class' => 'button')) ?>
                             </div>
                        </td>            
                        <?php if (!$form->getObject()->isNew()): ?>
                        <td>
                            <div class="button">
                                <?php echo link_to(__('Eliminar'), 'video/delete?id_video='.$form->getObject()->getIdTv(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                            </div>
                        </td>
                        <?php endif; ?>
                        <td>
                        <input type="submit" value="<?php echo __('Salvar') ?>" />
                        </td>
                    </tr>
                </table>
            </td>
          </tr>
        </tfoot>
        <tbody>
            <tr>
                <td>                
                    <table cellpadding="0" cellspacing="2" border="0" width="100%">
                      <tr>
                          <td><?php echo $form['id_profile']->renderLabel() ?><br />
                            <?php echo $form['id_profile'] ?>
                            <?php echo $form['id_profile']->renderError() ?>
                        </td>
                      </tr>
                      <tr>
                          <td><?php echo $form['titulo_tv']->renderLabel() ?><br />
                            <?php echo $form['titulo_tv'] ?>
                            <?php echo $form['titulo_tv']->renderError() ?>
                        </td>
                      </tr>
                      <tr>
                        <td valign="top"><?php echo $form['resumo_tv']->renderLabel() ?>&nbsp;<br />
                            <?php echo $form['resumo_tv'] ?>
                            <?php echo $form['resumo_tv']->renderError() ?>
                        </td>
                      </tr>
                      <tr>
                      <td>
                            <?php $module = 'videos'; ?>
                            <table cellpadding="0" cellspacing="0" border="0" width="80%" style="margin-top: 15px; margin-bottom: 15px;">
                                <tr>
                                    <td width="3%" align="left" >
                                        <div id="imageFIELD" style="min-height: 110px; min-width: 170px;">
                                            <?php if($form->getObject()->getSkin()):  ?>
                                                <?php echo image_tag('/uploads/'.$module.'/'.$appYml['size_1']['pref_1'].'_'.$form->getObject()->getSkin(), 'class="borderImage" ')?>
                                            <?php elseif(file_exists(sfConfig::get('sf_upload_dir').'/videos/'.$form->getObject()->getIdTv().'_imagem_1.jpg')): ?>
                                                <?php echo image_tag('/uploads/videos/'.$form->getObject()->getIdTv().'_imagem_1.jpg','class="big" width="175" height="98" ') ?>
                                            <?php else:?>
                                                <?php echo image_tag('no_image.jpg', 'border=0 width="110" height="82" class="borderImage"');?>
                                            <?php endif;?>
                                        </div>
                                    </td>
                                    <td width="67%" valign="bottom" style="padding-left:7px">
                                        <?php echo $form['skin']->renderLabel() ?><br />
                                        <?php echo $form['skin'] ?>
                                        <?php echo $form['skin']->renderError() ?>
                                        <span class="msn_help"><?php echo $form['skin']->renderHelp() ?></span>
                                    </td>
                                </tr>
                                <tr>
                                     <td>
                                         <?php if($form->getObject()->getSkin()):?>
                                            <div id="deleteImage" style="margin-left: 75px; margin-top: 10px;" >
                                                <?php echo jq_link_to_remote('Deletar Imagem', array(
                                                      'update'  =>  'imageFIELD',
                                                      'url'     =>  'video/deleteImage?id='.$form->getObject()->getIdTv(),
                                                      'script'  => true,
                                                      'confirm' => 'Tem certeza de que quer apagar os dados selecionados?',
                                                      'before'  => "$('#imageFIELD').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');",
                                                      'complete'=> "$('#deleteImage').html('');"
                                                  ));
                                                  ?>
                                            </div>
                                        <?php endif;?>
                                     </td>
                                     <td>&nbsp;</td>
                                </tr>
                            </table>
                      </td>
                    </tr>
                    </table>                
                </td>
            </tr>
        </tbody>
      </table>
    </div>
    <div id ="indicator" class="frameForm" align="left" style="display: none" >
        <p><?php echo image_tag('preload.gif').' Loading...'; ?></p>
    </div>
    <?php if ($form->getObject()->isNew()): ?>
        <div id="video_upload" class="frameForm" style="display:none;" align="left">
    <?php else: ?>
        <div id="video_upload" class="frameForm" align="left">
    <?php endif; ?>
    <?php if (!$form->getObject()->isNew()): ?>        
        <label>Carregar Vídeo</label>
        
        <div id="player-video">
            <?php if($dataVideo->getVideo()): ?>
                <?php include_partial('video', array('idVideo' => $dataVideo->getIdTv(), 'video' => $dataVideo->getVideo())) ?>
            <?php endif;?>
        </div>
        <div id="form-upload-video">        
	<form>
            <div id="queue"></div>            
            <input id="file_upload" name="file_upload" type="file" multiple="true"  />             
	</form>
        </div>
	<script type="text/javascript">
		<?php $timestamp = time();?>
		$(function() {
                    $("#file_upload").uploadify({
                        fileSizeLimit   : '10550MB',
                        fileTypeExts    : '*.flv; *.mp4; *.png; *.jpg',
                        removeCompleted : true,
                        height          : 30,
                        swf             : '/js/uploadify.swf',
                        uploader        : '<?php echo url_for("video/uploadVideo?uploadify=onUpload&" . session_name() . "=" . session_id(), true)?>',
                        buttonText      : 'Examinar',
                        width           : 120,
                        multi           : true,
                        onSelectError   : function() {
                            alert('The file ' + file.name + ' returned an error and was not added to the queue.');
                        },
                        onUploadError   : function(file, errorCode, errorMsg, errorString) {
                            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                        },
                        onQueueComplete : function(queueData) {
                            //alert(queueData.uploadsSuccessful + ' files were successfully uploaded.');
                            $("#player-video").load("<?php echo url_for('video/showVideo');?>");
                            //$("#file_upload-queue").hide();
                            $("#deletar-video").show();                            
                            $("#form-upload-video").hide();                            
                        },
                        onAllComplete: function() {
                            //$("#images").load("<?php //echo url_for('hotel/hotelImageList');?>");
                            //$("*").dialog("destroy");
                            //$("#image_form").remove();
                        }
                    });
                });
	</script> 
    <?php endif; ?>
    </div>
</form>