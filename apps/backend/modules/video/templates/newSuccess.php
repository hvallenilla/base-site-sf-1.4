<div id="title_module">
    <div class="frameForm" >
        <h1><?php echo __($moduleParent['parent_name'])?> - <a href="<?php echo url_for('video/index') ?>"><?php echo __('video')  ?></a> - <?php echo __('Add new video') ?> </h1>
    </div>
<?php include_partial('form', array('form' => $form)) ?>
</div>

