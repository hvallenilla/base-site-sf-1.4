<?php use_javascript('jwplayer/jwplayer.js') ?>
<div id="mediaplayer">Carregando vídeo</div>
<br />
<div id="deletar-video">
    <?php echo jq_link_to_remote('Deletar Video', array(
        'update'  =>  'player-video',
        'url'     =>  'video/deleteVideo?id='.$idVideo,
        'script'  => true,
        'confirm' => __('Tem certeza de que deseja eliminar os dados selecionados?'),
        'before'  => "$('#player-video').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');",
        'complete'=> "$('#deletar-video').hide(); $('#form-upload-video').show();"
    ));
    ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        jwplayer("mediaplayer").setup({
            skin: "/js/jwplayer/skin/glow.zip",
            flashplayer: "/js/jwplayer/player.swf",
            file: "/uploads/videos/<?php echo $video ?>",
            image: "/images/preview_.jpg",
            poster: "/images/preview_.jpg",
            height: 280,
            width: 440,
            autoplay: true
        });
    });
</script>

<style type="text/css">
    .fancybox-custom .fancybox-skin {
            box-shadow: 0 0 50px #222;
    }
</style>