<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jq/jquery.validationEngine.js') ?>
<?php use_javascript('jq/jquery.validationEngine-en_US.js') ?>
<script src="/js/jq/jquery-ui-1.8.16.custom/development-bundle/ui/i18n/jquery.ui.datepicker-br.js"></script>
<script type="text/javascript"> 
  $(document).ready(function() {
    $("#eventos").validationEngine();
    $("#eventos_fecha_inicio").datepicker({
            dateFormat: 'yy-mm-dd',  
            yearRange: '-1:+1',
            changeMonth: true,
            changeYear: true
          });
    $("#eventos_fecha_fin").datepicker({
            dateFormat: 'yy-mm-dd',  
            yearRange: '-1:+1',
            changeMonth: true,
            changeYear: true
          });
  })
</script>

<form id="eventos" action="<?php echo url_for('eventos/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id_evento='.$form->getObject()->getIdEvento() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

 <?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<div class="frameForm" align="left">
  <table width="100%">
      <tr>
        <td>
            &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são obrigatórios')?>
        </td>
      </tr>
      <tr>
        <td id="errorGlobal">
            <?php echo $form->renderGlobalErrors() ?>
        </td>
      </tr>
      <tr>
          <td>
              <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Voltar na lista'), '@default?module=eventos&action=index&'.$sf_user->getAttribute('uri_eventos'), array('class' => 'button')) ?>
                        </div>
                    </td>
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Eliminar'), 'eventos/delete?id_evento='.$form->getObject()->getIdEvento(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                    <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
          </td>
      </tr>
    <tfoot>
      <tr>
        <td>
            <?php echo $form->renderHiddenFields(false) ?>
            <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Voltar na lista'), '@default?module=eventos&action=index&'.$sf_user->getAttribute('uri_eventos'), array('class' => 'button')) ?>
                         </div>
                    </td>            
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Eliminar'), 'eventos/delete?id_evento='.$form->getObject()->getIdEvento(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                    <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
        </td>
      </tr>
    </tfoot>
    <tbody>
        <tr>
            <td>                
                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                  <?php if($sf_user->getAttribute('idProfile') == 1 || $sf_user->getAttribute('idProfile') == 2):?>
                  <tr>
                      <td><?php echo $form['id_profile']->renderLabel() ?><br />
                        <?php echo $form['id_profile']->render(array('id' => 'id_profile', 'onchange' =>
                                jq_remote_function(array(
                                  'update' => 'id_parent',
                                  'before' => '$("#id_parent > option").remove();$("#id_parent").append("<option>Carregando seções...</option>"); ',
                                  'url'    => 'lxsection/getSectionByIdProfile',
                                  'with'   => " 'id=' + this.value ",
                                ))))
                        ?>  
                        <?php echo $form['id_profile']->renderError() ?>
                    </td>
                  </tr>
                  <?php endif; ?>
                  <tr>
                      <td><?php echo $form['tipo_evento']->renderLabel() ?><br />
                        <?php echo $form['tipo_evento'] ?>
                        <?php echo $form['tipo_evento']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <?php $appYml = sfConfig::get('app_upload_images_eventos'); ?>
                      <?php $module = 'eventos'; ?>
                      <table cellpadding="0" cellspacing="0" border="0" width="80%" style="margin-top: 15px; margin-bottom: 15px;">
                        <tr>
                          <td width="3%" align="left" >
                            <div id="imageFIELD" style="min-height: 110px; min-width: 170px;">
                              <?php if($form->getObject()->getImagen()):  ?>
                              <?php echo image_tag('/uploads/'.$module.'/'.$appYml['size_1']['pref_1'].'_'.$form->getObject()->getImagen(), 'class="borderImage" width="150" height="110"')?>
                              <?php else:?>
                              <?php echo image_tag('no_image.jpg', 'border=0 width="150" height="110" class="borderImage"');?>
                              <?php endif;?>
                            </div>
                          </td>
                          <td width="67%" valign="bottom" style="padding-left:7px">
                            <?php echo $form['imagen']->renderLabel() ?><br />
                            <?php echo $form['imagen'] ?>
                            <?php echo $form['imagen']->renderError() ?>
                            <span class="msn_help"><?php echo $form['imagen']->renderHelp() ?></span>
                          </td>
                        </tr>
                        <tr>
                          <td>
                           <?php if($form->getObject()->getImagen()):?>
                             <div id="deleteImage" style="margin-left: 40px;" >
                               <?php echo jq_link_to_remote('Deletar Imagem', array(
                                 'update'  =>  'imageFIELD',
                                 'url'     =>  $module.'/deleteImage?id='.$form->getObject()->getIdEvento(),
                                 'script'  => true,
                                 'confirm' => __('Are you sure you want to delete the selected data?'),
                                 'before'  => "$('#imageFIELD').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');",
                                 'complete'=> "$('#deleteImage').html('');"
                                 ));
                               ?>
                             </div>
                           <?php endif;?>
                          </td>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['titulo']->renderLabel() ?><br />
                        <?php echo $form['titulo'] ?>
                        <?php echo $form['titulo']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['resumen']->renderLabel() ?><br />
                        <?php echo $form['resumen'] ?>
                        <?php echo $form['resumen']->renderError() ?>
                    </td>
                  </tr>
                              <tr>
                      <td><?php echo $form['descripcion']->renderLabel() ?><br />
                        <?php echo $form['descripcion'] ?>
                        <?php echo $form['descripcion']->renderError() ?>
                    </td>
                  </tr>
                              <tr>
                      <td><?php echo $form['fecha_inicio']->renderLabel() ?><br />
                        <?php echo $form['fecha_inicio'] ?>
                        <?php echo $form['fecha_inicio']->renderError() ?>
                    </td>
                  </tr>
                              <tr>
                      <td><?php echo $form['fecha_fin']->renderLabel() ?><br />
                        <?php echo $form['fecha_fin'] ?>
                        <?php echo $form['fecha_fin']->renderError() ?>
                    </td>
                  </tr>
                  
                  <tr>
                    <td><?php echo $form['status']->renderLabel() ?><br />
                        <?php echo $form['status'] ?>
                        <?php echo $form['status']->renderError() ?>
                    </td>
                  </tr>
                  
                </table>                
            </td>
        </tr>
    </tbody>
  </table>
    </div>
</form>
