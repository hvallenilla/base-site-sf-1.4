<?php

/**
 * eventos actions.
 *
 * @package    abge
 * @subpackage eventos
 * @author     Your name here
 */
class eventosActions extends sfActions
{
  public function executeActualizaPermalinks()
  {
      $news = EventosPeer::getAllEventos();
      $val = new globalFunctions();
      foreach ($news as $new) 
      {
          echo $new->getIdEvento().' -- '. $val->safename_url($new->getTitulo())."<br />";
          $update = EventosPeer::retrieveByPK($new->getIdEvento());
          $update->setPermalink($val->safename_url($new->getTitulo()));
          $update->save();
      }
      return sfView::NONE;
  }  
    
  public function executeIndex(sfWebRequest $request)
  {
      
  $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Lista').' de eventos - Lynx Cms');
	if (!$this->getRequestParameter('buscador')){
			$this->buscador = '';
		}else{
			$this->buscador = $this->getRequestParameter('buscador');
		}
		if(!$this->getRequestParameter('by'))
		{
			$this->by = 'desc';               // Variable para el orden de los registros
			$this->by_page = "asc";           // Variable para el paginador y las flechas de orden
			$sortTemp =  EventosPeer::getFieldNames(BasePeer::TYPE_FIELDNAME);
      //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
      $this->sort = $sortTemp[0];      // Nombre del campo que por defecto se ordenara
		}
		//Criterios de busqueda
		$c = new Criteria();
		if($this->getRequestParameter('sort'))
		{
			$this->sort = $this->getRequestParameter('sort');
			switch ($this->getRequestParameter('by')) {
				case 'desc':
				  $c->addDescendingOrderByColumn(EventosPeer::$this->getRequestParameter('sort'));
					$this->by = "asc";
					$this->by_page = "desc";
					break;
				default:
					$c->addAscendingOrderByColumn(EventosPeer::$this->getRequestParameter('sort'));
					$this->by = "desc";
					$this->by_page = "asc";
					break;
			}
		}else{
			$c->addAscendingOrderByColumn($this->sort);
		}
		if($this->getRequestParameter('buscador'))
    {
      //Desactiva temporalmente el metodo de escape para que funcionen los link de la paginacion
      sfConfig::set('sf_escaping_strategy', false);
      //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
      $criterio = $c->getNewCriterion(EventosPeer::ID_EVENTO, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE);
      $criterio->addOr($c->getNewCriterion(EventosPeer::TITULO, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $criterio->addOr($c->getNewCriterion(EventosPeer::RESUMEN, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $criterio->addOr($c->getNewCriterion(EventosPeer::DESCRIPCION, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $criterio->addOr($c->getNewCriterion(EventosPeer::FECHA_INICIO, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $criterio->addOr($c->getNewCriterion(EventosPeer::FECHA_FIN, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $criterio->addOr($c->getNewCriterion(EventosPeer::IMAGEN, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $criterio->addOr($c->getNewCriterion(EventosPeer::STATUS, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $criterio->addOr($c->getNewCriterion(EventosPeer::PERMALINK, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
      $c->add($criterio);
			$buscador = "&buscador=".$this->buscador;
			$this->bus_pagi = "&buscador=".$this->buscador;
    }else{
			$buscador = "";
			$this->bus_pagi = "";
		}
			
		$pager = new sfPropelPager('eventos',10);
		$pager->setCriteria($c);
		$pager->setPage($this->getRequestParameter('page',1));
		$pager->init();
		$this->eventoss = $pager;                
    // Crea sesion de la uri al momento
    $this->getUser()->setAttribute('uri_eventos','sort='.$this->sort.'&by='.$this->by_page.$buscador.'&page='.$this->eventoss->getPage());
  }

  public function executeNew(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
  	$this->getResponse()->setTitle($this->getContext()->getI18N()->__('Agregar').' evento - Lynx Cms');
    $this->form = new eventosForm();
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeCreate(sfWebRequest $request)
  {
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Agregar').' evento - Lynx Cms');
    if (!$request->isMethod('post'))
    {
      $this->redirect("eventos/new");
    }

    $this->form = new eventosForm();
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
  	$this->getResponse()->setTitle($this->getContext()->getI18N()->__('Editar').' evento - Lynx Cms');
    $this->forward404Unless($eventos = EventosPeer::retrieveByPk($request->getParameter('id_evento')), sprintf('Object eventos does not exist (%s).', $request->getParameter('id_evento')));
    $this->form = new eventosForm($eventos);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Editar').' evento - Lynx Cms');
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($eventos = EventosPeer::retrieveByPk($request->getParameter('id_evento')), sprintf('Object eventos does not exist (%s).', $request->getParameter('id_evento')));
    $this->form = new eventosForm($eventos);

    $this->processForm($request, $this->form);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $this->forward404Unless($eventos = EventosPeer::retrieveByPk($request->getParameter('id_evento')), sprintf('Object eventos does not exist (%s).', $request->getParameter('id_evento')));
    
    //Borrado de imagen
    $request->setParameter('id', $request->getParameter('id_evento'));
    $this->executeDeleteImage($request);

    //Borrado del registro
    $eventos->delete();

    $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));
    return $this->redirect('eventos/index');
  }

  public function executeDeleteAll(sfWebRequest $request)
  {
    if ($this->getRequestParameter('chk'))
    {
      foreach ($this->getRequestParameter('chk') as $gr => $val)
      {
        $this->forward404Unless($eventos = EventosPeer::retrieveByPk($val), sprintf('Object eventos does not exist (%s).', $val));
        
        //Borrado de imagen
        $request->setParameter('id', $val);
        $this->executeDeleteImage($request);
    
        //Borrado del registro
        $eventos->delete();
      }
      $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));
    }
    return $this->redirect('eventos/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      //En caso de edicion de la imagen se elimina la anterior
      if($form->getValue('id_evento') && $form->getValue('imagen'))
      {
        //Se elimina la imagen previa en caso de edicion
        $request->setParameter('id', $form->getValue('id_evento'));
        $this->executeDeleteImage($request);
      }
      $eventos = $form->save();
      
      //Image process
      if($form->getValue('imagen'))
      {
        //Procesamiento de la imagen
        $file = $form->getValue('imagen');
        $model = $eventos;

        // Aqui cargo la imagen con la funcion loadFiles de mi Helper
        sfProjectConfiguration::getActive()->loadHelpers('uploadFile');
        $fileUploaded = loadFiles($file->getOriginalName(), $file->getTempname(), 0, sfConfig::get('sf_upload_dir').'/eventos/', $model->getIdEvento(), false);
        $model->setImagen($fileUploaded);
        $model->save();
      }
      // Agrega por defecti la institución ppal al registro
      if(!EventosAccessPeer::getActiveNucleoInEventos($eventos->getIdProfile(), $eventos->getIdEvento()))
      {
            $newAccess = new EventosAccess();
            $newAccess->setIdEvento($eventos->getIdEvento());
            $newAccess->setIdProfile($eventos->getIdProfile());
            $newAccess->save();
      }

      $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_save_confir')));
      if($request->getParameter('id_evento')){
        return $this->redirect('@default?module=eventos&action=index&'.$this->getUser()->getAttribute('uri_eventos'));
      }else{
        return $this->redirect('eventos/index');
      }
    }
  }
  
  public function executeDeleteImage(sfWebRequest $request)
  {
    $this->forward404Unless($Model = EventosPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Model does not exist (%s).', $request->getParameter('id')));
    
    //Delete images process
    if ($Model->getImagen())
    {
      $appYml = sfConfig::get('app_upload_images_news');
      $uploadDir = sfConfig::get('sf_upload_dir').'/eventos/';
      for($i=1;$i<=$appYml['copies'];$i++)
      {
        //Delete images from uploads directory
        if(is_file($uploadDir.$appYml['size_'.$i]['pref_'.$i].'_'.$Model->getImagen()))
        {
          
          unlink($uploadDir.$appYml['size_'.$i]['pref_'.$i].'_'.$Model->getImagen());
        }
      }
    }
    $Model->setImagen('');
    $Model->save();
  }
  
  public function executeVisualizacionNucleo(sfWebRequest $request)
  {
      $this->setLayout('layoutSimple');
      $this->forward404Unless($this->evento = EventosPeer::retrieveByPk($request->getParameter('id_evento')), sprintf('Object Model does not exist (%s).', $request->getParameter('id_evento')));
      
      $this->nucleos = LxProfilePeer::getProfileWithoutAdminAndRoot();      
  }
  
  /**
   * Cambia el status del nucleo para el evento
   *
   * @param sfWebRequest $request
   */
  public function executeChangeStatusAccess(sfWebRequest $request)
  {
      $this->nucleo = EventosAccessPeer::getSelectActiveNucleoInEventos($request->getParameter('id_nucleo'),$request->getParameter('id_evento'));
      $this->forward404If(!$request->getParameter('id_nucleo') && !$request->getParameter('id_evento'));
      if($request->getParameter('status'))
      {
        $this->editAccess = EventosAccessPeer::retrieveByPk($this->nucleo->getIdEventoAccess());
        $this->editAccess->delete();
        $this->status = 0;
        
      }else{
        $this->editAccess = new EventosAccess();
        $this->editAccess->setIdProfile($request->getParameter('id_nucleo'));
        $this->editAccess->setIdEvento($request->getParameter('id_evento'));
        $this->editAccess->save();
        $this->status = 1;
      }
  }
  
  public function executeChangeStatus(sfWebRequest $request)
  {
      $this->forward404Unless($this->Evento = EventosPeer::retrieveByPK($request->getParameter('id_evento')), sprintf('Object Eventos does not exist (%s).', $request->getParameter('id_evento')));
      if($request->getParameter('status'))
      {
        $this->Evento->setStatus(0);
      }else{
        $this->Evento->setStatus(1);
      }
      $this->Evento->save();
      
  }
  
}
