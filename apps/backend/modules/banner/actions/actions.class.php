<?php

/**
 * banner actions.
 *
 * @package    joaopaulo
 * @subpackage banner
 * @author     Henry Vallenilla <henryvallenilla@gmail.com>
 */
class bannerActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
        $this->getResponse()->setTitle($this->getContext()->getI18N()->__('List').' banner - Lynx Cms');
	if (!$this->getRequestParameter('buscador')){
                $this->buscador = '';
        }else{
                $this->buscador = $this->getRequestParameter('buscador');
        }
        if(!$this->getRequestParameter('by'))
        {
            $this->by = 'desc';               // Variable para el orden de los registros
            $this->by_page = "asc";           // Variable para el paginador y las flechas de orden
            $sortTemp =  BannerPeer::getFieldNames(BasePeer::TYPE_FIELDNAME);
            //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
            $this->sort = $sortTemp[0];      // Nombre del campo que por defecto se ordenara
        }
        //Criterios de busqueda
        $c = new Criteria();
        if($this->getRequestParameter('sort'))
        {
            $this->sort = $this->getRequestParameter('sort');
            switch ($this->getRequestParameter('by')) {
                case 'desc':
                    $c->addDescendingOrderByColumn(BannerPeer::$this->getRequestParameter('sort'));
                    $this->by = "asc";
                    $this->by_page = "desc";
                    break;
                default:
                    $c->addAscendingOrderByColumn(BannerPeer::$this->getRequestParameter('sort'));
                    $this->by = "desc";
                    $this->by_page = "asc";
                    break;
            }
        }else{
            $c->addAscendingOrderByColumn($this->sort);
        }
        if($this->getRequestParameter('buscador'))
        {
            //Desactiva temporalmente el metodo de escape para que funcionen los link de la paginacion
            sfConfig::set('sf_escaping_strategy', false);
            $criterio = $c->getNewCriterion(BannerPeer::ID_BANNER, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE);
            $criterio->addOr($c->getNewCriterion(BannerPeer::ARQUIVO, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
            $criterio->addOr($c->getNewCriterion(BannerPeer::STATUS, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
            $c->add($criterio);
            $buscador = "&buscador=".$this->buscador;
            $this->bus_pagi = "&buscador=".$this->buscador;
        }else{
            $buscador = "";
            $this->bus_pagi = "";
        }
        $pager = new sfPropelPager('Banner',10);
        $pager->setCriteria($c);
        $pager->setPage($this->getRequestParameter('page',1));
        $pager->init();
        $this->BannerI18ns = $pager;                
        // Crea sesion de la uri al momento
        $this->getUser()->setAttribute('uri_banner','sort='.$this->sort.'&by='.$this->by_page.$buscador.'&page='.$this->BannerI18ns->getPage());
  }

  public function executeNew(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Add new').' banner - Lynx Cms');
    $this->form = new BannerForm();
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeCreate(sfWebRequest $request)
  {
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Edit').' banner - Lynx Cms');
    if (!$request->isMethod('post'))
    {
        $this->redirect("banner/new");
    }
    

    $this->form = new BannerForm();
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Edit').' banner - Lynx Cms');
    $this->forward404Unless($Banner = BannerPeer::retrieveByPk($request->getParameter('id_banner')), sprintf('Object BannerI18n does not exist (%s).', $request->getParameter('id_banner')));
    $this->form = new BannerForm($Banner);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($Banner = BannerPeer::retrieveByPk($request->getParameter('id_banner')), sprintf('Object BannerI18n does not exist (%s).', $request->getParameter('id_banner')));
    $this->form = new BannerForm($Banner);

    $this->processForm($request, $this->form);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($Banner = BannerPeer::retrieveByPk($request->getParameter('id_banner')), sprintf('Object BannerI18n does not exist (%s).', $request->getParameter('id_banner')));
    //Delete images process
    if ($Banner->getArquivo())
    {
      $uploadDir = sfConfig::get('sf_upload_dir').'/banner/';
      //Delete images from uploads directory
      if(is_file($uploadDir.'/banner_'.$Banner->getArquivo()))
      {
        unlink($uploadDir.'banner_'.$Banner->getArquivo());
      }
      
    }
    $Banner->delete();
    $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));
    return $this->redirect('banner/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
        $Banner = $form->save();
      
        if($form->getValue('arquivo'))
        {
          $file = $this->form->getValue('arquivo');
          $diretorio =  sfConfig::get('sf_upload_dir').'/banner/';
          sfProjectConfiguration::getActive()->loadHelpers('upload');
          
          
          $fileUploaded = loadFile($file->getOriginalName(), $file->getTempname(), 0, $diretorio , $Banner->getIdBanner(), true);          
          $file = $diretorio.$fileUploaded;
          $image = imagecreatefromjpeg($file);
          $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
          imagedestroy($image);
          imagejpeg($bg, $file, 50);
          exit();
          
          $img = new sfImage($file, 'image/jpg');
          $img->thumbnail(580,250, 'center', '#E6E6E6');
          $img->setQuality(100);
          $img->saveAs($diretorio.'banner_'.$fileUploaded);
          // Elimina imagen original
          unlink($diretorio.$fileUploaded);
          $Banner->setArquivo($fileUploaded);
          $Banner->save();                
        }
      
        if(!$request->getParameter('id_banner')){
            $Banner->setStatus('1');
            $Banner->save();
        }

      $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_save_confir')));
      return $this->redirect('banner/index');
    }
  }
  
  public function executeDeleteImage(sfWebRequest $request)
  {
    $this->forward404Unless($Model = BannerPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Model does not exist (%s).', $request->getParameter('id')));
    //Delete images process
    if ($Model->getArquivo())
    {
        $uploadDir = sfConfig::get('sf_upload_dir').'/banner/';
        //Delete images from uploads directory by language
        if(is_file($uploadDir.'/banner_'.$Model->getArquivo()))
        {
          unlink($uploadDir.'/banner_'.$Model->getArquivo());
        }      
    }
    $Model->save();
  }
  
  public function executeChangeStatus(sfWebRequest $request)
  {
      $this->forward404Unless($this->Banner = BannerPeer::retrieveByPk($request->getParameter('id_banner')), sprintf('Object Banner does not exist (%s).', $request->getParameter('id_banner')));
      if($request->getParameter('status'))
      {
        $this->Banner->setStatus(0);
      }else{
        $this->Banner->setStatus(1);
      }
      $this->Banner->save();
  }
}
