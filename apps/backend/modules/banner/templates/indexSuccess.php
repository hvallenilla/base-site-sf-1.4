<div id="title_module">
    <div class="frameForm" >
        <h1>Banners</h1>
    </div>
<div class="msn_error" id="no_select_item" style="display: none;"><?php echo __("Nenhum item selecionado"); ?>.&nbsp;&nbsp;<a href="#" onclick="noSelectedItem();"><?php echo __('Ocultar'); ?></a> </div>
<?php if ($sf_user->hasFlash('listo')): ?>
    <div class="msn_ready"><?php echo $sf_user->getFlash('listo') ?></div>
<?php endif; ?>
<div class="frameForm">
    <table border="0">
        <tr>
            <td>
                <a href="<?php echo url_for($this->getModuleName().'/new') ?>"><?php echo __('Novo Banner')?></a>
            </td>
        </tr>
    </table>
</div>
<div style="padding-left: 30px;">
    <?php if ($BannerI18ns->getNbResults()): ?>
        <?php foreach ($BannerI18ns as $BannerI18n): ?>
            <?php $id_banner = $BannerI18n->getIdBanner(); ?>
            <div id="item_<?php echo $id_banner;?>" class="content-img-banner" >
                <a href="<?php echo url_for('banner/edit?id_banner='.$BannerI18n->getIdBanner()) ?>" class="titulo">
                    <?php if($BannerI18n->getArquivo()): ?>
                        <?php $file_banner = sfConfig::get('sf_upload_dir').'/banner/banner_'.$BannerI18n->getArquivo();  ?>
                        <?php if (file_exists($file_banner)):  ?>
                            <?php echo image_tag('/uploads/banner/banner_'.$BannerI18n->getArquivo(), 'class="" width="250"')?>
                        <?php else:?>
                            <?php echo image_tag('no_banner', 'border=0 ');?>
                        <?php endif;?>                            
                    <?php else:?>
                        <?php echo image_tag('no_image.jpg', 'border=0 width="130" height="130" class="borderImage"');?>
                    <?php endif;?>
                    <div class="row-actions" style="height: 30px; margin-left: 0px; width: 90px; position: relative; top: 15px; left: -12px; float: left;">
                        <a href="<?php echo url_for('banner/edit?id_banner='.$id_banner) ?>" class="edit"><?php echo __('Editar') ?></a>&nbsp;|&nbsp;
                        <?php echo link_to(__('Deleitar'),'banner/delete?id_banner='.$id_banner, array('method' => 'delete', 'class' => 'delete' , 'confirm' => __('Você tem certeza que deseja excluir este Banner?'))) ?>
                    </div>
                    <div class="" id="status_<?php echo $id_banner?>" style="float: right; position: relative; top: 15px;">
                        <?php $img = $BannerI18n->getStatus() ? '1' : '0'; ?>
                        <?php echo jq_link_to_remote(image_tag($img.'.png','alt="" title="" border=0'), array(
                            'update'  =>  'status_'.$id_banner,
                            'url'     =>  'banner/changeStatus?id_banner='.$id_banner.'&status='.$BannerI18n->getStatus(),
                            'script'  => true,
                            'before'  => "$('#status_".$id_banner."').html('". image_tag('preload.gif','title="" alt=""')."');"
                        ));
                        ?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>    
    

</div>

