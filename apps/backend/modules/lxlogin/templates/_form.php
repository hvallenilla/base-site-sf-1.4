<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php if( $form->hasErrors() || $form->hasGlobalErrors() ) : ?>
    <?php $errors = $form->getErrorSchema()->getErrors() ?>
    <?php if ( count($errors) > 0 ) : ?>
        <?php foreach( $errors as $name => $error ) :
            //si no exite el token redirecciona la pagina para el home
            if($name=='_csrf_token') :?>
                <script type="text/javascript">
                    document.location = '/';
                </script>
            <?php exit();?>
            <?php endif ?>
        <?php endforeach ?>
    <?php endif ?>
<?php endif ?>

<script type="text/javascript"> 
    $(document).ready(function() {
         $("#lxlogin").validationEngine()
    })
</script>
<form id="lxlogin" action="<?php echo url_for('@default_index?module=lxlogin') ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <div style="padding-top: 100px;" align="center"><?php echo image_tag('logo') ?>
        <table border="0" cellpadding="0" cellspacing="5">
            <tfoot>
                <tr>
                    <td align="center"><?php
                    // @PENDIENTE: Falta recuperar contraseña
                    
                    //echo link_to(__('Forgot your password?'),'@default_index?module=lxreset') ?></td>
                </tr>
            </tfoot>
            <tbody>
                
                <tr>
                    <td align="center">
                        <div id="frmLogin">
                            
                                <?php
                                
                                if( $form->hasErrors() || $form->hasGlobalErrors() ) : ?>
                            <br />
                            <ul class="error_list" >
                                                <?php $errors = $form->getErrorSchema()->getErrors() ?>
                                                <?php if ( count($errors) > 0 ) : ?>
                                                    <?php foreach( $errors as $name => $error ) :?>
                                            <li><?php echo $name ?> : <?php echo $error ?></li>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                        </ul>

                            <?php endif ?>

                            <?php if ($sf_user->hasFlash('msn_error')): ?>
                            <div class="msn_error"><?php echo $sf_user->getFlash('msn_error') ?></div>
                        <?php endif; ?>
                            <table cellpadding="0" cellspacing="3" border="0" style="margin-top: 20px;margin-bottom: 20px;"   >
                                
                                <tr align="left">
                                    <td>
                                        
                                        <?php echo $form['login']->renderLabel(__('Usuario')) ?><br />
                                        <?php echo $form['login']->render(array('class' => 'validate[required]')) ?>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                        <?php echo $form['password']->renderLabel(__('Senha')) ?><br />
                                        <?php echo $form['password']->render(array('class' => 'validate[required]')) ?>
                                    </td>
                                </tr>
                                <tr>
                                   
                                    <td align="right">
                                        <?php echo $form->renderHiddenFields(false) ?>
                                        <input type="submit" value="<?php echo __('Login') ?>" />
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</form>
