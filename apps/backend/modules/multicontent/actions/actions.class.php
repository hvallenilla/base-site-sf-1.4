<?php

/**
 * multicontent actions.
 *
 * @package    joaopaulo
 * @subpackage multicontent
 * @author     Henry Vallenilla <henryvallenilla@gmail.com>
 */
class multicontentActions extends sfActions
{
  public function preExecute() 
  {
        $this->key = $this->getRequestParameter('key');
        $this->module = $this->getContext()->getModuleName(); 
        $this->action = $this->getContext()->getActionName();
        
        if(!$this->getUser()->getAttribute('id_category'))
        {
            $this->getUser()->setAttribute('id_category', $this->key );
        }
        if($this->action == 'index' &&  $this->getUser()->getAttribute('id_category') != $this->key)
        {
            $this->getUser()->setAttribute('id_category', $this->key );
        }
        $this->key = $this->getUser()->getAttribute('id_category');
        $this->infoModulo = CategoryContentPeer::retrieveByPK($this->getUser()->getAttribute('id_category'));
        $this->titulo_modulo = $this->infoModulo->getNombre();
        
    }


  public function executeIndex(sfWebRequest $request)
  { 
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('List').' multicontent - Lynx Cms');
    if (!$this->getRequestParameter('buscador')){
        $this->buscador = '';
    }else{
        $this->buscador = $this->getRequestParameter('buscador');
    }
    if(!$this->getRequestParameter('by'))
    {
        $this->by = 'desc';               // Variable para el orden de los registros
        $this->by_page = "asc";           // Variable para el paginador y las flechas de orden
        $sortTemp =  MultiContentPeer::getFieldNames(BasePeer::TYPE_FIELDNAME);
        $this->sort = $sortTemp[0];      // Nombre del campo que por defecto se ordenara
    }
    //Criterios de busqueda
    $c = new Criteria();
    $c->add(MultiContentPeer::ID_CATEGORY, $this->key, Criteria::EQUAL);
    if($this->getRequestParameter('sort'))
    {
        $this->sort = $this->getRequestParameter('sort');
        switch ($this->getRequestParameter('by')) {
            case 'desc':
                $c->addDescendingOrderByColumn(MultiContentPeer::$this->getRequestParameter('sort'));
                $this->by = "asc";
                $this->by_page = "desc";
                break;
            default:
                $c->addAscendingOrderByColumn(MultiContentPeer::$this->getRequestParameter('sort'));
                $this->by = "desc";
                $this->by_page = "asc";
                break;
        }
    }else{
        $c->addAscendingOrderByColumn($this->sort);
    }
    if($this->getRequestParameter('buscador'))
    {
    //Desactiva temporalmente el metodo de escape para que funcionen los link de la paginacion
    sfConfig::set('sf_escaping_strategy', false);
        $criterio = $c->getNewCriterion(MultiContentPeer::ID, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE);
        $criterio->addOr($c->getNewCriterion(MultiContentPeer::ID_CATEGORY, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
        $criterio->addOr($c->getNewCriterion(MultiContentPeer::TITULO, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
        $criterio->addOr($c->getNewCriterion(MultiContentPeer::RESUMO, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
        $criterio->addOr($c->getNewCriterion(MultiContentPeer::ARQUIVO, '%'.$this->getRequestParameter('buscador').'%', Criteria::LIKE));
        $c->add($criterio);
        $buscador = "&buscador=".$this->buscador;
        $this->bus_pagi = "&buscador=".$this->buscador;
    }else{
        $buscador = "";
        $this->bus_pagi = "";
    }

    $pager = new sfPropelPager('MultiContent',10);
    $pager->setCriteria($c);
    $pager->setPage($this->getRequestParameter('page',1));
    $pager->init();
    $this->MultiContents = $pager;                
    // Crea sesion de la uri al momento
    $this->getUser()->setAttribute('uri_multicontent','sort='.$this->sort.'&by='.$this->by_page.$buscador.'&page='.$this->MultiContents->getPage());
  
  }

  public function executeNew(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Add new').' multicontent - Lynx Cms');
    $this->form = new MultiContentForm();
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeCreate(sfWebRequest $request)
  {
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
    $this->getResponse()->setTitle($this->getContext()->getI18N()->__('Edit').' multicontent - Lynx Cms');
    if (!$request->isMethod('post'))
    {
        $this->redirect("multicontent/new");
    }
    

    $this->form = new MultiContentForm();
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->processForm($request, $this->form);
    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    //Desactiva temporalmente el metodo de escape para que funcione el link Back to list
    sfConfig::set('sf_escaping_strategy', false);
    //PERSONALIZAR SEGUN LA NECESIDAD DEL MODULO
  	$this->getResponse()->setTitle($this->getContext()->getI18N()->__('Edit').' multicontent - Lynx Cms');
    $this->forward404Unless($MultiContent = MultiContentPeer::retrieveByPk($request->getParameter('id')), sprintf('Object MultiContent does not exist (%s).', $request->getParameter('id')));
    $this->form = new MultiContentForm($MultiContent);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
  }

  public function executeUpdate(sfWebRequest $request)
  {
 
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($MultiContent = MultiContentPeer::retrieveByPk($request->getParameter('id')), sprintf('Object MultiContent does not exist (%s).', $request->getParameter('id')));
    $this->form = new MultiContentForm($MultiContent);

    $this->processForm($request, $this->form);
    //Identifica el modulo padre
    $idParentModule = LxModulePeer::getParentIdXSfModule($this->getModuleName());
    $this->moduleParent = LxModulePeer::getParentNameXParentId($idParentModule['parent_id']);
    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($MultiContent = MultiContentPeer::retrieveByPk($request->getParameter('id')), sprintf('Object MultiContent does not exist (%s).', $request->getParameter('id')));
    $MultiContent->delete();

    $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));
    return $this->redirect('multicontent/index?key='.$this->key);
  }



public function executeDeleteAll(sfWebRequest $request)
{
    if ($this->getRequestParameter('chk'))
    {
            foreach ($this->getRequestParameter('chk') as $gr => $val)
            {
                    
                    $this->forward404Unless($MultiContent = MultiContentPeer::retrieveByPk($val), sprintf('Object MultiContent does not exist (%s).', $request->getParameter('id')));
                    $MultiContent->delete();
            }
            $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_delete_confir')));

    }
    return $this->redirect('multicontent/index?key='.$this->key);
}

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $MultiContent = $form->save();
      $MultiContent->setTitulo($form->getValue('titulo'));
      $MultiContent->setResumo($form->getValue('resumo'));
      $MultiContent->setIdCategory($this->key);
      
      //Permalink process
      $link = new globalFunctions();
      $MultiContent->setPermalink($link->crearPermalink($MultiContent->getTitulo()));
      $MultiContent->setDataCreated($form->getValue('data_created'));
      $MultiContent->save();
      
      $id = $this->infoModulo->getKey().'_'.$MultiContent->getId();
      // File process
      if($form->getValue('arquivo'))
      {
        $file = $form->getValue('arquivo');
        // Aqui cargo la imagen con la funcion loadFiles de mi Helper
        sfProjectConfiguration::getActive()->loadHelpers('upload');
        $fileUploaded = loadFile($file->getOriginalName(), $file->getTempname(), 0, sfConfig::get('sf_upload_dir').'/arquivos/' , $id, true);
        $MultiContent->setArquivo($fileUploaded);
        $MultiContent->save();
      }
      if($form->getValue('imagen'))
      {
         $file = $form->getValue('imagen'); 
         $random = aplication_system::GenerateRandomString(); 
         sfProjectConfiguration::getActive()->loadHelpers('upload'); 
         $name_foto = $random.'_'.$id.'.jpg';
         $dir = sfConfig::get('sf_upload_dir').'/imagescontent/';
         $this->fileUploaded = loadFile($name_foto, $file->getTempname(), 0, $dir , $id, true);
         $MultiContent->setImagen($this->fileUploaded);
         $MultiContent->save();
         $file = $dir.$this->fileUploaded;  
         
         $img = new sfImage($file, 'image/jpg');         
         
         //$img->overlay(new sfImage($file, 'image/jpg'), 'top-left');
         //$img->resize(140,null)->overlay(new sfImage($file, 'image/jpg'));          
         // Grande
         $img->thumbnail(580,250, 'center', '#E6E6E6');
         $img->setQuality(100);         
         $img->saveAs($dir.'big_'.$this->fileUploaded);
         // Lista         
         $img->thumbnail(140,70, 'center', '#E6E6E6');
         $img->setQuality(100);
         $img->saveAs($dir.'min_'.$this->fileUploaded);
         // Lista         
         $img->thumbnail(60,75, 'center', '#E6E6E6');
         $img->setQuality(100);
         $img->saveAs($dir.'sm_'.$this->fileUploaded);
         unlink($dir.$this->fileUploaded);
      }
      $this->getUser()->setFlash('listo', $this->getContext()->getI18N()->__(sfConfig::get('app_msn_save_confir')));
      return $this->redirect('multicontent/index?key='.$this->key);
      
    }
  }
}
