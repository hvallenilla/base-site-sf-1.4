<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<link rel="stylesheet" href="js/jq/jquery-ui-1.8.16.custom/development-bundle/themes/base/jquery.ui.all.css" />
<script src="/js/jq/jquery-ui-1.8.16.custom/development-bundle/ui/i18n/jquery.ui.datepicker-br.js"></script>
<script type="text/javascript"> 
$(document).ready(function() {
      $("#multicontent").validationEngine();
      $("#multi_content_data_created").datepicker({
            dateFormat: 'yy-mm-dd',  
            yearRange: '-93:+1',
            changeMonth: true,
            changeYear: true
          });
})
</script>

<form id="multicontent" action="<?php echo url_for('multicontent/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

 <?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<div class="frameForm" align="left">
  <table width="100%">
      <tr>
        <td>
            &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são obrigatórios')?>
        </td>
      </tr>
      <tr>
        <td id="errorGlobal">
            <?php echo $form->renderGlobalErrors() ?>
        </td>
      </tr>
      <tr>
          <td>
              <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                                <?php echo link_to(__('Voltar à lista'), '@default?module=multicontent&action=index&key='.$sf_user->getAttribute('id_category'), array('class' => 'button')) ?>
                        </div>
                    </td>
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                                <?php echo link_to(__('Eliminar'), 'multicontent/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                    <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
          </td>
      </tr>
    <tfoot>
      <tr>
        <td>
            <?php echo $form->renderHiddenFields(false) ?>
            <table cellspacing="4">
                <tr>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Voltar à lista'), '@default?module=multicontent&action=index&key='.$sf_user->getAttribute('id_category'), array('class' => 'button')) ?>
                         </div>
                    </td>            
                    <?php if (!$form->getObject()->isNew()): ?>
                    <td>
                        <div class="button">
                            <?php echo link_to(__('Eliminar'), 'multicontent/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => __('Are you sure you want to delete the selected data?'), 'class' => 'button')) ?>
                        </div>
                    </td>
                    <?php endif; ?>
                    <td>
                    <input type="submit" value="<?php echo __('Salvar') ?>" />
                    </td>
                </tr>
            </table>
        </td>
      </tr>
    </tfoot>
    <tbody>
        <tr>
            <td>                
                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                  <tr>
                      <td>
                          <?php $module = 'imagescontent'; ?>
                          <table cellpadding="0" cellspacing="0" border="0" width="80%" style="margin-top: 15px; margin-bottom: 15px;">
                              <tr>
                                  <td width="3%" align="left" >
                                      <div id="imageFIELD" style="min-height: 110px; min-width: 170px;">
                                          <?php if($form->getObject()->getImagen()):  ?>
                                          <?php echo image_tag('/uploads/'.$module.'/min_'.$form->getObject()->getImagen(), 'class="borderImage" width="150" height="110"')?>
                                          <?php else:?>
                                          <?php echo image_tag('no_image.jpg', 'border=0 width="150" height="110" class="borderImage"');?>
                                          <?php endif;?>
                                      </div>
                                  </td>
                                  <td width="67%" valign="bottom" style="padding-left:7px">
                                      <?php echo $form['imagen']->renderLabel() ?><br />
                                      <?php echo $form['imagen'] ?>
                                      <?php echo $form['imagen']->renderError() ?>
                                      <span class="msn_help"><?php echo $form['imagen']->renderHelp() ?></span>
                                  </td>
                              </tr>
                              <tr>
                                   <td>
                                       <?php if($form->getObject()->getImagen()):?>
                                      <div id="deleteImage" style="margin-left: 40px;" >
                                          <?php echo jq_link_to_remote('Deletar Imagem', array(
                                                'update'  =>  'imageFIELD',
                                                'url'     =>  'multicontent/deleteImage?id='.$form->getObject()->getId(),
                                                'script'  => true,
                                                'confirm' => __('Are you sure you want to delete the selected data?'),
                                                'before'  => "$('#imageFIELD').html('<div>". image_tag('preload.gif','title="" alt=""')."</div>');",
                                                'complete'=> "$('#deleteImage').html('');"
                                            ));
                                            ?>
                                      </div>
                                      <?php endif;?>
                                   </td>
                                   <td>&nbsp;</td>
                              </tr>
                          </table>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['data_created']->renderLabel() ?><br />
                        <?php echo $form['data_created'] ?>
                        <?php echo $form['data_created']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['titulo']->renderLabel() ?><br />
                        <?php echo $form['titulo'] ?>
                        <?php echo $form['titulo']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['resumo']->renderLabel() ?><br />
                        <?php echo $form['resumo'] ?>
                        <?php echo $form['resumo']->renderError() ?>
                    </td>
                  </tr>
                  <tr>
                      <td><?php echo $form['arquivo']->renderLabel() ?><br />
                        <?php echo $form['arquivo'] ?>
                        <?php echo $form['arquivo']->renderError() ?>
                    </td>
                  </tr>
                  
                </table>                
            </td>
        </tr>
    </tbody>
  </table>
    </div>
</form>
