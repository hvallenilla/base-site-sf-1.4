<div id="title_module">
    <div class="frameForm" >
        <h1><?php echo $titulo_modulo ?></h1>
    </div>
<div class="msn_error" id="no_select_item" style="display: none;"><?php echo __("Nenhum item selecionado"); ?>.&nbsp;&nbsp;<a href="#" onclick="noSelectedItem();"><?php echo __('Ocultar'); ?></a> </div>
<?php if ($sf_user->hasFlash('listo')): ?>
    <div class="msn_ready"><?php echo $sf_user->getFlash('listo') ?></div>
<?php endif; ?>
<div class="frameForm">
    <?php echo form_tag('multicontent/deleteAll',array('name' => 'frmChk', 'id' => 'frmChk','style'=>'margin:0px')) ?>
    <table border="0">
        <tr>
            <td>
                <a name="commit" href="#" onclick="return existItems(this);"><?php echo __('Remover todos os') ?></a>
            </td>
            <td>&nbsp;|&nbsp;</td>
            <td>
                <a href="<?php echo url_for($this->getModuleName().'/new') ?>"><?php echo __('Adicionar novo registro')?></a>
            </td>
        </tr>
    </table>
</div>
<table cellpadding="0" cellspacing="0" border="0"  id="resultsList">
  <thead>
    <tr>
    <th>
            &nbsp;<input type="checkbox" id="chkTodos" value="checkbox" onClick="checkTodos(this);" >&nbsp;
    </th>
    <th>
      <?php echo link_to(__('Titulo'),'@default?module=multicontent&action=index&sort=titulo&by='.$by.'&page='.$MultiContents->getPage().'&buscador='.$buscador) ?>
    <?php if($sort == "titulo"){ echo image_tag($by_page); }?>
    </th>  
    
    </tr>
  </thead>
  <tbody>
  <?php if ($MultiContents->getNbResults()): ?>
  	<?php $i=0; ?>
    <?php foreach ($MultiContents as $MultiContent): ?>
    <?php fmod($i,2)?$class = "grayBackground":$class=''; ?>
    <tr class="<?php echo $class;?>" valign="top" onmouseover="javascript:overRow(<?php echo $i; ?>);" onmouseout="javascript:outRow(<?php echo $i; ?>);">
        <td class="borderBottomDarkGray" width="28" align="center">&nbsp;<input type="checkbox" id="chk_<?php echo $MultiContent->getId() ?>" name="chk[<?php echo $MultiContent->getId() ?>]" value="<?php echo $MultiContent->getId() ?>">&nbsp;</td>
        <td class="borderBottomDarkGray">
            <div class="displayTitle">
               <div id="title">                               
                    <a href="<?php echo url_for('multicontent/edit?id='.$MultiContent->getId()) ?>" class="titulo"><?php echo $MultiContent->getTitulo() ?></a>
               </div>
                <div class="row-actions">
                    <div class="row-actions_<?php echo $i; ?>" style="display: none;">
                        <a href="<?php echo url_for('multicontent/edit?id='.$MultiContent->getId(), $MultiContent) ?>" class="edit"><?php echo __('Editar') ?></a>&nbsp;|&nbsp;
                        <?php echo link_to(__('Excluir'),'multicontent/delete?id='.$MultiContent->getId(), array('method' => 'delete', 'class' => 'delete' , 'confirm' => __('Are you sure you want to delete the selected data?'))) ?>
                    </div>
                </div>
            </div>
        </td>
        
    </tr>
    <?php $i++; ?>
    <?php endforeach; ?>
  </tbody>
</table>
    <?php else: ?>
    <table width="100%" align="center"  border="0" cellspacing="10">
        <tr>
            <td align="center"><strong><?php echo __('Sua busca não gerou resultados') ?></strong></td>
        </tr>
    </table>
    <?php endif; ?>
  
</form>
<?php if ($MultiContents->haveToPaginate()): ?>
<table width="100%" align="center" id="paginationTop" border="0">
	<tr>
    	<td align="left" ><i><?php echo $MultiContents->getNbResults().' '.__('resultados') ?>  - <?php echo __('page').' '.$MultiContents->getPage().' '.__('for').' ' ?> <?php echo $MultiContents->getLastPage() ?></i> </td>
        <td align="right">	
        	<table>
                	<tr>
                		<?php if ($MultiContents->getFirstPage()!=$MultiContents->getPage()) :?>
                		<td><?php echo link_to(image_tag('icon_first_page.jpg','alt='.__('First').' title='.__('First').' border=0'), '@default?module=multicontent&action=index&sort='.$sort.'&by='.$by_page.'&page='.$MultiContents->getFirstPage().$bus_pagi) ?></td>
                		<td><?php echo link_to(image_tag('icon_prew_page.jpg','alt='.__('Previous').' title='.__('Previous').' border=0'),'@default?module=multicontent&action=index&sort='.$sort.'&by='.$by_page.'&page='.$MultiContents->getPreviousPage().$bus_pagi) ?></td>
                		<?php endif; ?>
                		<td >
                		<?php $links = $MultiContents->getLinks(); 
                        
	                        foreach ($links as $page): ?>
	                        <?php echo ($page == $MultiContents->getPage()) ? '<strong>'.$page.'</strong>' : link_to($page, '@default?module=multicontent&action=index&sort='.$sort.'&by='.$by_page.'&page='.$page.$bus_pagi) ?>
		                        <?php if ($page != $MultiContents->getCurrentMaxLink()): ?>
		                        -
		                        <?php endif; ?>
	                        <?php endforeach; ?>
                		</td>
                		<?php if ($MultiContents->getLastPage()!=$MultiContents->getPage()) :?>
                		<td><?php echo link_to(image_tag('icon_next_page.jpg','alt='.__('Next').' title='.__('Next').' border=0'), '@default?module=multicontent&action=index&page='.$MultiContents->getNextPage().$bus_pagi) ?></td>
                		<td><?php echo link_to(image_tag('icon_last_page.jpg','alt='.__('Last').' title='.__('Last').' border=0'), 'multicontent/index?page='.$MultiContents->getLastPage().$bus_pagi) ?></td>
                		<?php endif; ?>
                	</tr>
            </table>
		</td>
	</tr>
</table>
<?php else: ?>
<div class="results">
    <i><?php echo $MultiContents->getNbResults().' '.__('resultados') ?></i>
</div>
<?php endif; ?>
</div>

