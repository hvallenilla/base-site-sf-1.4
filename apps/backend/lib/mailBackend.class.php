<?php
/**
 * mailBackend PHP 
 * @package Carapicuiba
 * @author Henry Vallenilla <henryvallenilla@gmail.com>
 *
 */
class mailBackend
{
    public function __construct() {
        
    }
    
    static function mailNovaNoticia($idNew)
    {
        $datosNoticia = SfNewsPeer::getDataNew($idNew);
        $emailsender = sfConfig::get('app_email_administrator');
        
        if(PHP_OS == "Linux") 
        {
            $quebra_linha = "\n"; //Se for Linux
        }elseif(PHP_OS == "WINNT"){
            $quebra_linha = "\r\n"; // Se for Windows
        }
        $global = new globalFunctions();
        $mensagemHTML = $global->headerMail();
        $mensagemHTML.= '            
            <tr>
                <td style="font-family:Arial,Helvetica,sans-serif;margin:0px;font-size:14px;color: #333">
                  O Usuário da <b>'.$datosNoticia['nome_perfil'].'</b> publicou este conteúdo e esta no aguardo da aprovação:
                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:13px; padding:15px; color: #333">
                    <br />
                    <strong>Codigo Noticia</strong>: '.$datosNoticia['id_news'].'<br />
                    <strong>Titulo Noticia</strong>: '.$datosNoticia['title'].'<br /><br />
                    <a href="http://www.carapicuiba.sp.gov.br/backend.php">Acessar o Painel Administrativo</a>
                </td>
            </tr>
        ';
        $mensagemHTML.= $global->footerMail();
        
        $headers = "MIME-Version: 1.1".$quebra_linha;
        $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
        $headers .= "From: ".$emailsender.$quebra_linha;
        $headers .= "Return-Path: " . $emailsender . $quebra_linha;
        mail(sfConfig::get('app_email_receptor_noticias'), "Prefeitura de Carapicuiba - Nova Noticia", $mensagemHTML, $headers, "-r". $emailsender);
        //mail('henryvallenilla@gmail.com', "Prefeitura de Carapicuiba - Nova Noticia", $mensagemHTML, $headers, "-r". $emailsender);
    }
    
    
}
