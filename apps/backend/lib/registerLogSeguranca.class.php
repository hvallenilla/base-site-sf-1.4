<?php
    /**
     *  Filtro que registra las acciones del usuario logeado en los módulo que tienen acceso
     *  Se valida de acuerdo a las permisologias de acceso     
     *  @author     Henry Vallenilla <henryvallenilla@gmail.com>  
     */
    class registerLogSeguranca extends sfFilter
    {
      public function execute ($filterChain)
      {
        $filterChain->execute();
        // ... Antes de la ejecución de la acción        
        if(sfContext::getInstance()->getModuleName() != 'lxlogin' && sfContext::getInstance()->getModuleName() != 'seguranca' && sfContext::getInstance()->getModuleName() != 'home')
        {
            $this->log = new sfLogActivities();
            $this->log->registerLog();
        }        
      }
    }
?>
