<?php use_helper('Date')?>
<div class="data">
    <?php if($palabra):?>
    <div style="height: 40px; color: #333;">
        Pesquisa relacionada com "<b><?php echo $palabra ?></b>"
    </div>
    <?php endif; ?>
    <?php if($total > 1):?>
        <ul class="newsPress">
            <?php foreach ($lista as $entrada):?>
                <li class="wip-block gallery-item-block">
                    <div class="wip-main cfix">
                        <a href="<?php echo url_for('@permalink?secciones='.$seccion.'&subseccion=detalle&permalink='.$entrada['permalink']) ?>">
                            
                            <?php if(file_exists(sfConfig::get('sf_upload_dir').'/news/small_'.$entrada['image'])): ?>
                                <?php echo image_tag('/uploads/news/small_'.$entrada['image'],'width="175" height="98"') ?>                                
                            <?php else: ?>
                                <?php echo image_tag('frontend/no_foto','width="175" height="98"') ?>                                
                            <?php endif; ?>
                        </a>
                        <div class="wip-info left">
                            <div class="fecha"><?php echo ($entrada['data']) ? format_date($entrada['data'], 'p', $sf_user->getCulture()) : '--' ?></div>
                            <div class="wip-title"><?php echo link_to($entrada['title'], '@permalink?secciones='.$seccion.'&subseccion=detalle&permalink='.$entrada['permalink']) ?></div>
                            <div class="wip-owner text-ellipsis">
                                <?php echo substr(html_entity_decode($entrada['sumario']),0,250) ?> 
                            </div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?> 
        </ul>
    <?php else:?>
        <div style="height: 40px; color: #D51921; text-align: center;">
            <?php echo 'Não há conteúdo para esta seção neste momento';?>
        </div>
    <?php endif; ?>
</div>
<?php if($total > 1):?>
    <?php echo html_entity_decode($paginator ) ?>
<?php endif; ?>