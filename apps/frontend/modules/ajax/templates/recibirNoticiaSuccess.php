<table border="0" cellpadding="0" cellspacing="5">
    <tbody>                
        <tr>
            <td align="center" style="padding: 0px !important;">
                <div id="frmLogin">
                    <?php if( $formNewsletter->hasErrors() || $formNewsletter->hasGlobalErrors() ) : ?>
                        <ul class="error_list" >
                            <?php $errors = $formNewsletter->getErrorSchema()->getErrors() ?>
                            <?php if ( count($errors) > 0 ) : ?>
                                <?php foreach( $errors as $name => $error ) :?>
                                    <li><?php echo $name ?> <?php echo $error ?></li>
                                <?php endforeach ?>
                            <?php endif ?>
                        </ul>
                    <?php endif ?>
                    <?php if ($sf_user->hasFlash('msj_news_listo')): ?>
                    <div class="msn_ready"><?php echo $sf_user->getFlash('msj_news_listo') ?></div>
                    <?php endif; ?>
                    <table cellpadding="0" cellspacing="3" border="0" style="margin-top: 0px;margin-bottom: 20px;" >
                        <tr align="left">
                            <td>
                                <?php echo $formNewsletter['nombre']->renderLabel('Nome') ?><br />
                                <?php echo $formNewsletter['nombre']->render(array('class' => 'validate[required]')) ?>
                            </td>
                            <td rowspan="3">
                                <?php echo $formNewsletter->renderHiddenFields(false) ?>
                                <input type="submit" value="<?php echo 'Enviar' ?>" class="" id="newsletter_button" name="newsletter_button" />
                            </td>
                        </tr>
                        <tr align="left" >
                            <td style="background-color: #FFF !important;"  >
                                <?php echo $formNewsletter['email']->renderLabel('Email') ?><br />
                                <?php echo $formNewsletter['email']->render(array('class' => 'validate[required]')) ?>
                            </td>
                        </tr>
                        <tr>

                            <td align="right">
                                
                            </td>
                        </tr>

                    </table>
                </div>
            </td>
        </tr>
    </tbody>
</table>