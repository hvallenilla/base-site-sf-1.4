<?php

/**
 * ajax actions.
 *
 ** @package   base_sf1.4
 * @subpackage ajax
 * @author     Henry Vallenilla <henryvallenilla@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ajaxActions extends sfActions
{
    /**
     * Lista Ajax Noticias
     * @param sfWebRequest $request
     */
    public function executeNews(sfWebRequest $request)
    {
        $global = new globalFunctions();
        $page = $request->getParameter('page');
        $this->palabra = $request->getParameter('palabra');
        $this->seccion = 'noticias';
        $cur_page = $page;
        $page -= 1;
        $per_page = 5;
        $start = $page * $per_page;
        
        $this->lista = SfNewsPeer::getNewsPagination($start, $per_page, $this->palabra); 
        $this->total = SfNewsPeer::getCount($this->palabra);
        
        $no_of_paginations = ceil($this->total / $per_page);
        $this->paginator = $global->generatePaginator($cur_page, $no_of_paginations);        
    }
      
    public function executeRecibirNoticia(sfWebRequest $request)
    {
      $this->setLayout(false);
      $this->formNewsletter = new NewsletterForm();
      if ($request->isMethod('post')) {
        $this->formNewsletter->bind(array_merge($request->getParameter($this->formNewsletter->getName()), array()));
        if($this->formNewsletter->isValid()){
            $this->getUser()->setFlash('msj_news_listo', 'Suas informações foram enviadas com sucesso');
            $newsletter = $this->formNewsletter->save();
        }            
      }      
    }
    
    
    
}
