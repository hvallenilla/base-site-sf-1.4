<div class="column span-17 first">
  <?php foreach ($infoSecciones as $infoSeccion): ?>
    <?php if(!$infoSeccion['onlyComplement'] or $infoSeccion['showText']): ?>

      <?php if(!$infoSeccion['onlyComplement'] && $sf_request->getParameter('secciones', 'home') !== $prinSeccion): ?>
      <h1><?php echo $infoSeccionPadre['nameSection']; ?></h1>
      <?php endif; ?>

      <?php if($sf_request->getParameter('subseccion') and $infoSeccionPadre['nameSection']!=$infoSeccion['nameSection']): ?>
      <h2><?php echo strtoupper($infoSeccion['nameSection']) ?></h2>
      <?php endif; ?>

      <?php if($infoSeccion['showText']): ?>
      <?php echo html_entity_decode($infoSeccion['descripSection']); ?>
      <?php endif; ?>
      <?php if($infoSeccion['specialPage']): ?>
      <?php include_component('component', $infoSeccion['specialPage'],array('nameSection' => $infoSeccion['nameSection'], 'descripSection'=>$infoSeccion['descripSection'], 'infoSection' => $infoSeccion )) ?>
      <?php endif; ?>
      
    <?php endif; ?>
  <?php endforeach; ?>
</div>
