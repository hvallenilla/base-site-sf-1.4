<?php

class componentComponents extends sfComponents {

  public function executeMenuTop(sfWebRequest $request)
  {
      $this->secciones = $request->getParameter('secciones');
      $this->secoePai = SfSectionPeer::validatePaternSectionByPermalink($this->secciones);
      
      if($this->secoePai['parent_id'])
      {
          $this->sw_menu = ExtendSfSection::getSwMenuSection($this->secoePai['parent_id']);
          $this->sw_menu = $this->sw_menu['sw_menu'];
      }else{
          $this->sw_menu = $this->secciones;          
      }
      
      $this->sections = ExtendSfSection::searchParentSectionByProfile($this->getUser()->getCulture());
      $this->html="";
      if($this->sections)
      {
        foreach ($this->sections as $section):
            $cls = '';
            if($section['url_externa'])
              {
                  $this->html.="<li>".link_to($section['nameSection'],''.$section['url_externa'].'','target="_blank"');			
              }else{
                  $this->html.="<li>".link_to($section['nameSection'],'@menu?secciones='.$section['sw_menu']);
              }
              $this->html.= $this->ArmarArbolHijo($section['id']);
              
          $this->html.="</li>";
        endforeach;
      }
  }
  
  public function ArmarArbolHijo($id_padre="")
    {
      $htm_axu="";	
      $children = ExtendSfSection::searchChildrenSection($id_padre,'pt');					
      if($children)
      {
        $htm_axu.="<ul>";
        foreach ($children as $subTmp)
        {
          if($subTmp['url_externa'])
              {
                  $htm_axu.="<li>".link_to($subTmp['nameSection'],''.$subTmp['url_externa'].'','target="_blank"');			
              }else{
                  if($subTmp['control'])
                  {
                      $htm_axu.="<li>".link_to($subTmp['nameSection'],'@menu?secciones='.$subTmp['sw_menu']);
                  }else{
                      $htm_axu.="<li>".$subTmp['nameSection'];
                  }
                  
              }  
          
          $htm_axu.= $this->ArmarArbolHijo($subTmp['id']);
          $htm_axu.="</li>";
        }
        $htm_axu.="</ul>";
      }
      return $htm_axu;
    }
    
  public function executeFooter(sfWebRequest $request)
  {
    $this->nucleo = $request->getParameter('nucleo');
    $this->sections  = ExtendSfSection::searchSectionFooter($this->getUser()->getCulture());
    $this->swContact = ExtendSfSection::searcSwicheMenu(20);
    
    
  }
  
  public function executeBusca(sfWebRequest $request)
  {
      
  }
  
  public function executeHomeRecibirNoticias(sfWebRequest $request)
  {
    $this->formNewsletter = new NewsletterForm();
    if ($request->isMethod('post')) {
        if($request->getParameter('newsletter_button'))            
        {
            $this->formNewsletter->bind(array_merge($request->getParameter($this->formNewsletter->getName()), array()));
            if($this->formNewsletter->isValid()){
                $newsletter = $this->formNewsletter->save();
            }
        }
    }
  }
  
  public function executeTopNoticias(sfWebRequest $request)
  {
      $this->nucleo = $request->getParameter('nucleo');
      $this->topNews = SfNewsAccessPeer::getTopNews(29, 4);
  }

  public function executeHome()
  {
    $this->nucleoForHome        = 29;   // Es el perfil - Nucleo INSTITUCIONAL
    $this->categoriaLastNews    = 2; // Es el codigo de la categoria que se encuentra en el app.yml
    $this->categoriaAgenda      = 3; // Es el codigo de la categoria que se encuentra en el app.yml
    $this->categoriaAcontece    = 1; // Es el codigo de la categoria que se encuentra en el app.yml
    $this->categoriaInformativo = 0; // Es el codigo de la categoria que se encuentra en el app.yml
    $this->i                    = false;

    $this->newsHomeCarousel     = SfNewsPeer::getNewsHome(20);

    $this->lx_valida    = new lynxValida();
    
  }
  
  public function executeGallery(sfWebRequest $request)
  {
      $this->nucleo = $request->getParameter('nucleo');
      $this->secciones = $request->getParameter('secciones');
      $this->idNucleo = LxProfilePeer::getDataByPermalink($this->nucleo);
      
      $this->valida = new lynxValida();
      
      $current_action = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();
      
      if($this->getRequestParameter('subseccion') && $this->getRequestParameter('subseccion') != 'page' )
      {
        $codigoAlbum = $this->valida->extraerCodigoTitulo($this->getRequestParameter('subseccion'));
        $current_action->forward404If(!$codigoAlbum);
        
        // Valida existencia del album
        $this->galleryDetail = ExtendSfAlbum::viewListAlbunsByIdByNucleo($this->idNucleo->getIdProfile(), $codigoAlbum);
        $current_action->forward404If(!$this->galleryDetail);
        
      }else{          
          $limit = 12; 
          $stages = 3;
          
          $this->total_pages = ExtendSfAlbum::getCountPagination($this->idNucleo->getIdProfile());
          $page = $this->getRequestParameter('permalink');
          if($page)
          {
            $start = ($page - 1) * $limit; 
          }else{
            $start = 0;	
          }	
          
          $this->galleryList = ExtendSfAlbum::getGaleriaPaginate($this->idNucleo->getIdProfile(), $start, $limit );          
          $this->paginator = globalFunctions::generatePaginatorSimple($this->nucleo, $this->secciones, $this->total_pages, $page, $limit, $stages);    
        
      }
  }

  public function executeNews(sfWebRequest $request)
  { 
    $this->nucleo = $request->getParameter('nucleo');
    $this->valida = new lynxValida();
    $this->idNucleo = LxProfilePeer::getDataByPermalink($this->nucleo);
    $this->links    = ExtendSfSection::retrieveByPKs(array(30, 45, 46 , 47));
    $current_action = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();
    if($this->getRequestParameter('permalink'))
    {

        $this->news = ExtendSfNews::selectNewsDetail($this->getRequestParameter('permalink'));
        $current_action->forward404If(!$this->news);
        //Add Metas
        $this->getResponse()->setTitle($this->news->getTitle()." - ".$this->nameSection." - ".sfConfig::get('app_namecompany'));
        if(!$this->infoSection['metaDescription']){
            $this->getResponse()->addMeta('Description', strip_tags($this->news->getSummary()));
        }
        if(!$this->infoSection['metaKeyword']){
            $this->getResponse()->addMeta('Keywords', str_replace(',,', ',', implode(',', explode(' ', strip_tags($this->news->getTitle())))));
        }

    }else{
        if($request->getParameter('palabra'))
        {
            
            //$this->newss = ExtendSfNews::selectNewsLike($this->getRequestParameter('page', 1),0, $request->getParameter('palabra'));
        }else{
            //$this->newss = ExtendSfNews::selectListNews($this->getRequestParameter('page', 1), $this->idNucleo->getIdProfile());
        }
        
    }
  }

  
  public function executeNewsHome(sfWebRequest $request)
  {
      $this->newsHomeCarousel     = SfNewsPeer::getNewsHome(100);
      
  }

  public function executeUltimasNoticias(sfWebRequest $request)
  {
    $this->nucleo = $request->getParameter('nucleo');
    $this->secciones = $request->getParameter('secciones');
    $this->valida = new lynxValida();
    $this->idNucleo = LxProfilePeer::getDataByPermalink($this->nucleo);
    $this->newss = ExtendSfNews::selectUltimasNoticias($this->getRequestParameter('page', 1), $this->idNucleo->getIdProfile());
  }


  public function executePagination(sfWebRequest $request)
  {
    if($request->hasParameter('subseccion'))
    {
      $this->enlace = '@pager?secciones='.$request->getParameter('secciones').'&subseccion='.$request->getParameter('subseccion');
    }else{
      $this->enlace = '@menu?secciones='.$request->getParameter('secciones');
    }
    $this->enlace = urlencode($this->enlace);
  }

  public function executeContact(sfWebRequest $request) {
      
      $this->nucleo = $request->getParameter('nucleo') ? $request->getParameter('nucleo') : 'carapicuiba';
      $this->form = new ExtendsContactForm();
        if ($request->isMethod('post')) {
            /*$captcha = array(
                    'recaptcha_challenge_field' => $request->getParameter('recaptcha_challenge_field'),
                    'recaptcha_response_field'  => $request->getParameter('recaptcha_response_field'),
            );*/
            //$this->form->bind(array_merge($request->getParameter($this->form->getName()), array('captcha' => $captcha)));
            $this->form->bind(array_merge($request->getParameter($this->form->getName()), array()));
            if ($this->form->isValid()) {
                $Contact = $this->form->save();
                //$this->getContext()->getController()->forward('mail','SendMailContact');
                $this->getUser()->setFlash('listo',sfConfig::get('app_msn_save_confir'));
                $this->form = new ExtendsContactForm();
            }
            else{
                $this->getUser()->setFlash('error',sfConfig::get('app_msn_save_err'));
            }
        }
  }

  public function executeSiteMap()
  {
    $this->sections = ExtendSfSection::searchParentSection($this->getUser()->getCulture());
  }

  
}
?>
