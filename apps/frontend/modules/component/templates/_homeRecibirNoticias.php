<div class="home_article_title texto_azul">
    <b>Receber notícias da Cidade</b>
</div>

<div class="div_float">
    <?php echo jq_form_remote_tag(array(
        'update' => 'content_newsletter',
        'url'    => 'ajax/recibirNoticia',        
        'loading'  => "$('#indicator').show();$('#content_newsletter').hide();",
        'complete' => "$('#indicator').hide();$('#content_newsletter').show();",
      ), array(
        'id'     => 'frm_newsletter',)
      ) 
    ?>
    <div id ="indicator" class="" align="left" style="display: none;" >
        <p><?php echo image_tag('preload.gif').' Loading...'; ?></p>
    </div>
    <div id="content_newsletter">
    <!--<form id="frm_newsletter" action="" method="post" <?php $formNewsletter->isMultipart() and print 'enctype="multipart/form-data" ' ?>>-->
        <table border="0" cellpadding="0" cellspacing="5">
            <tbody>                
                <tr>
                    <td align="center" style="padding: 0px !important;">
                        <div id="frmLogin">
                            <?php if( $formNewsletter->hasErrors() || $formNewsletter->hasGlobalErrors() ) : ?>
                                <ul class="error_list" >
                                    <?php $errors = $formNewsletter->getErrorSchema()->getErrors() ?>
                                    <?php if ( count($errors) > 0 ) : ?>
                                        <?php foreach( $errors as $name => $error ) :?>
                                            <li><?php echo $name ?> <?php echo $error ?></li>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </ul>
                            <?php endif ?>
                            <?php if ($sf_user->hasFlash('msn_error')): ?>
                            <div class="msn_error"><?php echo $sf_user->getFlash('msn_error') ?></div>
                            <?php endif; ?>
                            <table cellpadding="0" cellspacing="3" border="0" style="margin-top: 0px;margin-bottom: 20px;" >
                                <tr align="left">
                                    <td>
                                        <?php echo $formNewsletter['nombre']->renderLabel('Nome') ?>&nbsp;&nbsp;
                                        <?php echo $formNewsletter['nombre']->render(array('class' => 'validate[required]')) ?>
                                    </td>
                                    <td rowspan="3">
                                        <?php echo $formNewsletter->renderHiddenFields(false) ?>
                                        <input type="submit" value="<?php echo 'Enviar' ?>" class="boton" id="newsletter_button" name="newsletter_button" />
                                    </td>
                                </tr>
                                <tr align="left" >
                                    <td style="background-color: #FFF !important;"  >
                                        <?php echo $formNewsletter['email']->renderLabel('Email') ?>&nbsp;&nbsp;
                                        <?php echo $formNewsletter['email']->render(array('class' => 'validate[required]')) ?>
                                    </td>
                                </tr>
                                <tr>

                                    <td align="right">
                                        
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</div>