<div class="column span-17">
    <br />
    <h2>Notícias</h2>
    <ul>
        <form action="<?php echo url_for('@menu?nucleo=carapicuiba&secciones=noticias') ?>" method="post" enctype="multipart/form-data" >
            <input type="hidden" name="palabra" value="<?php echo $palabra ?>" />
            <li><b><?php echo count($totalNoticias) ?></b> Resultados em Notícias <?php if(count($totalNoticias) > 0):?><a href="javascript:void();" onclick="$(this).parents('form').submit();">mais detalhes +</a><?php endif;?></li>
        </form>
        
    </ul>
    <br />
    <h2>Serviços</h2>
    <ul>
        <form action="<?php echo url_for('@menu?nucleo=carapicuiba&secciones=servicos') ?>" method="post" enctype="multipart/form-data" >
            <input type="hidden" name="palabra" value="<?php echo $palabra ?>" />        
            <li><?php echo count($totalServicios) ?> Resultados em Serviços <?php if(count($totalServicios) > 0):?><a href="javascript:void();" onclick="$(this).parents('form').submit();">mais detalhes +</a><?php endif;?></li>
        </form>        
    </ul>
</div>