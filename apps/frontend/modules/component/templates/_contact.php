<?php use_helper('I18N') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<script type="text/javascript">
$(document).ready(function() {
      $("#frmContact").validationEngine()
})
</script>
<form id="frmContact" action="" method="post"  method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if ($form->isCSRFProtected()) : ?>
  <?php echo $form['_csrf_token']; ?>
<?php endif; ?>
<?php echo $form['contact_type'] ?>
    <table cellpadding="0" cellspacing="0" border="0">
        <?php if ($sf_user->hasFlash('listo')): ?>
    <div class="msn_ready"><?php echo $sf_user->getFlash('listo') ?></div>
        <?php elseif($sf_user->hasFlash('error')):?>
    <div class="msn_error"><?php echo $sf_user->getFlash('error') ?></div>
        <?php endif; ?>
    <tr>
        <td colspan="2" style="color: #000" >
            &nbsp;<?php echo __('Os campos marcados com') ?> <span class="required">*</span> <?php echo __('são obrigatórios')?>
        </td>
      </tr>
      <tr>
        <td colspan="3" id="errorGlobal">
            <?php echo $form->renderGlobalErrors() ?>
        </td>
      </tr>
         <tr>
             <td colspan="3">
               <?php echo $form['first_name']->renderLabel() ?><br />
               <?php echo $form['first_name'] ?>
               <?php echo $form['first_name']->renderError() ?>
          </td>           
        </tr>
        <tr>
           <td colspan="3">
               <?php echo $form['last_name']->renderLabel() ?><br />
               <?php echo $form['last_name']->render() ?>
               <?php echo $form['last_name']->renderError() ?>
          </td>
        </tr>
        <tr>
           <td colspan="3">
               <?php echo $form['email']->renderLabel() ?><br />
               <?php echo $form['email'] ?>
               <?php echo $form['email']->renderError() ?>
          </td>
        </tr>
        <tr>
           <td colspan="3">
               <?php echo $form['comments']->renderLabel() ?><br />
               <?php echo $form['comments'] ?>
               <?php echo $form['comments']->renderError() ?>
          </td>
        </tr>
<!--        <tr>
            <td colspan="3">
            <?php //echo $form['captcha']->render() ?>
            <?php //echo $form['captcha']->renderError() ?>
            </td>
        </tr>-->
        <tr>
          <td colspan="3">
              <input type="submit" name="send" value="Enviar" />
          </td>
        </tr>

    </table>
</form>