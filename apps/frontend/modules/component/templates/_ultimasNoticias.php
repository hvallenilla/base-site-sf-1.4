<?php use_helper('Date') ?>
<div class="column span-9 last border-forma separacion-top" style="float: right;">
    <h2>Últimas Noticias</h2>
    <br>
    <?php if(count($newss)):?>
        <ul class="shop-container">
        <?php foreach($newss->getResults() as $news): ?>
            <li style="margin-bottom: 32px; border-bottom: 1px dotted #ccc;padding-bottom: 30px;">
               <a href="<?php echo url_for('@permalink?nucleo='.$nucleo.'&secciones=noticias&subseccion=detalle&permalink='.$news->getPermalink()) ?>">
                <?php if($news->getImage()):  ?>
                    <?php echo image_tag('/uploads/news/small_'.$news->getImage(), ' width="92" height="52" ')?>               
                <?php else:?>
                    <?php echo image_tag('/uploads/news/'.$news->getIdNews().'_imagem_5.jpg', ' width="92" height="52" ')?>
                <?php endif;?>                
                    <big><?php echo format_date($news->getDate(), 'D')  ?></big>
                    <span class="title"><?php echo $news->getTitle() ?></span>
                    <strong><?php echo substr(html_entity_decode($news->getSummary()),0,110) ?> </strong>
               </a>
            </li>
        <?php endforeach; ?>
        </ul>
    <?php else:?>
        <?php echo 'Não há conteúdo para esta seção neste momento';?>
    <?php endif;?>
</div>
