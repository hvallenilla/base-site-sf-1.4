<?php if ($childrenSection): ?>
<div id="subMenu" align="left">
  <h4><?php echo link_to($nameParentSection['nameSection'], '@menu?secciones='.$linkParentSection['swMenu'], 'class="black"') ?></h4>

  <ul>
    <?php foreach ($childrenSection as $sonSection): ?>
    <?php $subsecciones = ExtendSfSection::searchChildrenSection($sonSection['id'], $sf_user->getCulture()) ?>
    <?php ($sf_request->getParameter('subseccion') == $sonSection['sw_menu'] && !$subsecciones) ? $class = ' class="selected"' : $class = "" ?>
    <li>
      <?php echo link_to($sonSection['nameSection'],'@submenu?secciones='.$sf_request->getParameter('secciones').'&subseccion='.$sonSection['sw_menu'], $class) ?>
      
      <?php if($subsecciones): ?>
      <ul class="sublevel">
        <?php foreach($subsecciones as $subseccion): ?>
        <?php ($sf_request->getParameter('permalink') == $subseccion['sw_menu']) ? $class = ' class="selected"' : $class = "" ?>
        <li><?php echo link_to($subseccion['nameSection'], '@permalink?secciones='.$sf_request->getParameter('secciones').'&subseccion='.$sonSection['sw_menu'].'&permalink='.$subseccion['sw_menu'], $class) ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>

    </li>
    <?php endforeach; ?>
  </ul>
</div>
<?php endif; ?>
