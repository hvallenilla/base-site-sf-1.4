<?php use_helper('Date') ?>
<script type="text/javascript">
    $(document).ready(function() {
        var url_page = 'http://'+location.hostname+'<?php echo sfConfig::get('app_base_dir_web') ?>frontend_dev.php/';
        
        function loading_show(){
            $('#loading').html("<img src='<?php echo sfConfig::get('app_base_dir_web') ?>images/preloader.gif'/>").fadeIn('fast');
        }
        function loading_hide(){
            $('#loading').fadeOut('fast');
        }  
        function loadData(page){
            loading_show();                    
            $.ajax
            ({
                type: "POST",
                url:  "<?php echo url_for('ajax/news') ?>",
                data: "page="+page + "&palabra=<?php echo $sf_request->getParameter('palabra') ?>",
                success: function(msg)
                {
                    $("#paginador").ajaxComplete(function(event, request, settings)
                    {
                        loading_hide();
                        $("#paginador").html(msg);
                    });
                }
            });
        }
        loadData(1);  // For first time page load default results
        $('#paginador .pagination li.active').live('click',function(){
            var page = $(this).attr('p');
            loadData(page);
        }); 
    });
</script>

<div class="column span-14 first" style="width: 590px;">
<?php if($sf_request->hasParameter('permalink')): ?>
    <span style="font-size: 12px;">
    <?php if($news->getDate()):?>
        <?php echo format_date($news->getDate(), 'D') ?>
    <?php endif; ?>
    </span>    
    <h3><?php echo $news->getTiTle() ?></h3>
    
    <div style="width: 550px; text-align: justify; margin-top: 20px;">
    <?php if($news->getImage()):  ?>
        <?php //echo sfConfig::get('sf_upload_dir').'/news/big_'.$news->getImage() ?>
        <?php if(file_exists(sfConfig::get('sf_upload_dir').'/news/big_'.$news->getImage())):?>
            <?php echo image_tag('/uploads/news/big_'.$news->getImage(), 'class="borderImage"  width="573" height="322" style="padding:0 3px 0px !important;" ')?>
        <?php else: ?>
            <?php echo image_tag('/uploads/news/'.$news->getIdNews().'_imagem_5.jpg', 'class="borderImage" width="573" height="322" style="padding:0 3px 0px !important;" ')?>
        <?php endif; ?>        
    <?php else:?>
        <?php echo image_tag('/uploads/news/'.$news->getIdNews().'_imagem_5.jpg', 'class="borderImage" width="573" height="322" style="padding:0 3px 0px !important;" ')?>
    <?php endif;?>
    </div>
    <div style="width: 574px; text-align: justify; margin-top: 20px; padding-left: 3px;">
        <?php echo htmlspecialchars_decode($news->getBody()) ?>
    </div>
  
   <br />
   <hr class="blue">
  <?php echo link_to('Ver todas as notícias', '@menu?secciones='.$sf_params->get('secciones')) ?>
<?php else: ?>
    
    <div id="loading"></div>
    <div id="paginador">
        <div class="data"></div>
        <div class="pagination"></div>
    </div>    
    
<?php endif; ?>
</div>