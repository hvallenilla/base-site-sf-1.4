<?php if(count($newsHomeCarousel)): ?>
<?php $valida   = new lynxValida(); ?>
<div class="column span-24 last border-forma" style="margin-top: 15px; height: 355px;">
    <h2>Noticias</h2>
    <div class="carousel noticias-home">    
        <div class="jCarouselLite" style="left: 12px !important; width: 960px !important; height: 240px;">
            <ul style="margin: 0px; padding: 0px; position: relative; list-style-type: none; z-index: 1; width: 2890px; left: -850px;">
                <?php foreach($newsHomeCarousel as $news): ?>
                <li>
                    <a href="<?php echo url_for('@permalink?nucleo=carapicuiba&secciones=noticias&subseccion=detalle&permalink='.$news['permalink']) ?>">
                        <?php if($news['image']): ?>
                            <?php if(file_exists(sfConfig::get('sf_upload_dir').'/news/big_'.$news['image'])):?>
                                <?php echo image_tag('/uploads/news/small_'.$news['image'],'width="219" height="126"') ?>                                            
                            <?php else: ?>
                                <?php echo image_tag('/uploads/news/'.$news['id_news'].'_imagem_5.jpg','width="219" height="126"') ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo image_tag('/uploads/news/'.$news['id_news'].'_imagem_5.jpg','width="219" height="126"') ?>
                        <?php endif; ?>
                    </a>
                    <div class="title"><?php echo substr($news['title'], 0,100)  ?></div>
                    <div class="desc">
                        <?php $texto = $valida->limpiaCadena($news['sumario']) ?>
                        <?php $texto = $valida->cuentaPalabra($news['sumario'],14) ?>
                        <?php //echo html_entity_decode($texto) ?>
                        <?php echo substr(html_entity_decode($news['sumario']),0,90) ?>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="controls">
        <button class="prev"></button><button class="next"></button>
        </div>
        <div class="veja-todos" style="top: -2px !important; width: 156px;">
            <?php echo image_tag('frontend/veja_mais') ?>
            <?php echo link_to('Ver todos as notícias', '@menu?nucleo='.sfConfig::get('app_nome_base_app').'&secciones=noticias', array('class' => 'link_ver_todos')) ?>
        </div>
    </div>
    
</div>

<?php endif; ?>
