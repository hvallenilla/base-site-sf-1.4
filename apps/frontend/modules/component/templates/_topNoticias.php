<?php use_stylesheet('jq/galleriffic.css') ?>
<?php use_javascript('jq/jquery.galleriffic.js') ?>
<?php use_javascript('jq/jquery.opacityrollover.js') ?>
<!-- We only want the thunbnails to display when javascript is disabled -->
<script type="text/javascript">
        document.write('<style>.noscript { display: none; }</style>');
</script>
<!-- Start Advanced Gallery Html Containers -->
                <div id="gallery" class="content" style="float: left !important;">
                    <div id="controls" class="controls" style="display: none;"></div>
                        <div class="slideshow-container">
                                <div id="loading" class="loader"></div>
                                <div id="slideshow" class="slideshow"></div>
                        </div>
                        
                        <div id="caption" class="caption-container"></div>
                       
                </div>
                <div id="thumbs" class="navigation" style="float: right !important; width: 370px !important;">
                        <ul class="thumbs noscript">
                                <?php foreach ($topNews as $news):?>
                                <li>
                                    <?php if($news['image']): ?>
                                        <?php if(file_exists(sfConfig::get('sf_upload_dir').'/news/big_'.$news['image'])):?>
                                            <a class="thumb" name="leaf" href="<?php echo sfConfig::get('app_base_dir_web') ?>uploads/news/big_<?php echo $news['image'] ?>" title="">
                                                <?php echo image_tag('/uploads/news/small_'.$news['image'],'width="175" height="98"') ?>                                            
                                            </a>
                                        <?php else: ?>
                                            <a class="thumb" name="leaf" href="<?php echo sfConfig::get('app_base_dir_web') ?>uploads/news/<?php echo $news['id_news'] ?>_imagem_5.jpg" title="">
                                                <?php echo image_tag('/uploads/news/'.$news['id_news'].'_imagem_5.jpg','width="175" height="98"') ?>
                                            </a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <a class="thumb" name="leaf" href="<?php echo sfConfig::get('app_base_dir_web') ?>uploads/news/<?php echo $news['id_news'] ?>_imagem_5.jpg" title="">
                                            <?php echo image_tag('/uploads/news/'.$news['id_news'].'_imagem_5.jpg','width="175" height="98"') ?>
                                        </a>
                                    <?php endif; ?>                                    
                                    <div class="captions" style="text-align: left !important; padding-left: 9px; font-size: 12px; font-weight: normal !important; padding-top: 4px;">
                                        <a  href="<?php echo url_for('@permalink?nucleo=carapicuiba&secciones=noticias&subseccion=detalle&permalink='.$news['permalink']) ?>">
                                            <?php echo globalFunctions::trunkTextByword($news['title'], 80); ?>
                                        </a>    
                                    </div>
                                    <div class="caption">
                                            <div class="image-desc">
                                                <?php echo $news['title'] ?></div>
                                            <div class="boton-caption">
                                                <a href="<?php echo url_for('@permalink?nucleo=carapicuiba&secciones=noticias&subseccion=detalle&permalink='.$news['permalink']) ?>">
                                                    <?php echo image_tag('frontend/veja_mais','style="position: relative;top: 2px;"') ?>
                                                    Saiba mais
                                                </a>
                                            </div>
                                    </div>                                    
                                </li>
                                <?php endforeach;?>
                        </ul>
                </div>
                <div style="clear: both;"></div>
		
		<script type="text/javascript">
			$(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '375px', 'float' : 'left'});
				$('div.content').css('display', 'block');
				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 1.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: ''
				});				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     4500,
					numThumbs:                 15,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            7,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Slideshow',
					pauseLinkText:             'Pause Slideshow',
					prevLinkText:              '&lsaquo; Previous Photo',
					nextLinkText:              'Next Photo &rsaquo;',
					nextPageLinkText:          'Next &rsaquo;',
					prevPageLinkText:          '&lsaquo; Prev',
					enableHistory:             false,
					autoStart:                 true,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});
			});
		</script>