<?php
class errorActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
      
      $this->setLayout('layoutError');
      $this->getResponse()->setTitle('Erro - Página não encontrada - '.sfConfig::get('app_namecompany'));
  }

  public function executeUnavailable(sfWebRequest $request)
  {
    
  }
}
