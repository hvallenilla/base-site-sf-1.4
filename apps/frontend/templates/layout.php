<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
    <body>
        <!--[if lte IE 6]>
          <div align="center" style="border: 1px solid #ccc;padding:10px;">
              This browser is not supported to view this page. Please update it.
          </div>
        <![endif]-->
        <div id="container-ppal">
            <div id="content">
                <div id="conteint">
                    <div class="container showgrid">
                        <?php include_partial('global/header'); ?>
                        <?php echo $sf_content ?>
                    </div>
                </div>
            </div>
            <div id="content-footer">
                <div id="content">
                    <?php include_component('component', 'footer') ?>
                </div>
            </div>
            <div id="content-footer" class="content-footer-bottom">
                  <div id="content">
                      <div class="text">
                            <?php echo sfConfig::get('app_nameCompany') ?>
                            <a style="display: none; color: #333; float: right;" href="http://www.gallardodesigner.com.br/" target="_blank">
                                <?php echo image_tag('logo-gd') ?>
                            </a>
                        </div>
                  </div>
            </div>
        </div>
    </body>
</html>
