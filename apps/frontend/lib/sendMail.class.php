<?php
/**
 * sendMail PHP 
 * @package Carapicuiba
 * @author Henry Vallenilla <henryvallenilla@gmail.com>
 *
 */
class sendMail
{
    public function __construct() {
        sfProjectConfiguration::getActive()->loadHelpers("Partial");
    }
    
    static function mailContatoImprensa($request)
    {
        //$emailsender = sfConfig::get('app_email_administrator');
        $emailsender = $request->getParameter('email');
        //validacoes
        $empresa = $request->getParameter('empresa');
        $email = $request->getParameter('email');
        $pessoa_contato = $request->getParameter('pessoa_contato');
	$telefone = $request->getParameter('telefone');
	$assunto = $request->getParameter('assunto');	
	$mensagem = $request->getParameter('mensagem');
        
        if(PHP_OS == "Linux") 
        {
            $quebra_linha = "\n"; //Se for Linux
        }elseif(PHP_OS == "WINNT"){
            $quebra_linha = "\r\n"; // Se for Windows
        }
        $global = new globalFunctions();
        $mensagemHTML = $global->headerMail();
        $mensagemHTML.= '            
            <tr>
                <td style="font-family:Arial,Helvetica,sans-serif;margin:0px;font-size:14px;color: #333">
                  <strong>Olá, o(a) '.$empresa.' entrou no site da Prefeitura e fez contato com vocês. </strong>      
                </td>
            </tr>
            <tr>
                <td style="font-family:Arial,Helvetica,sans-serif;margin:0px;font-size:14px">
                    <br />Segue as informa&ccedil;&otilde;es abaixo: <br /><br />
                    <ul style="margin-top:0px;padding:5px 0px 0px 15px;line-height:22px">
                        <li style="margin-left:15px">
                              Empresa:&nbsp;
                              <strong style="color:#333">'.$empresa.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              E-mail:&nbsp;<strong style="color:#333;">'.$email.'</strong></li>
                        </li>
                        <li style="margin-left:15px">
                              Pessoa para contato:&nbsp;
                              <strong style="color:#333;">'.$pessoa_contato.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Telefone:&nbsp;
                              <strong style="color:#333;">'.$telefone.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Assunto:&nbsp;
                              <strong style="color:#333;">'.$assunto.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Mensagem:&nbsp;
                              <strong style="color:#333;">'.$mensagem.'.</strong>
                        </li>
                    </ul>
                </td>
            </tr>
        ';
        $mensagemHTML.= $global->footerMail();        
        $headers = "MIME-Version: 1.1".$quebra_linha;
        $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
        $headers .= "From: ".$emailsender.$quebra_linha;
        $headers .= "Return-Path: " . $emailsender . $quebra_linha;
        if(mail('faleconosco@carapicuiba.sp.gov.br', "Contato Prefeitura de Carapicuiba", $mensagemHTML, $headers, "-r". $emailsender))
        {
            return true;
        }else{
            return false;
        }
    }
    static function mailContatoPessoaFisica($request)
    {
        //$emailsender = sfConfig::get('app_email_administrator');
        $emailsender = $request->getParameter('email');
        //validacoes
        $nome = $request->getParameter('name');
        $email = $request->getParameter('email');
        $sexo = $request->getParameter('sexo') == 'm' ? "Masculino" : "Feminino";
	$nascimento = $request->getParameter('birthday');
	$escolaridade = $request->getParameter('education');
	$endereco = $request->getParameter('address');
	$end_numero = $request->getParameter('numero');
	$bairro = $request->getParameter('bairro');
	$cidade = $request->getParameter('cidade');
	$cep = $request->getParameter('cep');
	$telefone = $request->getParameter('phone');
	$mensagem = $request->getParameter('message');
        
        if(PHP_OS == "Linux") 
        {
            $quebra_linha = "\n"; //Se for Linux
        }elseif(PHP_OS == "WINNT"){
            $quebra_linha = "\r\n"; // Se for Windows
        }
        $global = new globalFunctions();
        $mensagemHTML = $global->headerMail();
        $mensagemHTML.= '            
            <tr>
                <td style="font-family:Arial,Helvetica,sans-serif;margin:0px;font-size:14px;color: #333">
                  <strong>Olá, o(a) '.$nome.' entrou no site da Prefeitura e fez contato com vocês. </strong>      
                </td>
            </tr>
            <tr>
                <td style="font-family:Arial,Helvetica,sans-serif;margin:0px;font-size:14px">
                    <br />Segue as informa&ccedil;&otilde;es abaixo: <br /><br />
                    <ul style="margin-top:0px;padding:5px 0px 0px 15px;line-height:22px">
                        <li style="margin-left:15px">
                              Nome:&nbsp;
                              <strong style="color:#333">'.$nome.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              E-mail:&nbsp;<strong style="color:#333;">'.$email.'</strong></li>
                        </li>
                        <li style="margin-left:15px">
                              Sexo:&nbsp;
                              <strong style="color:#333;">'.$sexo.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Nascimento:&nbsp;
                              <strong style="color:#333;">'.$nascimento.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Escolaridade:&nbsp;
                              <strong style="color:#333;">'.$escolaridade.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Endere&ccedil;o:&nbsp;
                              <strong style="color:#333;">'.$endereco.', N&deg;: '.$end_numero.', do bairro '.$bairro.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Cidade:&nbsp;
                              <strong style="color:#333;">'.$cidade.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              CEP:&nbsp;
                              <strong style="color:#333;">'.$cep.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Telefone:&nbsp;
                              <strong style="color:#333;">'.$telefone.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Mensagem:&nbsp;
                              <strong style="color:#333;">'.$mensagem.'.</strong>
                        </li>
                    </ul>
                </td>
            </tr>
        ';
        $mensagemHTML.= $global->footerMail();        
        $headers = "MIME-Version: 1.1".$quebra_linha;
        $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
        $headers .= "From: ".$emailsender.$quebra_linha;
        $headers .= "Return-Path: " . $emailsender . $quebra_linha;
        if(mail('faleconosco@carapicuiba.sp.gov.br', "Contato Prefeitura de Carapicuiba", $mensagemHTML, $headers, "-r". $emailsender))
        {
            return true;
        }else{
            return false;
        }
    }
    static function mailContatoPessoaJuridica($request)
    {
        //$emailsender = sfConfig::get('app_email_administrator');
        $emailsender = $request->getParameter('email');
        //validacoes
        $razao_social = $request->getParameter('razao_social');
	$cnpj = $request->getParameter('cnpj');
	$nome_fantasia = $request->getParameter('nome_fantasia');
	$seguemento = $request->getParameter('seguemento');
	$data_abertura = $request->getParameter('data_abertura');
	$email = $request->getParameter('email');
	$area_interesse = $request->getParameter('area_interesse');
	$endereco = $request->getParameter('address');
	$end_numero = $request->getParameter('numero');
	$bairro = $request->getParameter('bairro');
	$cidade = $request->getParameter('cidade');
	$cep = $request->getParameter('cep');
	$telefone = $request->getParameter('phone');
	$mensagem = $request->getParameter('message');
	$contato = $request->getParameter('contato');
	$departamento = $request->getParameter('departamento');
	
        if(PHP_OS == "Linux") 
        {
            $quebra_linha = "\n"; //Se for Linux
        }elseif(PHP_OS == "WINNT"){
            $quebra_linha = "\r\n"; // Se for Windows
        }
        $global = new globalFunctions();
        $mensagemHTML = $global->headerMail();
        $mensagemHTML.= '            
            <tr>
                <td style="font-family:Arial,Helvetica,sans-serif;margin:0px;font-size:14px;color: #333">
                  <strong>Olá, o(a) '.$razao_social.' entrou no site da Prefeitura e fez contato com vocês. </strong>      
                </td>
            </tr>
            <tr>
                <td style="font-family:Arial,Helvetica,sans-serif;margin:0px;font-size:14px">
                    <br />Segue as informa&ccedil;&otilde;es abaixo: <br /><br />
                    <ul style="margin-top:0px;padding:5px 0px 0px 15px;line-height:22px">
                        <li style="margin-left:15px">
                              Razão Social:&nbsp;
                              <strong style="color:#333">'.$razao_social.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              CNPJ:&nbsp;<strong style="color:#333;">'.$cnpj.'</strong></li>
                        </li>
                        <li style="margin-left:15px">
                              Nome Fantasia:&nbsp;
                              <strong style="color:#333;">'.$nome_fantasia.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Seguemento:&nbsp;
                              <strong style="color:#333;">'.$seguemento.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              E-mail:&nbsp;
                              <strong style="color:#333;">'.$email.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Data de Abertura:&nbsp;
                              <strong style="color:#333;">'.$data_abertura.'</strong>
                        </li>
                        <li style="margin-left:15px">
                              Area de Interesse:&nbsp;
                              <strong style="color:#333;">'.$area_interesse.'.</strong>
                        </li>                        
                        <li style="margin-left:15px">
                              Endereço:&nbsp;
                              <strong style="color:#333;">'.$endereco.', N&deg;: '.$end_numero.', do bairro '.$bairro.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Cidade:&nbsp;
                              <strong style="color:#333;">'.$cidade.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              CEP:&nbsp;
                              <strong style="color:#333;">'.$cep.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Telefone:&nbsp;
                              <strong style="color:#333;">'.$telefone.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Contato:&nbsp;
                              <strong style="color:#333;">'.$contato.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Departamento:&nbsp;
                              <strong style="color:#333;">'.$departamento.'.</strong>
                        </li>
                        <li style="margin-left:15px">
                              Mensagem:&nbsp;
                              <strong style="color:#333;">'.$mensagem.'.</strong>
                        </li>
                    </ul>
                </td>
            </tr>
        ';
        $mensagemHTML.= $global->footerMail();
        
        $headers = "MIME-Version: 1.1".$quebra_linha;
        $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
        $headers .= "From: ".$emailsender.$quebra_linha;
        $headers .= "Return-Path: " . $emailsender . $quebra_linha;
        if(mail('faleconosco@carapicuiba.sp.gov.br', "Contato Prefeitura de Carapicuiba", $mensagemHTML, $headers, "-r". $emailsender))
        {
            return true;
        }else{
            return false;
        }
        
    }
    
    static function mailTrackOrder()
    {
        
        $message = sfContext::getInstance()->getMailer();
        $correo = $message->compose();
        $correo->setSubject("Tiene un nuevo mensaje relacionado con su Orden Nro. Stocksys Puma");
        $correo->setTo('henryvallenilla@gmail.com');
        $correo->setFrom('formulario@carapicuiba.sp.gov.br');
        
        $parametros = array(
            'nombre'         => 'Henry',
            'email'          => "henryvallenilla@gmail.com",
            
            
        );
        $html = get_partial('mail/mailHeader');
        $html = $html.get_partial('mail/mailNewSenha', array('parametros' => $parametros));
        $html = $html.get_partial('mail/mailFooter');
        $correo->setBody($html, 'text/html');
        
        $message->send($correo);
    }
    
    
}
