<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
class gaFilter extends sfFilter {
  public function execute($filterChain) {
    // Nothing to do before the action
    $filterChain->execute();
    // Decorar la respuesta con el código de Google Analytics
    $codigoGoogle = "
    <!-- Google Analytics -->
    <script type='text/javascript'>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-39791930-1']);
        _gaq.push(['_trackPageview']);

        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

      </script>
    ";
    $respuesta = $this->getContext()->getResponse();
    $respuesta->setContent(str_ireplace('</body>', $codigoGoogle.'</body>',$respuesta->getContent()));
  }
}
?>
