/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Muestra las opciones Edit - Delete - Entre otras de los listados de los diferentes modulos
 */
var tmpStyle;
var base_url = "/";
function overRow(item)
{
    $(".row-actions_" + item).show();
    /*tmpStyle = $("#row_style_" + item).attr('class');
    $("#row_style_" + item).attr('class','bgList');*/
}
/**
 * Oculta las opciones Edit - Delete - Entre otras de los listados de los diferentes modulos
 */
function outRow(item)
{    
    $(".row-actions_" + item).hide();
    /*$("#row_style_" + item).attr('class',tmpStyle);*/
}
/**
 * Muestra los privilegios
 */
function viewInfoSection()
{
    $(".informationSection").show();
    $("#viewInfoSection").hide();
    $("#noViewInfoSection").show();
}

function noViewInfoSection()
{
    $(".informationSection").hide();
    $("#viewInfoSection").show();
    $("#noViewInfoSection").hide();
}


/**
 * Muestra los privilegios
 */
function showPrivileges(item)
{
    $(".permissions_" + item).show();
    $("#displayPrivActive_" + item).hide();
    $("#displayPrivDesactive_" + item).show();
}
/**
 * Oculta los privilegios
 */
function hidePrivileges(item)
{
    $(".permissions_" + item).hide();
    $("#displayPrivActive_" + item).show();
    $("#displayPrivDesactive_" + item).hide();
}
/**
 * Oculta el mensaje de actualizacion de privilegios
 */
function fadeMessage(secuence)
{
    $("#validate-secao" + secuence).hide();

}

/**
 * Ajax para registrar los nuevos permisos
 */
function submitPermissionsLxProfile(module,priv,profile)
{
    // Detecto el status del check, checked o unchecked
    var checkSelected = $("#chk_" + module + "_" + priv + ":checked").val();
    var privPpal = 0;
    if(!checkSelected){
        checkSelected = 0;
    }
    // Si el privilegio seleccionado es distinto de 1(View), lo activo
    if(priv != 1)
        {
            $("#chk_" + module + "_1").attr('checked',true);
            privPpal = 1; // Me indica que en la accion debo agregar otra permisologia con id_privelege = 1
        }
    // Si el privilegio seleccionado es igual a 1(View) y lo estoy desactivando, entonces desactivo el resto
    if(priv == 1 && checkSelected !=1)
        {
            $("#chk_" + module + "_2").attr('checked',false);
            $("#chk_" + module + "_3").attr('checked',false);
            $("#chk_" + module + "_4").attr('checked',false);
        }
    // Ajax para guardar los cambios de permisologia
    $(function(){
        $.ajax({
          type: "POST",
          url: "lxprofile/changePrivileges?id_module=" + module + "&id_privilege=" + priv + "&id_profile=" + profile + "&status=" + checkSelected + "&privPpal=" + privPpal,
          dataType: "script",
          beforeSend: function(objeto){
                $("#message_" + module).html("Wait ...");
          },
          success: function(msg){
                $("#message_" + module).animate({width:150, height:10}, "slow");
                $("#message_" + module).html('<div>Saved &nbsp;&nbsp;&nbsp; <a href="#" onclick="fadeMessage(' + module + ' );">Hide</a></div>');                
          },
          error: function(objeto, quepaso, otroobj){
                $("#message_" + module).animate({width:150, height:10}, "slow");
                $("#message_" + module).html('<div>Error. Please update browser&nbsp;&nbsp;&nbsp; <a href="#" onclick="fadeMessage(' + module + ' );">Hide</a></div>');
          }

        });
    });
}

/**
 * Ajax para registrar los nuevos permisos
 */
function submitPermissions(module,priv,profile)
{
    // Detecto el status del check, checked o unchecked
    var checkSelected = $("#chk_" + module + "_" + priv + ":checked").val();
    
    var privPpal = 0;
    if(!checkSelected){
        checkSelected = 0;
    }
    // Si el privilegio seleccionado es distinto de 1(View), lo activo
    if(priv != 1)
        {
            $("#chk_" + "_1").attr('checked',true);
            privPpal = 1; // Me indica que en la accion debo agregar otra permisologia con id_privelege = 1
        }
    // Si el privilegio seleccionado es igual a 1(View) y lo estoy desactivando, entonces desactivo el resto
    if(priv == 1 && checkSelected !=1)
        {
            $("#chk_" + "_2").attr('checked',false);
            $("#chk_" + "_3").attr('checked',false);
            $("#chk_" + "_4").attr('checked',false);
        }
    // Ajax para guardar los cambios de permisologia
    $(function(){
        $.ajax({
          type: "POST",
          url: "lxprofile/changePrivileges?id_module=" + "&id_privilege=" + priv + "&id_profile=" + profile + "&status=" + checkSelected + "&privPpal=" + privPpal,
          dataType: "script",
          beforeSend: function(objeto){
                $("#validate-secao").html("Wait ...");
          },
          success: function(msg){
                $("#validate-secao").animate({width:150, height:10}, "slow");
                $("#validate-secao").html('<div>Saved &nbsp;&nbsp;&nbsp; <a href="#" onclick="fadeMessage(' + ' );">Hide</a></div>');                
          },
          error: function(objeto, quepaso, otroobj){
                $("#validate-secao").animate({width:150, height:10}, "slow");
                $("#validate-secao").html('<div>Error. Please update browser&nbsp;&nbsp;&nbsp; <a href="#" onclick="fadeMessage(' + ' );">Hide</a></div>');
          }

        });
    });
}


function submitPermissionsUser(module,iduser)
{
    // Detecto el status del check, checked o unchecked
    var checkSelected = $('input[name=chk_' + ']').is(':checked');
    var privPpal = 0;
    if(!checkSelected){
        checkSelected = 0;
    }else{
        checkSelected = 1;
    }
    // Ajax para guardar los cambios de permisologia del usuario
    $(function(){
        $.ajax({
          type: "POST",
          url: "lxuserpermission/changePermissionUser?id_module=" + "&id_user=" + iduser + "&status=" + checkSelected,
          dataType: "script",
          beforeSend: function(objeto){
                $("#validate-secao").html("Wait ...");
          },
          success: function(msg){
                $("#validate-secao").animate({width:150, height:10}, "slow");
                $("#validate-secao").html('<div>Dados armazenados &nbsp;&nbsp;&nbsp; <a href="#" onclick="fadeMessage(' + ' );">Ocultar</a></div>');                
          },
          error: function(objeto, quepaso, otroobj){
                $("#validate-secao").animate({width:150, height:10}, "slow");
                $("#validate-secao").html('<div>Error. Por favor, atualize navegador&nbsp;&nbsp;&nbsp; <a href="#" onclick="fadeMessage(' + ' );">Ocultar</a></div>');
          }

        });
    });
}

function changeCategoryInNuclueByNews(idnucleo, idnews)
{
    // Detecto el status del check, checked o unchecked
    var category = $("#category").val();
    var url = 'http://'+location.hostname+base_url+'backend_dev.php/';
    // Ajax para guardar los cambios de permisologia del usuario
    $(function(){
        $.ajax({
          type: "POST",
          url: url +"news/changeCategoryInNuclueByNews?id_news=" + idnews + "&id_nucleo=" + idnucleo + "&category=" + category,
          dataType: "script",
          beforeSend: function(objeto){
                $("#message").html("Wait ...");
          },
          success: function(msg){
                $("#message").animate({width:400, height:10}, "slow");
                $("#message").html('<div class="msn_ready">Dados armazenados</div>');       
                
          },
          error: function(objeto, quepaso, otroobj){
                $("#message").animate({width:400, height:10}, "slow");
                $("#message").html('<div class="msn_error">Error. Por favor, atualize navegador&nbsp;</div>');
          }
        });
    });
}

function uploadItem(moduleAction)
{
    var url = 'http://'+location.hostname+base_url+'backend_dev.php/';
    var urlUpload =  url + moduleAction;
    var btnUpload=$('#upload_image');
    var status=$('#mimeError');
    new AjaxUpload(btnUpload, {
    action: urlUpload,
    name: 'uploadfile',
    dataType: 'html',
    onSubmit: function(file,ext){
    //if there is an error
    if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
        status.show();
        status.animate({opacity: 5000}, 0)
        status.animate({opacity: 0}, 7000);
        return false;
    }
    $('#preLoad').show();
    btnUpload.css("display","none");
    $('#msg_upload').css("display","none");
    $('#wait').css("display","");
    $('#wait').html("&nbsp;&nbsp;Wait until the upload image...");
    },
    onComplete: function(file, response){
    $('#preLoad').hide();
    //Add uploaded file to list
    $('#image_poster').append(response);
    $('#preLoad').remove().appendTo("#image_poster");
    $('#no_foto').fadeOut('');
    btnUpload.css("display","");
    $('#wait').css("display","none");
    $('#msg_upload').css("display","");
    }
    });
}

function validateSecaoUrl(secao, id)
{
    var url = 'http://'+location.hostname+'/backend_dev.php/lxsection/';
    $.ajax({
          type: "POST",
          url: url + "validaSecaoUrl?seccion=" + secao + "&id_seccion=" + id,
          dataType: "script",
          beforeSend: function(objeto){
                $(".validate-secao").html('<img width="16" height="16" border="0" src="/images/preload.gif" >');
          },
          success: function(msg){
            if(msg == '0')
            {
                $(".validate-secao").html('<img width="16" height="16" border="0" src="/images/1.png" > Seção disponível');                
            }else{
                
                $(".validate-secao").html('<img width="16" height="16" border="0" src="/images/no_disponible.png" ><span>Seção não disponível</span>');
            }
                
          },
          error: function(objeto, otroobj){
                $(".validate-secao").animate({width:150, height:10}, "slow");
                $(".validate-secao").html('<div>Error. Please update browser&nbsp;&nbsp;&nbsp; <a href="#" onclick="fadeMessage(' + ' );">Hide</a></div>');
          }

        });
}






